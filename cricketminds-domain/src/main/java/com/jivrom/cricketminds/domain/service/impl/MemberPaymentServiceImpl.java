package com.jivrom.cricketminds.domain.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.jivrom.cricketminds.domain.dto.MemberPaymentDTO;
import com.jivrom.cricketminds.domain.model.Event;
import com.jivrom.cricketminds.domain.model.EventEnrollment;
import com.jivrom.cricketminds.domain.model.MemberPayment;
import com.jivrom.cricketminds.domain.repository.EventEnrollmentRepository;
import com.jivrom.cricketminds.domain.repository.EventRepository;
import com.jivrom.cricketminds.domain.repository.MemberPaymentRepository;
import com.jivrom.cricketminds.domain.repository.SessionEnrollmentRepository;
import com.jivrom.cricketminds.domain.repository.SessionRepository;
import com.jivrom.cricketminds.domain.service.MemberPaymentService;
import com.jivrom.cricketminds.payment.dto.PaymentDTO;

@Service
public class MemberPaymentServiceImpl implements MemberPaymentService {

	@Resource
	private MemberPaymentRepository memberPaymentRepository;
	@Resource
	private EventRepository eventRepository;
	@Resource
	private SessionRepository sessionRepository;
	@Resource
	private EventEnrollmentRepository eventEnrollmentRepository;
	@Resource
	private SessionEnrollmentRepository sessionEnrollmentRepository;
	
	@Override
	public List<MemberPaymentDTO> getAllPayments() {
		List<MemberPaymentDTO> retval =  new ArrayList<MemberPaymentDTO>();
		List<MemberPayment> payments = memberPaymentRepository.findAllMember();
		for(MemberPayment payment : payments) {
			MemberPaymentDTO dto = new MemberPaymentDTO(payment);

//			String eventName = eventRepository.findEventName(eventEnrollmentRepository.findEventId(dto.getMemberPaymentId()));
//			dto.setEventName(eventName);
			
			
			retval.add(dto);
		}
		return retval;
	}

	@Override
	public List<MemberPaymentDTO> getAllPayments(Long memberId) {
		List<MemberPaymentDTO> retval =  new ArrayList<MemberPaymentDTO>();
		//TODO: 
		List<MemberPayment> payments = memberPaymentRepository.findByMemberId(memberId);
		for(MemberPayment payment : payments) {
			MemberPaymentDTO dto = new MemberPaymentDTO(payment);

			String name= "";
			if(payment.getPaymentReason().equals("Event")){
				name = eventRepository.findEventName(eventEnrollmentRepository.findEventId(dto.getMemberPaymentId()));
				
			}else if(payment.getPaymentReason().equals("CoachingSession")){
				
				Long sessionId = sessionEnrollmentRepository.findSessionId(dto.getMemberPaymentId());
				
				name = sessionRepository.findSessionName(sessionId);
				
			} 
			dto.setEventName(name);
			retval.add(dto);
		}
		return retval;
	}

}
