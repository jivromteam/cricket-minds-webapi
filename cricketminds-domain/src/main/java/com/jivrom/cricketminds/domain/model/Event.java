package com.jivrom.cricketminds.domain.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


@Entity
@Table(name="EVENT",
uniqueConstraints = {
	}
)

public class Event {
    
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="EVENT_ID",nullable = false)
    private Long eventId;
    
    @Column(name="EVENT_NAME")
    private String eventName;

    @Column(name="EVENT_DESCRIPTION")
    private String eventDescription;

    @Column(name="LOCATION")
    private String location;

    @Column(name="START_DATE")
    private Date startDate;

    @Column(name="END_DATE")
    private Date endDate;

    @Column(name="FEE")
    private String fee;

    @Column(name="ENROLLMENT_END_DATE")
    private Date enrollmentEndDate;

    @Column(name="EVENT_STATUS")
    private String eventStatus;
    
    @Column(name="SLOTS_AVAILABLE")
    private String slotsAvailable;

    @Column(name="EVENT_BANNER_URL")
    private String eventBannerUrl;

    @Column(name="CREATED_DATE")
    private Date createdDate;

    @Column(name="UPDATED_DATE")
    private Date updatedDate;

    //@OneToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "EVENT_ID", nullable = false, insertable = false, updatable = false)
    @Fetch(value = FetchMode.SUBSELECT)
    //private List<EventType> memberTypes = new ArrayList<EventType>();

    
    public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
    }
    
    public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
    }
    
    public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
    }
    
    public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
    }

    public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
    }
    
    public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
    }
    
    public String getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
    }

    public Date getEnrollmentEndDate() {
		return enrollmentEndDate;
	}

	public void setEnrollmentEndDate(Date enrollmentEndDate) {
		this.enrollmentEndDate = enrollmentEndDate;
    }

    public String getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
    }
    
    public String getSlotsAvailable() {
		return slotsAvailable;
	}

	public void setSlotsAvailable(String slotsAvailable) {
		this.slotsAvailable = slotsAvailable;
    }

    public String getEventBannerUrl() {
		return eventBannerUrl;
	}

	public void setEventBannerUrl(String eventBannerUrl) {
		this.eventBannerUrl = eventBannerUrl;
    }

    public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date date) {
		this.createdDate = date;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}



    


}