package com.jivrom.cricketminds.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jivrom.cricketminds.domain.model.MemberSlot;

@Repository
public interface MemberSlotRepository extends JpaRepository<MemberSlot, Long> {


	@Query(nativeQuery = true, value = "SELECT * from MEMBER_SLOT WHERE SLOT_ID = :slotId") 
	public List<MemberSlot> findBySlotId(@Param("slotId") Long slotId);

	@Query(nativeQuery = true, value = "SELECT * from MEMBER_SLOT WHERE MEMBER_ID = :memberId AND SLOT_ID = :slotId") 
	public MemberSlot findByMemberId(@Param("memberId") Long memberId, @Param("slotId") Long slotId);

	@Query(nativeQuery = true, value = "SELECT COUNT(MEMBER_ID) from MEMBER_SLOT WHERE SLOT_ID = :slotId AND MEMBER_TYPE = :memberType") 
	public Long findSlotBookingsUsed(@Param("slotId") Long slotId, @Param("memberType") String memberType);
}
