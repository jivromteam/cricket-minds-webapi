package com.jivrom.cricketminds.domain.dto;

public class ProfilePictureDTO {

    private Long memberId;
    private String profilepictureurl;
    
    public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
    }
    
    public String getProfilePictureUrl() {
		return profilepictureurl;
	}

	public void setProfilePictureUrl(String profilepictureurl) {
		this.profilepictureurl = profilepictureurl;
	}


}