package com.jivrom.cricketminds.domain.repository;

import java.util.List;

import com.jivrom.cricketminds.domain.model.Event;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EventRepository extends JpaRepository<Event,Long>{

    //public List<Event> findEventByStatus(String eventStatus);
   
    @Query(nativeQuery = true, value = "SELECT * from EVENT WHERE EVENT_STATUS = 'Active' AND END_DATE< curdate()") 	
    public List<Event> findPastEvents(); 

    @Query(nativeQuery = true, value = "SELECT * from EVENT WHERE EVENT_STATUS = 'Active' AND START_DATE > curdate()") 	
    public List<Event> findFutureEvents(); 	 
    	
    @Query(nativeQuery = true, value = "SELECT * from EVENT WHERE EVENT_STATUS = 'Active' AND START_DATE <= curdate() AND END_DATE >curdate()") 	
    public List<Event> findCurrentEvents();
    
    @Query(nativeQuery = true, value = "SELECT EVENT_NAME from EVENT WHERE EVENT_ID = :eventId") 	
    String findEventName( @Param("eventId") Long eventId);

    @Query(nativeQuery = true, value = "SELECT SLOTS_AVAILABLE from EVENT WHERE EVENT_ID = :eventId") 	
    Long findEventEnrollmentAvailable( @Param("eventId") Long eventId);
    
    @Query(nativeQuery = true, value = "SELECT e.* from EVENT e inner join EVENT_ENROLLMENT en on e.EVENT_ID = en.EVENT_ID WHERE en.MEMBER_ID = :memberId and e.EVENT_STATUS = 'Active' AND e.END_DATE< curdate()") 	
    public List<Event> findPastEvents(@Param("memberId")Long memberId); 
    
    @Query(nativeQuery = true, value = "SELECT * from EVENT e inner join EVENT_ENROLLMENT en on e.EVENT_ID = en.EVENT_ID WHERE en.MEMBER_ID = :memberId and e.EVENT_STATUS = 'Active' AND e.START_DATE > curdate()")
    public List<Event> findFutureEvents(@Param("memberId")Long memberId); 	
    
    @Query(nativeQuery = true, value = "SELECT * from EVENT e inner join EVENT_ENROLLMENT en on e.EVENT_ID = en.EVENT_ID WHERE en.MEMBER_ID = :memberId and e.EVENT_STATUS = 'Active' AND e.START_DATE <= curdate() AND e.END_DATE >curdate()")
    public List<Event> findCurrentEvents(@Param("memberId")Long memberId);
    

}