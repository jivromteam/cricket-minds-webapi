package com.jivrom.cricketminds.domain.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="MEMBER_SLOT",
uniqueConstraints = {
	}
)

public class MemberSlot {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="MEMBER_SLOT_ID",nullable = false)
	private Long memberSlotId;
	
    @Column(name="SLOT_ID")
    private Long slotId;
    
    @Column(name="MEMBER_ID")
    private Long memberId;
    
    @ManyToOne
	@JoinColumn(name="MEMBER_ID", referencedColumnName = "MEMBER_ID", nullable = false, insertable = false, updatable = false)
	private Member member;
    
    @Column(name="MEMBER_TYPE")
    private String memberType;
    
    @Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	public Long getMemberSlotId() {
		return memberSlotId;
	}

	public void setMemberSlotId(Long memberSlotId) {
		this.memberSlotId = memberSlotId;
	}

	public Long getSlotId() {
		return slotId;
	}

	public void setSlotId(Long slotId) {
		this.slotId = slotId;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
}
