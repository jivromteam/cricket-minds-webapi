package com.jivrom.cricketminds.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jivrom.cricketminds.domain.model.Member;

@Repository
public interface MemberRepository extends JpaRepository<Member, Long> {

	public Member findByEmailId(String emailId);

	public Member findByMemberId(Long memberId);
	
	public List<Member> findByStatus(String status);

    @Query(nativeQuery = true, value = "SELECT m.*,mt.MEMBER_TYPE FROM MEMBER m JOIN MEMBER_TYPE mt ON mt.MEMBER_ID = m.MEMBER_ID WHERE mt.MEMBER_TYPE IN ('COACH','HDCOACH')")
	public List<Member> findAllCoaches();
	
	@Query(nativeQuery = true, value = "SELECT m.*,mt.MEMBER_TYPE FROM MEMBER m JOIN MEMBER_TYPE mt ON mt.MEMBER_ID = m.MEMBER_ID WHERE mt.MEMBER_TYPE IN ('MEMBER')")
    public List<Member> findAllMembers();
}
