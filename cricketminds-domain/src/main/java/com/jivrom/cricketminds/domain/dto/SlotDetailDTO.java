package com.jivrom.cricketminds.domain.dto;

import java.util.List;

public class SlotDetailDTO {

	private SlotDTO slot;
	private List<UserDTO> members;
	private List<UserDTO> coaches;
	
	public SlotDetailDTO(SlotDTO slot, List<UserDTO> members, List<UserDTO> coaches) {
		this.slot = slot;
		this.members = members;
		this.coaches = coaches;
	}
	
	public List<UserDTO> getCoaches() {
		return coaches;
	}

	public void setCoaches(List<UserDTO> coaches) {
		this.coaches = coaches;
	}

	public SlotDTO getSlot() {
		return slot;
	}
	public void setSlot(SlotDTO slot) {
		this.slot = slot;
	}
	
	public List<UserDTO> getMembers() {
		return members;
	}
	
	public void setMembers(List<UserDTO> members) {
		this.members = members;
	}
	
	
}
