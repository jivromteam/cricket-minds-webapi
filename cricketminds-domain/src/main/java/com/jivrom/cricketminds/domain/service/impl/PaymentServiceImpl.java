package com.jivrom.cricketminds.domain.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.jivrom.cricketminds.domain.dto.PaymentDueDTO;
import com.jivrom.cricketminds.domain.dto.SessionPaymentDTO;
import com.jivrom.cricketminds.domain.model.MemberPayment;
import com.jivrom.cricketminds.domain.model.MemberPaymentFactory;
import com.jivrom.cricketminds.domain.model.PaymentReason;
import com.jivrom.cricketminds.domain.model.PaymentStatus;
import com.jivrom.cricketminds.domain.model.SessionPayment;
import com.jivrom.cricketminds.domain.repository.MemberPaymentRepository;
import com.jivrom.cricketminds.domain.repository.SessionPaymentRepository;
import com.jivrom.cricketminds.domain.service.PaymentService;
import com.jivrom.cricketminds.payment.dto.MemberSessionPaymentDTO;
import com.jivrom.cricketminds.payment.service.ExternalPaymentService;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;

@Service
public class PaymentServiceImpl implements PaymentService {

	@Resource
	private SessionPaymentRepository sessionPaymentRepository;
	
	@Resource
    private ExternalPaymentService externalPaymentService;
    
    @Resource
    private MemberPaymentFactory memberPaymentFactory;
    
    @Resource
    private MemberPaymentRepository memberPaymentRepository;
    
    
	
	@Override
	public PaymentDueDTO getPaymentDue(Long memberId) {
		List<SessionPayment> paymentDueHistories = sessionPaymentRepository.findDueByMemberId(memberId);
		
		PaymentDueDTO paymentDTO = new PaymentDueDTO();
		paymentDTO.setMemberId(memberId);
		Long totalAmount = 0l;
		
		
		for(SessionPayment payment: paymentDueHistories) {
			String amountDue = payment.getAmountDue();
			Date dueDate = payment.getPaymentDueDate();
			Long id = payment.getSessionPaymentId();
			String sessionName = payment.getSession().getSessionName();
			String paymentMonth = payment.getPaymentMonth();
			
			paymentDTO.addDue(new PaymentDueDTO.PaymentDueHistory(id, amountDue, dueDate, sessionName, paymentMonth));
			totalAmount += Long.parseLong(amountDue);
			
		}
		paymentDTO.setTotalAmountDue(Long.toString(totalAmount));
		return paymentDTO;
	}
	
	@Override
	public void generatePaymentDue(Long sessionId, String month, List<Long> memberIds, String amount) {
		
		List<SessionPayment> paymentsToSave = new ArrayList<>();
		
		for(Long memberId: memberIds) {
			SessionPayment sessionPayment = new  SessionPayment();
			sessionPayment.setSessionId(sessionId);
			sessionPayment.setAmountDue(amount);
			sessionPayment.setPaymentMonth(month);
			sessionPayment.setStatus(PaymentStatus.DUE.getStatus());
			
			sessionPayment.setMemberId(memberId);
			sessionPayment.setCreatedDate(new Date());
			
			paymentsToSave.add(sessionPayment);
		}
		
		if(!paymentsToSave.isEmpty()) {
			sessionPaymentRepository.save(paymentsToSave);
		}
		
	}

	@Override
	public void makeSessionPayment(Long memberId, MemberSessionPaymentDTO sessionPaymentDTO) {
		
		Charge charge;
		try {
			charge = externalPaymentService.charge(sessionPaymentDTO);
			
			String receipt = charge.getReceiptUrl();
			// save the payment details
			MemberPayment memberPayment = memberPaymentFactory.create(sessionPaymentDTO, charge, PaymentReason.COACHINNG_SESSION);
			MemberPayment updated = memberPaymentRepository.save(memberPayment);
			
			
			// update session_payment
			System.out.println("sessionPaymentDTO session payment ids : "+sessionPaymentDTO.getSessionPaymentIds());
			for(Long sessionPaymentId: sessionPaymentDTO.getSessionPaymentIds()) {
				SessionPayment sessionPayment = sessionPaymentRepository.findOne(sessionPaymentId);
				sessionPayment.setMemberPaymentId(updated.getMemberPaymentId());
				
				if(charge.getStatus().equals("succeeded")) {
					sessionPayment.setStatus(PaymentStatus.PAID.getStatus());
				} else  {
					sessionPayment.setStatus(PaymentStatus.PENDING.getStatus());
				}
				
				sessionPayment.setUpdatedDate(new Date());
				
				sessionPaymentRepository.save(sessionPayment);
			}
			
			
		} catch (StripeException e) {
			e.printStackTrace();
		}
        
		
	}

	@Override
	public List<SessionPaymentDTO> getPaymentDue() {
		List<SessionPayment> sessionPayments = sessionPaymentRepository.findAllDue(); 
		
		List<SessionPaymentDTO> retval = new ArrayList<>();
		for(SessionPayment sessionPayment: sessionPayments) {
			SessionPaymentDTO sessionPaymentDTO = new SessionPaymentDTO(sessionPayment);
			retval.add(sessionPaymentDTO);
			
		}
		System.out.println("  retval :"+retval);
		
		return retval;
	}

}
