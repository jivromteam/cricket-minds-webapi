package com.jivrom.cricketminds.domain.dto;

public class PasswordDTO {
    private Long memberId;
    private String password;
    
    public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
    }
    
    public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}