package com.jivrom.cricketminds.domain.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class EventUpdateDTO {

    //private Long eventId;
    private String eventName;
    private String eventDescription;
    private String location;
    private String fee;
    private String slotsAvailable;
    private String eventBannerUrl;
    
    @JsonFormat(pattern = "MM-dd-yyyy HH:mm")
    private Date startDate;
    
    @JsonFormat(pattern = "MM-dd-yyyy HH:mm")
    private Date endDate;
    
    @JsonFormat(pattern = "MM-dd-yyyy")
	private Date enrollmentEndDate;

    /*public Long getEventId() {
		return eventId;
	}
	public void setEventId(Long eventId) {
		this.eventId = eventId;
    }*/

    public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
    }

    public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
    }

    public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
    }

    public String getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
    }
    
    public String getSlotsAvailable() {
		return slotsAvailable;
	}

	public void setSlotsAvailable(String slotsAvailable) {
		this.slotsAvailable = slotsAvailable;
    }

    public String getEventBannerUrl() {
		return eventBannerUrl;
	}

	public void setEventBannerUrl(String eventBannerUrl) {
		this.eventBannerUrl = eventBannerUrl;
    }

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getEnrollmentEndDate() {
		return enrollmentEndDate;
	}

	public void setEnrollmentEndDate(Date enrollmentEndDate) {
		this.enrollmentEndDate = enrollmentEndDate;
	}
	
	
}