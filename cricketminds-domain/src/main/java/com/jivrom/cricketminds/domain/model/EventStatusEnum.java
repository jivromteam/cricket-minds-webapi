package com.jivrom.cricketminds.domain.model;

public enum EventStatusEnum {

    NULL("NULL"),
    ACTIVE("Active"),
    INACTIVE("Inactive");
    
    private String status;
	
	EventStatusEnum(String status) {
		this.status = status;
    }
    
    public String getStatus() {
		return this.status;
    }
    
    public static EventStatusEnum fromValue(String status) {
		for(EventStatusEnum thisStatus: values()) {
			if(thisStatus.getStatus().equals(status)) {
				return thisStatus;
			}
		}
		
		return NULL;
	}
	
	public boolean isNull() {
		return this == NULL;
	}

}