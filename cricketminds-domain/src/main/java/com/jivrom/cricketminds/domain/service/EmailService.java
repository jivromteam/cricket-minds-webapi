package com.jivrom.cricketminds.domain.service;

import com.jivrom.cricketminds.domain.model.Mail;



public interface EmailService {
    
    public void sendEmail(Mail mail);

}