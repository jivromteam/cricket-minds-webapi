package com.jivrom.cricketminds.domain.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SESSION_TYPE",
uniqueConstraints = {
	}
)
public class SessionType {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="SESSION_TYPE_ID",nullable = false)
	private Long sessionTypeId;
	
    @Column(name="TYPE")
	private String type;
    
    @Column(name="DEFAULT_HOURS")
	private String defaultHours;
    
    @Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	public Long getSessionTypeId() {
		return sessionTypeId;
	}

	public void setSessionTypeId(Long sessionTypeId) {
		this.sessionTypeId = sessionTypeId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDefaultHours() {
		return defaultHours;
	}

	public void setDefaultHours(String defaultHours) {
		this.defaultHours = defaultHours;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
