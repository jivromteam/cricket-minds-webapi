package com.jivrom.cricketminds.domain.exception;

public class EnrollmentNotFoundException extends RuntimeException {

	public EnrollmentNotFoundException(String message) {
		super(message);
	}
	
}