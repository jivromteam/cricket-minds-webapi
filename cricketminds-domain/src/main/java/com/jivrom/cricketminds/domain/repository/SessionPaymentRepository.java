package com.jivrom.cricketminds.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jivrom.cricketminds.domain.model.SessionPayment;

@Repository
public interface SessionPaymentRepository  extends JpaRepository<SessionPayment, Long> {
	
	public static final String GET_ALL_DUE = 
			"Select * " + 
			"	from SESSION_PAYMENT sp, SESSION s, MEMBER m " + 
			"	where sp.session_id = s.session_id " + 
			"	and sp.member_id = m.member_id " + 
			"	and sp.STATUS = 'Due' ";

	@Query(nativeQuery = true, value = "SELECT * from SESSION_PAYMENT WHERE MEMBER_ID = :memberId AND status = 'Due' ")
	public List<SessionPayment> findDueByMemberId(@Param("memberId") Long memberId);
	
	@Query(nativeQuery = true, value = GET_ALL_DUE)
	public List<SessionPayment> findAllDue();
	
	
	
}
