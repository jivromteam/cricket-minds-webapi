package com.jivrom.cricketminds.domain.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="MEMBER_TYPE",
uniqueConstraints = {
	}
)
public class MemberType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="MEMBER_TYPE_ID",nullable = false)
	private Long memberTypeId;

//    @Column(name="MEMBERID")
//	@ManyToOne(fetch=FetchType.LAZY,cascade = CascadeType.DETACH)
//	@JoinColumn(name = "MEMBER_ID", referencedColumnName="MEMBER_ID", nullable = false)
//	private Member member;
	
	@Column(name="MEMBER_ID",nullable = false)
	private Long memberId;

    @Column(name="MEMBER_TYPE")
	private String memberType;

    @Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="UPDATED_DATE")
	private Date updatedDate;

    public Long getMemberTypeId() {
		return memberTypeId;
	}

	public void setMemberTypeId(Long memberTypeId) {
		this.memberTypeId = memberTypeId;
	}

//	public Member getMember() {
//		return member;
//	}
//
//	public void setMember(Member member) {
//		this.member = member;
//	}
	
	

    public String getMemberType() {
		return memberType;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

    public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}