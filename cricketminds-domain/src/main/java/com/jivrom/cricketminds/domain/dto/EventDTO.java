package com.jivrom.cricketminds.domain.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jivrom.cricketminds.domain.model.Event;

public class EventDTO {

    private Long eventId;
	private String eventName;
	private String eventDescription;
	private String location;
	@JsonFormat(pattern = "MM-dd-yyyy HH:mm")
	private Date startDate;
	@JsonFormat(pattern = "MM-dd-yyyy HH:mm")
    private Date endDate;
	private String fee;
	
	@JsonFormat(pattern = "MM-dd-yyyy")
    private Date enrollmentEndDate;
	private String eventStatus;
    private String slotsAvailable;
	private String eventBannerUrl;
    //private Date createdDate;
    //private Date updatedDate;

    //private List<EventTpe> types = new ArrayList<>();

    public EventDTO(Event event){

        this.eventId = event.getEventId();
        this.eventName=event.getEventName();
        this.eventDescription=event.getEventDescription();
        this.location=event.getLocation();
        this.startDate=event.getStartDate();
        this.endDate=event.getEndDate();
        this.fee=event.getFee();
        this.enrollmentEndDate=event.getEnrollmentEndDate();
        this.eventStatus=event.getEventStatus();
        this.slotsAvailable=event.getSlotsAvailable();
        this.eventBannerUrl=event.getEventBannerUrl();
        //this.createdDate=event.getCreatedDate();
        //this.updatedDate=event.getUpdatedDate();


    }


    public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
    }
    
    public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
    }
    
    public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
    }
    
    public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
    }

    public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
    }
    
    public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
    }
    
    public String getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
    }

    public Date getEnrollmentEndDate() {
		return enrollmentEndDate;
	}

	public void setEnrollmentEndDate(Date enrollmentEndDate) {
		this.enrollmentEndDate = enrollmentEndDate;
    }

    public String getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
    }
    
    public String getSlotsAvailable() {
		return slotsAvailable;
	}

	public void setSlotsAvailable(String slotsAvailable) {
		this.slotsAvailable = slotsAvailable;
    }

    public String getEventBannerUrl() {
		return eventBannerUrl;
	}

	public void setEventBannerUrl(String eventBannerUrl) {
		this.eventBannerUrl = eventBannerUrl;
    }

    /*public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}*/



}