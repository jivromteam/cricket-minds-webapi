package com.jivrom.cricketminds.domain.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class SessionUpdateDTO {

    private String cronTab;
    private Long sessionId;
    private Long sessionTypeId;
    private String sessionType;
    private String sessionName;
    private String sessionPrice;
    @JsonFormat(pattern = "MM-dd-yyyy HH:mm")
    private Date startDate;
    @JsonFormat(pattern = "MM-dd-yyyy HH:mm")
    private Date endDate;
    @JsonFormat(pattern = "HH:mm")
    private Date startTime;
    @JsonFormat(pattern = "HH:mm")
    private Date endTime;

    /*
     * public void SessionDTO(Session session) { this.sessionId =
     * session.getSessionId(); this.sessionTypeId=session.getSessionTypeId();
     * this.sessionName =session.getSessionName();
     * this.sessionPrice=session.getSessionPrice();
     * this.startDate=session.getStartDate(); this.endDate=session.getEndDate(); }
     * 
     * public void SessionDTO() { }
     */

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Long getSessionTypeId() {
        return sessionTypeId;
    }

    public void setSessionTypeId(Long sessionTypeId) {
        this.sessionTypeId = sessionTypeId;
    }

    public String getSessionPrice() {
        return sessionPrice;
    }

    public void setSessionPrice(String sessionPrice) {
        this.sessionPrice = sessionPrice;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getSessionType() {
        return sessionType;
    }

    public void setSessionType(String sessionType) {
        this.sessionType = sessionType;
    }

    public String getCronTab() {
        return cronTab;
    }

    public void setCronTab(String cronTab) {
        this.cronTab = cronTab;
    }

    public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
}