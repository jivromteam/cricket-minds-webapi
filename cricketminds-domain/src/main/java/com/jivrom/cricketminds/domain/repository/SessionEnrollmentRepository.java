package com.jivrom.cricketminds.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.jivrom.cricketminds.domain.model.SessionEnrollment;

@Repository
public interface SessionEnrollmentRepository extends JpaRepository<SessionEnrollment, Long>{

    @Query(nativeQuery = true, value = "SELECT * from SESSION_ENROLLMENT WHERE MEMBER_ID = :memberId AND SESSION_ID = :sessionId") 
	public List<SessionEnrollment> findByMemberAndSession(@Param("memberId") Long memberId, @Param("sessionId") Long sessionId);

	//@Query(nativeQuery = true, value = "SELECT * from EVENT_ENROLLMENT inner join EVENT on EVENT.EVENT_ID = EVENT_ENROLLMENT.EVENT_ID WHERE MEMBER_PAYMENT_ID = :memberPaymentId") 
	//public List<EventEnrollment> findAllEvents(@Param("memberPaymentId") Long memberId);

	@Query(nativeQuery = true, value = "SELECT SESSION_ID from SESSION_ENROLLMENT  WHERE MEMBER_PAYMENT_ID = :memberPaymentId") 
	public Long findSessionId( @Param("memberPaymentId") Long memberPaymentId);

	@Query(nativeQuery = true, value = "SELECT * from SESSION_ENROLLMENT WHERE MEMBER_PAYMENT_ID = :memberPaymentId") 
	public SessionEnrollment findByMemberPaymentId( @Param("memberPaymentId") Long memberPaymentId);

	@Query(nativeQuery = true, value = "SELECT COUNT(MEMBER_ID) from SESSION_ENROLLMENT WHERE SESSION_ID = :sessionId AND PAYMENT_STATUS='succeeded'") 
	public Long findSessionEnrollmentUsed( @Param("sessionId") Long sessionId);
	
    //	@Query(nativeQuery = true, value = "SELECT * from EVENT_ENROLLMENT WHERE MEMBER_ID = :memberId AND EVENT_ID = :eventId")
    
	@Query(nativeQuery = true, value = "SELECT * from SESSION_ENROLLMENT WHERE SESSION_ID = :sessionId AND ROLE = :role")
	public List<SessionEnrollment> findBySessionAndRole(@Param("sessionId") Long sessionId, @Param("role") String role);

	@Query(nativeQuery = true, value = "SELECT * from SESSION_ENROLLMENT WHERE SESSION_ID = :sessionId AND MEMBER_ID = :memberId")
	public SessionEnrollment findBySessionAndMember(@Param("sessionId") Long sessionId, @Param("memberId") Long memberId);

	/*@Query(nativeQuery = true, value = "SELECT MEMBER_ID from SESSION_ENROLLMENT  WHERE MEMBER_ID = :memberId") 
	public Long findByMemberId( @Param("memberId") Long memberId); */

	@Query(nativeQuery = true, value = "SELECT * from SESSION_ENROLLMENT WHERE SESSION_ID = :sessionId ")
	public List<SessionEnrollment> findBySessionId(@Param("sessionId") Long sessionId);

}
