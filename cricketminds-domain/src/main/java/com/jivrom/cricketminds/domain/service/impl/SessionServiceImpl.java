package com.jivrom.cricketminds.domain.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.jivrom.cricketminds.domain.dto.SessionDTO;
import com.jivrom.cricketminds.domain.dto.SessionRegisterDTO;
import com.jivrom.cricketminds.domain.dto.SessionUpdateDTO;
import com.jivrom.cricketminds.domain.dto.UserDTO;
import com.jivrom.cricketminds.domain.exception.EventNotFoundException;
import com.jivrom.cricketminds.domain.exception.SessionNotFoundException;
import com.jivrom.cricketminds.domain.model.Mail;
import com.jivrom.cricketminds.domain.model.Member;
import com.jivrom.cricketminds.domain.model.MemberPayment;
import com.jivrom.cricketminds.domain.model.MemberPaymentFactory;
import com.jivrom.cricketminds.domain.model.MemberSlot;
import com.jivrom.cricketminds.domain.model.PaymentReason;
import com.jivrom.cricketminds.domain.model.Session;
import com.jivrom.cricketminds.domain.model.SessionEnrollment;
import com.jivrom.cricketminds.domain.model.SessionEnrollmentFactory;
import com.jivrom.cricketminds.domain.model.Slot;
import com.jivrom.cricketminds.domain.repository.MemberPaymentRepository;
import com.jivrom.cricketminds.domain.repository.MemberRepository;
import com.jivrom.cricketminds.domain.repository.MemberSlotRepository;
import com.jivrom.cricketminds.domain.repository.MemberTypeRepository;
import com.jivrom.cricketminds.domain.repository.SessionEnrollmentRepository;
import com.jivrom.cricketminds.domain.repository.SessionRepository;
import com.jivrom.cricketminds.domain.repository.SessionTypeRepository;
import com.jivrom.cricketminds.domain.repository.SlotRepository;
import com.jivrom.cricketminds.domain.service.EmailService;
import com.jivrom.cricketminds.domain.service.SessionService;
import com.jivrom.cricketminds.domain.service.SessionSlotService;
import com.jivrom.cricketminds.payment.dto.PaymentDTO;
import com.jivrom.cricketminds.payment.service.ExternalPaymentService;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;

@Service
public class SessionServiceImpl implements SessionService {

    @Resource
    private SessionRepository sessionRepository;

    @Resource
    private ExternalPaymentService externalPaymentService;

    @Resource
    private SlotRepository slotRepository;

    @Resource
    private MemberPaymentFactory memberPaymentFactory;

    @Resource
    private SessionEnrollmentFactory sessionEnrollmentFactory;

    @Resource
    private SessionTypeRepository sessionTypeRepository;

    @Resource
    private MemberPaymentRepository memberPaymentRepository;

    @Resource
    private SessionEnrollmentRepository sessionEnrollmentRepository;

    @Resource
    private EmailService emailService;

    @Resource
    private MemberRepository memberRepository;

    @Resource
    private MemberSlotRepository memberSlotRepository;

    @Resource
    private MemberTypeRepository memberTypeRepository;
    
    @Resource
    private SessionSlotService sessionSlotService;
    
    @Value("${mail.from}")
	private String fromEmail;
	
    // get all sessions

    @Override
    public List<SessionDTO> getAllSessions() {
        List<SessionDTO> returnValue = new ArrayList<SessionDTO>();
        List<Session> sessions = sessionRepository.findAll();

        for (Session session : sessions) {
            SessionDTO sessionDTO = new SessionDTO(session);
            returnValue.add(sessionDTO);
        }
        return returnValue;
    }

    // creating session

    @Override
    public SessionDTO createSession(SessionRegisterDTO sessionRegisterDTO) {

        Session session = new Session();

        session.setSessionName(sessionRegisterDTO.getSessionName());
        session.setSessionPrice(sessionRegisterDTO.getSessionPrice());
        session.setStartDate(sessionRegisterDTO.getStartDate());
        session.setEndDate(sessionRegisterDTO.getEndDate());
        session.setSessionTypeId(sessionTypeRepository.findSessionTypeId(sessionRegisterDTO.getSessionType()));
        session.setCreatedDate(new Date());
        session.setUpdatedDate(new Date());
        session.setCronTab(sessionRegisterDTO.getCronTab());
        
        session.setStartTime(sessionRegisterDTO.getStartTime());
        session.setEndTime(sessionRegisterDTO.getEndTime());

        Session retVal = sessionRepository.save(session);


        for( String member : sessionRegisterDTO.getEnrolledMembers()) {
            Long memberId =  Long.valueOf(member);
            this.enrollSessionBeforePay(retVal.getSessionId(), memberId);
        }

        

        // String[] coachList = sessionRegisterDTO.getEnrolledMembers().split(",");
        // for(String coach: coachList) {
        for(String coach: sessionRegisterDTO.getAssignedCoaches()) {
            Long coachId =  Long.valueOf(coach);
            this.enrollSessionBeforePay(retVal.getSessionId(), coachId);
        }
        
        sessionSlotService.generateSlots(retVal.getSessionId());
        
        return this.getSession(retVal.getSessionId());
    }

    // get individual session using id

    @Override
    public SessionDTO getSession(Long sessionId) {
        System.out.println("testing session by id 2222222........");
        Session session = sessionRepository.findOne(sessionId);
        System.out.println("testing session by id 3333333........");
        if (session != null) {
            SessionDTO eventDTO = new SessionDTO(session);
            return eventDTO;
        } else {
            throw new SessionNotFoundException("No Session found with sessionId " + sessionId);
        }
    }

    // get individual session using slot id

    @Override
    public SessionDTO getSessionBySlotId(Long slotId) {
        Slot slot = slotRepository.findOne(slotId);
        if (slot != null) {
            Session session = sessionRepository.findOne(slot.getSessionId());
            System.out.println("testing session by id 3333333........");
            SessionDTO sessionDTO = new SessionDTO(session);

            return sessionDTO;
        } else {
            throw new EventNotFoundException("No slot found with slotId " + slotId);
        }

    }

    // find past sessions

    @Override
    public List<SessionDTO> findPastSessions() {
        List<SessionDTO> returnValue = new ArrayList<SessionDTO>();
        List<Session> sessions = sessionRepository.findPastSessions();

        for (Session session : sessions) {
            SessionDTO sessionDTO = new SessionDTO(session);
            returnValue.add(sessionDTO);
        }
        return returnValue;
    }

    // find future sessions

    @Override
    public List<SessionDTO> findFutureSessions() {
        List<SessionDTO> returnValue = new ArrayList<SessionDTO>();
        List<Session> sessions = sessionRepository.findFutureSessions();

        for (Session session : sessions) {
            SessionDTO sessionDTO = new SessionDTO(session);
            returnValue.add(sessionDTO);
        }
        return returnValue;
    }

    // find current sessions

    @Override
    public List<SessionDTO> findCurrentSessions() {
        List<SessionDTO> returnValue = new ArrayList<SessionDTO>();
        List<Session> sessions = sessionRepository.findCurrentSessions();

        for (Session session : sessions) {
            SessionDTO sessionDTO = new SessionDTO(session);
            returnValue.add(sessionDTO);
        }
        return returnValue;
    }

    // find session by status

    @Override
    public List<SessionDTO> findSessions(Long memberId, String status) {
        List<SessionDTO> returnValue = new ArrayList<SessionDTO>();

        List<Session> sessions = new ArrayList<>();

        if (status.equals("past")) {
            sessions = sessionRepository.findPastSessions(memberId);
        } else if (status.equals("future")) {
            sessions = sessionRepository.findFutureSessions(memberId);

        } else if (status.equals("current")) {
            sessions = sessionRepository.findCurrentSessions(memberId);
        }

        for (Session session : sessions) {
            SessionDTO sessionDTO = new SessionDTO(session);
            returnValue.add(sessionDTO);
        }
        return returnValue;
    }

    // get sessions by month

    @Override
    public List<SessionDTO> getSessionsByMonth(String monthOfYear) {
        List<SessionDTO> retval = new ArrayList<SessionDTO>();

        Integer month = Integer.parseInt(monthOfYear.substring(0, 2));
        // Integer day = Integer.parseInt(monthOfYear.substring(3, 5));
        Integer year = Integer.parseInt(monthOfYear.substring(3, 7));
        // System.out.println("The day is: "+day+"month is: "+month+" and the year is:
        // "+year);
        System.out.println("The month is: " + month + " and the year is: " + year);
        // List<Session> sessions =
        // sessionRepository.findSessionsByMonth(month,day,year);
        List<Session> sessions = sessionRepository.findSessionsByMonth(month, year);
        if (sessions != null) {
            for (Session session : sessions) {
                SessionDTO sessionDTO = new SessionDTO(session);
                retval.add(sessionDTO);

            }
        }
        return retval;
    }

    @Override
    public List<SessionDTO> getAvailableSessionsByDate(Date sessionDate) {
        List<SessionDTO> retval = new ArrayList<SessionDTO>();

        System.out.println("sessionDate : " + sessionDate);

        // System.out.println("The day is: "+day+"month is: "+month+" and the year is:
        // "+year);
        // List<Session> sessions =
        // sessionRepository.findSessionsByMonth(month,day,year);
        List<Session> sessions = sessionRepository.findSessionsAvailableByDate(sessionDate);
        if (sessions != null) {
            for (Session session : sessions) {
                SessionDTO sessionDTO = new SessionDTO(session);
                retval.add(sessionDTO);

            }
        }
        return retval;
    }

    // update a session

    @Override
    public SessionDTO updateSession(SessionUpdateDTO sessionUpdateDTO) {

        Session session = sessionRepository.findOne(sessionUpdateDTO.getSessionId());

        if (session != null) {
            session.setSessionTypeId(sessionTypeRepository.findSessionTypeId(sessionUpdateDTO.getSessionType()));
            session.setSessionName(sessionUpdateDTO.getSessionName());
            session.setSessionPrice(sessionUpdateDTO.getSessionPrice());
            session.setStartDate(sessionUpdateDTO.getStartDate());
            session.setEndDate(sessionUpdateDTO.getEndDate());
            session.setStartTime(sessionUpdateDTO.getStartTime());
            session.setEndTime(sessionUpdateDTO.getEndTime());

            sessionRepository.save(session);
            SessionDTO sessionDTO = new SessionDTO(session);
            return sessionDTO;
        } else {
            throw new SessionNotFoundException("No Session found with sessionId " + sessionUpdateDTO.getSessionId());
        }

    }

    /*
     * @Override public Long remainingSlots(Long eventId) {
     * 
     * Event event = eventRepository.findOne(eventId);
     * 
     * if(event!= null){
     * 
     * Long slotsUsed = eventEnrollmentRepository.findSlotsUsed(eventId);
     * 
     * Long slotsAvailable = Long.parseLong(event.getSlotsAvailable());
     * //eventRepository.findSlotsAvailable(eventId);
     * 
     * Long slotsRemain = (slotsAvailable - slotsUsed);
     * //eventRepository.save(event); //EventDTO eventDTO=new EventDTO(event);
     * 
     * return slotsRemain; }else{ throw new
     * EventNotFoundException("No Event found with eventId "+eventId); }
     * 
     * 
     * }
     */

    @Override
    public SessionDTO deleteSession(Long sessionId) {

        Session session = sessionRepository.findOne(sessionId);

        if (session != null) {
            // session.setSessionStatus(SessionStatusEnum.INACTIVE.getStatus());
            List<Slot> Slots = slotRepository.findBySessionId(sessionId);
            for( Slot slot: Slots) {
                List<MemberSlot> Memslot = memberSlotRepository.findBySlotId(slot.getSlotId());
                for(MemberSlot memslot: Memslot){
                    memberSlotRepository.delete(memslot);
                }

                slotRepository.delete(slot);
            }

            sessionRepository.delete(session);

            SessionDTO sessionDTO = new SessionDTO(session);
            return sessionDTO;

        } else {
            throw new SessionNotFoundException("No Session found with sessionId " + sessionId);
        }
    }

    @Override
    public List<UserDTO> enrolledMembers(Long sessionId, String role) {
        
        List<SessionEnrollment> enrollments = sessionEnrollmentRepository.findBySessionAndRole(sessionId, role);

        List<UserDTO> retval = new ArrayList<>();
        for (SessionEnrollment enrollment : enrollments) {
            // Member member = memberRepository.findOne(enrollment.getMember());
            Member member = enrollment.getMember();
            UserDTO userDro = new UserDTO(member);
            retval.add(userDro);
        }

        return retval;
    }

    // @Override
    // public List<UserDTO> enrolledCoaches(Long sessionId, String role) {
    //     String role = "COACH";
    //     List<SessionEnrollment> enrollments = sessionEnrollmentRepository.findBySession(sessionId, role);

    //     List<UserDTO> retval = new ArrayList<>();
    //     for (SessionEnrollment enrollment : enrollments) {
    //         // Member member = memberRepository.findOne(enrollment.getMember());
    //         Member member = enrollment.getMember();
    //         UserDTO userDro = new UserDTO(member);
    //         retval.add(userDro);
    //     }

    //     return retval;
    // }
    // pay and enroll

    @Override
    public SessionEnrollment payAndEnrollSession(Long sessionId, PaymentDTO paymentDTO) {
        try {
            Long memberId = paymentDTO.getMemberId();
            String description = paymentDTO.getDescription();
            String token = paymentDTO.getToken();
            Long amount = paymentDTO.getAmount();
            Member member = memberRepository.findOne(memberId);
            String role = memberTypeRepository.getMemberType(memberId);
            String emailId = member.getEmailId();
            String memberName = member.getFirstName() + " " + member.getLastName();
            System.out.println("member details: " + memberId + " " + amount + " " + memberName + " " + sessionId);
            List<SessionEnrollment> existingEnrollments = sessionEnrollmentRepository.findByMemberAndSession(memberId,
                    sessionId);
            if (existingEnrollments != null && existingEnrollments.size() > 0) {
                throw new RuntimeException(
                        "Session already enrolled for sessionId: " + sessionId + ", memberId:" + memberId);
            }

            // TODO: validate null for event and member
            Session session = sessionRepository.findOne(sessionId);
            String sessionName = session.getSessionName();
            // Member member = memberRepository.findOne(memberId);

            Charge charge = externalPaymentService.charge(paymentDTO);

            String receipt = charge.getReceiptUrl();

            // save the payment details
            MemberPayment memberPayment = memberPaymentFactory.create(paymentDTO, charge,
                    PaymentReason.COACHINNG_SESSION);
            MemberPayment updated = memberPaymentRepository.save(memberPayment);

            SessionEnrollment sessionEnrollment = sessionEnrollmentFactory.create(session, member, role,
                    updated.getPaymentStatus(), updated.getMemberPaymentId());
            SessionEnrollment retval = sessionEnrollmentRepository.save(sessionEnrollment);

            // Mail to the user after successful event enrollment.
            Mail mail = new Mail();
            mail.setMailFrom(fromEmail);

            mail.setMailTo(emailId);
            mail.setMailSubject("You have a new message from Cricket Minds regarding payment for session.");
            mail.setMailContent("The payment has been successful for " + sessionId + " by the user: " + "\n" + "\n"
                    + "Name : " + memberName + "\n" + "Email Id : " + emailId + "\n" + "Amount : " + amount + "\n"
                    + "Payment Receipt : " + receipt + "\n" + "\n");

            emailService.sendEmail(mail);

            // Mail to the admin or headcoach after successful event enrollment.
            Mail adminMail = new Mail();
            adminMail.setMailFrom(fromEmail);

            adminMail.setMailTo("admin@cm.com");
            adminMail.setMailSubject("Message from Cricket Minds regarding payment for session.");
            adminMail.setMailContent(" An Event enrollment has been done for the session : " + sessionName
                    + " by the user : " + "\n" + "\n" + "Name : " + memberName + "\n" + "Email Id : " + emailId + "\n"
                    + "Amount : " + amount + "\n" + "Payment Receipt : " + receipt + "\n" + "\n");

            emailService.sendEmail(adminMail);

            // save the enrollment
            return retval;

        } catch (StripeException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public SessionEnrollment getEnrollment(Long sessionId, Long memberId) {

        List<SessionEnrollment> enrollments = sessionEnrollmentRepository.findByMemberAndSession(memberId, sessionId);
        if (enrollments.size() > 0) {
            return enrollments.get(0);
        }
        return null;
    }

    @Override
    public Long remainingSlots(Long sessionId) {

        Session session = sessionRepository.findOne(sessionId);

        if (session != null) {

            Long slotsUsed = sessionEnrollmentRepository.findSessionEnrollmentUsed(sessionId);

            // Long slotsAvailable = Long.parseLong(session.getSlotsAvailable());
            // //eventRepository.findSlotsAvailable(eventId);

            // Long slotsRemain = (slotsAvailable - slotsUsed);
            // eventRepository.save(event);
            // EventDTO eventDTO=new EventDTO(event);

            return slotsUsed;
        } else {
            throw new EventNotFoundException("No Event found with eventId " + sessionId);
        }

    }

    // remove member from the session
    @Override
    public UserDTO removeMembers(Long sessionId, Long memberId) {
        String memberType = memberTypeRepository.getMemberType(memberId);
        SessionEnrollment enrollment = sessionEnrollmentRepository.findBySessionAndMember(sessionId,memberId);
        
        if (enrollment == null) {
            throw new RuntimeException(
                    "Session has no enrollments.");
         } 
         
        else {

            List<Slot> Slots = slotRepository.findBySessionId(sessionId);
            for( Slot slot: Slots) {
                MemberSlot memslot = memberSlotRepository.findByMemberId(memberId, slot.getSlotId());
                if(memslot != null) {
                    memberSlotRepository.delete(memslot);
                }
            }

            // SessionEnrollment Enrollment = new SessionEnrollment();
            // Member memb = new Member();
            // for (SessionEnrollment enrollment : enrollments) {
            // // Member member = memberRepository.findOne(enrollment.getMember());
            //     Member member = enrollment.getMember();
            //     if (member.getMemberId() == memberId) {
            //      memb = member;
            //      Enrollment = enrollment;
                
            //      break;
            //     }
            // } 
            // sessionEnrollmentRepository.delete(Enrollment.getSessionEnrollmentId());
            // UserDTO retVal = new UserDTO(memb);
            // return retVal;

            Member member = enrollment.getMember();
            UserDTO retVal = new UserDTO(member);
            sessionEnrollmentRepository.delete(enrollment);
            
            return retVal;
        }
    }

    // enroll user before pay

    @Override
    public SessionEnrollment enrollSessionBeforePay(Long sessionId, Long memberId) {
        try {

            Member member = memberRepository.findOne(memberId);
            String role = memberTypeRepository.getMemberType(memberId);
            String emailId = member.getEmailId();
            String memberName = member.getFirstName() + " " + member.getLastName();

            List<SessionEnrollment> existingEnrollments = sessionEnrollmentRepository.findByMemberAndSession(memberId,
                    sessionId);
            if (existingEnrollments != null && existingEnrollments.size() > 0) {
                throw new RuntimeException(
                        "Session already enrolled for sessionId: " + sessionId + ", memberId:" + memberId);
            }

            Session session = sessionRepository.findOne(sessionId);
            String sessionType = sessionTypeRepository.findSessionType(session.getSessionTypeId());
            String sessionName = session.getSessionName();
            String paymentStatus = "NULL";
            Long memberPaymentId = null;

            SessionEnrollment sessionEnrollment = sessionEnrollmentFactory.create(session, member, role, paymentStatus,
                    memberPaymentId);
            SessionEnrollment retVal = sessionEnrollmentRepository.save(sessionEnrollment);

            // Mail to the user after successful event enrollment.
//            if (role == "HDCOACH" || role == "COACH") {
//                Mail mail = new Mail();
//                mail.setMailFrom("jivrompvt@gmail.com");
//
//                mail.setMailTo(emailId);
//                mail.setMailSubject("Cricket Minds: Assigned to a Session.");
//                mail.setMailContent("You have been assigned to the enrollment list of session : " + sessionName + " type :  "
//                    + sessionType + " , by the Head Coach. " + "\n" + "\n" + "You are also assigned to all the corresponding slots of this session." + "\n" + "\n" +"Regards" + "\n" + "Cricket Minds");
//
//                emailService.sendEmail(mail);
//            } else {
//                Mail mail = new Mail();
//                mail.setMailFrom("jivrompvt@gmail.com");
//
//                mail.setMailTo(emailId);
//                mail.setMailSubject("You have a new message from Cricket Minds regarding enrollement for session.");
//                mail.setMailContent("You have been added to the enrollment list of session : " + sessionName + " type :  "
//                    + sessionType + " , by the admin. " + "\n" + "\n"
//                    + " Kindly pay for the session in order to avoid the automatic removal from the enrollement list for the session :"
//                    + sessionName + "\n" + "\n" + "Regards" + "\n" + "Cricket Minds");
//
//                emailService.sendEmail(mail);
//            }
        
            // Mail to the admin or headcoach after successful event enrollment.
//            Mail adminMail = new Mail();
//            adminMail.setMailFrom("jivrompvt@gmail.com");
//
//            adminMail.setMailTo("admin@cm.com");
//            adminMail.setMailSubject("Message from Cricket Minds regarding enrollment for session.");
//            adminMail.setMailContent(" An enrollment has been done for the session : " + sessionName
//                    + " for the user : " + "\n" + "\n" + "Name : " + memberName + "\n" + "Email Id : " + emailId + "\n"
//                    + "Amount : " + "NA" + "\n" + "Payment Receipt : " + "NA" + "\n" + "\n");
//
//            emailService.sendEmail(adminMail);

            // save the enrollment
            return retVal;

        } catch (RuntimeException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

}
