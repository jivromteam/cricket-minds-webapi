package com.jivrom.cricketminds.domain.service;

import java.util.List;

import com.jivrom.cricketminds.domain.dto.AssignCoachDTO;
import com.jivrom.cricketminds.domain.dto.CoachDTO;
import com.jivrom.cricketminds.domain.dto.CreateSlotDTO;
import com.jivrom.cricketminds.domain.dto.SessionDTO;
import com.jivrom.cricketminds.domain.dto.SlotDTO;
import com.jivrom.cricketminds.domain.dto.SlotDetailDTO;

public interface SessionSlotService {

    public List<SlotDTO> getAllSessionSlots();
    
    public SlotDTO createSlot(CreateSlotDTO createSlotDTO);

    public SlotDTO deleteSlot(Long slotId);

    public List<SlotDTO> getSlots(Long sessionId);

    public SlotDTO getSlotById(Long slotId);

    public CoachDTO assignCoach(AssignCoachDTO assignCoachDTO);

    public CoachDTO deleteCoach(AssignCoachDTO assignCoachDTO);

    public List<CoachDTO> getAllCoaches();
    
    public List<SlotDTO> getSlotsByMonth(String month);

    public SlotDTO updateSlots(SlotDTO SlotDTO);
    
    public SlotDetailDTO getSlotDetail(Long slotId);
    
    public Long remainingSlotBookings(Long slotId);
    
    public void generateSlots(Long sessionId);
    
}