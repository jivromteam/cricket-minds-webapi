package com.jivrom.cricketminds.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jivrom.cricketminds.domain.model.EventEnrollment;

@Repository
public interface EventEnrollmentRepository extends JpaRepository<EventEnrollment, Long> {

	@Query(nativeQuery = true, value = "SELECT * from EVENT_ENROLLMENT WHERE MEMBER_ID = :memberId AND EVENT_ID = :eventId") 
	public List<EventEnrollment> findByMemberAndEvent(@Param("memberId") Long memberId, @Param("eventId") Long eventId);

	//@Query(nativeQuery = true, value = "SELECT * from EVENT_ENROLLMENT inner join EVENT on EVENT.EVENT_ID = EVENT_ENROLLMENT.EVENT_ID WHERE MEMBER_PAYMENT_ID = :memberPaymentId") 
	//public List<EventEnrollment> findAllEvents(@Param("memberPaymentId") Long memberId);

	@Query(nativeQuery = true, value = "SELECT EVENT_ID from EVENT_ENROLLMENT  WHERE MEMBER_PAYMENT_ID = :memberPaymentId") 
	public Long findEventId( @Param("memberPaymentId") Long memberPaymentId);

	@Query(nativeQuery = true, value = "SELECT * from EVENT_ENROLLMENT  WHERE MEMBER_PAYMENT_ID = :memberPaymentId") 
	public EventEnrollment findByMemberPaymentId( @Param("memberPaymentId") Long memberPaymentId);

	@Query(nativeQuery = true, value = "SELECT COUNT(MEMBER_ID) from EVENT_ENROLLMENT WHERE EVENT_ID = :eventId AND PAYMENT_STATUS='succeeded'") 
	public Long findEventEnrollmentUsed( @Param("eventId") Long eventId);
	
//	@Query(nativeQuery = true, value = "SELECT * from EVENT_ENROLLMENT WHERE MEMBER_ID = :memberId AND EVENT_ID = :eventId")
	@Query(value = "SELECT e from EventEnrollment e WHERE e.event.eventId = :eventId")
	public List<EventEnrollment> findByEvent(@Param("eventId") Long eventId);
}
