package com.jivrom.cricketminds.domain.model;

public enum SessionTypeEnum {
    
    NULL("NULL", "NULL"),
	
	PRIVATE("1", "Private"),
	
	SEMI_PRIVATE("2", "Semi Private"),
	
    SEMI_GROUP("3", "Semi Group"),
    
    GROUP("4", "Group")
	
	;
	
	private String type;
	private String desc;
	
	SessionTypeEnum(String type, String desc) {
		this.type = type;
		this.desc = desc;
	}
	
	public String getType() {
		return this.type;
	}
	
	public String getDesc() {
		return this.desc;
	}
	
	public static SessionTypeEnum fromType(String value) {
		for(SessionTypeEnum thisType : values()) {
			if(thisType.getType().equals(value)) {
				return thisType;
			}
		}
		return NULL;
	}
	
	public boolean isNull() {
		return this == NULL;
	}
}