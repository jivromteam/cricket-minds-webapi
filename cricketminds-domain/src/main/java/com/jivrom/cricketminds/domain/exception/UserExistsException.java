package com.jivrom.cricketminds.domain.exception;

    public class UserExistsException extends RuntimeException {

        public UserExistsException(String message) {
            super(message);
        }
    
}