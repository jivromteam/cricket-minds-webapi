package com.jivrom.cricketminds.domain.model;

public enum DaysOfWeek {

	SUN("SUN", 1),
	
	MON("MON", 2),
	
	TUES("TUE", 3),
	
	WED("WED", 4),
	
	THUR("THU", 5),
	
	FRI("FRI", 6),
	
	SATUR("SAT", 7)
	
	;
	
	private String dayString;
	private Integer calendarDay;
	
	DaysOfWeek(String dayString, Integer calendarDay) {
		this.dayString = dayString;
		this.calendarDay = calendarDay;
	}

	public String getDayString() {
		return dayString;
	}

	public Integer getCalendarDay() {
		return calendarDay;
	}
	
	public static DaysOfWeek fromDayString(String day) {
		for(DaysOfWeek thisDay: values()) {
			if(thisDay.getDayString().equalsIgnoreCase(day)) {
				return thisDay;
			}
		}
		
		throw new RuntimeException("Not a valid day provided. day:"+day);
	}
	
}
