package com.jivrom.cricketminds.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jivrom.cricketminds.domain.model.Slot;


@Repository
public interface SlotRepository extends JpaRepository<Slot, Long>{
    @Query(nativeQuery = true, value = "SELECT SESSION_ID from SESSION WHERE SESSION_ID = :sessionId ")
    public Long findSessionId(@Param("sessionId") Long sessionId);

    @Query(nativeQuery = true, value = "SELECT SLOT_ID from SLOT WHERE SLOT_ID = :slotId")
    public Long findBySlotId(@Param("slotId")Long slotId);

    @Query(nativeQuery = true, value = "SELECT * from SLOT WHERE SESSION_ID = :sessionId ")
    public List<Slot> findBySessionId(@Param("sessionId") Long sessionId);

    @Query(nativeQuery = true, value = "SELECT * from SLOT WHERE YEAR(SESSION_DATE) =:year and MONTH(SESSION_DATE) =:month ")
    public List<Slot> findSlotsByMonth(@Param("month") Integer month, @Param("year") Integer year);
}
