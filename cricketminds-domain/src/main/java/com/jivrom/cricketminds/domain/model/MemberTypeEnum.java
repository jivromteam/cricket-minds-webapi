package com.jivrom.cricketminds.domain.model;

public enum MemberTypeEnum {

	NULL("NULL", "NULL"),
	
	MEMBER("MEMBER", "Member"),
	
	COACH("COACH", "Coach"),
	
	HEAD_COACH("HDCOACH", "Head Coach")
	
	;
	
	private String type;
	private String desc;
	
	MemberTypeEnum(String type, String desc) {
		this.type = type;
		this.desc = desc;
	}
	
	public String getType() {
		return this.type;
	}
	
	public String getDesc() {
		return this.desc;
	}
	
	public static MemberTypeEnum fromType(String value) {
		for(MemberTypeEnum thisType: values()) {
			if(thisType.getType().equals(value)) {
				return thisType;
			}
		}
		return NULL;
	}
	
	public boolean isNull() {
		return this == NULL;
	}
}
