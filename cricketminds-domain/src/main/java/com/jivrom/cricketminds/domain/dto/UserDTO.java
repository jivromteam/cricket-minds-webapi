package com.jivrom.cricketminds.domain.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jivrom.cricketminds.domain.model.Member;
import com.jivrom.cricketminds.domain.model.MemberType;
import com.jivrom.cricketminds.domain.model.MemberTypeEnum;

public class UserDTO {

	private Long userId;
	private String firstName;
	private String lastName;
	private String emailId;
	private String phone;
	private String status;
	private Boolean forceResetPassword;
	private String profilePictureUrl;
	@JsonFormat(pattern = "MM-dd-yyyy")
	private Date dateOfBirth;
//	private boolean isAdmin;
	
	private List<UserType> types = new ArrayList<>();
	
	public UserDTO(Member member) {
		this.firstName = member.getFirstName();
		this.lastName = member.getLastName();
		this.emailId = member.getEmailId();
		this.phone = member.getPhone();
		this.userId = member.getMemberId();
		this.status = member.getStatus();
		this.forceResetPassword=member.getForceResetPassword();
		this.profilePictureUrl=member.getProfilePictureUrl();
		this.dateOfBirth=member.getDateOfBirth();
		
//		this.isAdmin = member.getIsAdmin();
		
		if(member.hasMemberTypes()) {
			for(MemberType type : member.getMemberTypes()) {
				MemberTypeEnum typeEnum = MemberTypeEnum.fromType(type.getMemberType());
				this.types.add(new UserType(typeEnum.getType(), typeEnum.getDesc()));
			}
		}
	}

	public UserDTO() {
		
	}
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getProfilePictureUrl(){
		return profilePictureUrl;
	}

	public void setProfilePictureUrl(String profilePictureUrl){
		this.profilePictureUrl=profilePictureUrl;
	}

	public Boolean getForceResetPassword() {
		return forceResetPassword;
	}

	public void setForceResetPassword(Boolean forceResetPassword) {
		this.forceResetPassword = forceResetPassword;
	}



	

//	public boolean isAdmin() {
//		return isAdmin;
//	}
//
//	public void setAdmin(boolean isAdmin) {
//		this.isAdmin = isAdmin;
//	}
	
	public List<UserType> getTypes() {
		return types;
	}

	public void setTypes(List<UserType> types) {
		this.types = types;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}





	public static class UserType {
		
		private String type;
		private String desc;
		
		public UserType() {}
		
		public UserType(String type, String desc) {
			this.type = type;
			this.desc = desc;
		}
		
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getDesc() {
			return desc;
		}
		public void setDesc(String desc) {
			this.desc = desc;
		}
		
	}
}
