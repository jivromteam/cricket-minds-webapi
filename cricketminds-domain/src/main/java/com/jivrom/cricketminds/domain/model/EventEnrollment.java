package com.jivrom.cricketminds.domain.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="EVENT_ENROLLMENT")
public class EventEnrollment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ENROLLMENT_ID",nullable = false)
	private Long enrollmentId;
	

	@ManyToOne
	@JoinColumn(name="MEMBER_ID", referencedColumnName = "MEMBER_ID", nullable = false)
	private Member member;
	
	@ManyToOne
	@JoinColumn(name="EVENT_ID", referencedColumnName = "EVENT_ID", nullable = false)
	private Event event;
	
	//@Column(name="MEMBER_ID" )
	//private Long memberId;
	
	//@Column(name="EVENT_ID")
	//private Long eventId;
	
	@Column(name="PAYMENT_STATUS")
	private String paymentStatus;
	
	@Column(name="MEMBER_PAYMENT_ID")
	private Long memberPaymentId;
	
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	public Long getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Long enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}
	/*public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}*/

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Long getMemberPaymentId() {
		return memberPaymentId;
	}

	public void setMemberPaymentId(Long memberPaymentId) {
		this.memberPaymentId = memberPaymentId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}
