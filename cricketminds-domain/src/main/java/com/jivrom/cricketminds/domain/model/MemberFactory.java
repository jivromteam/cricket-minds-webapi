package com.jivrom.cricketminds.domain.model;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.jivrom.cricketminds.domain.dto.RegistrationDTO;

@Component
public class MemberFactory {

	public Member create(RegistrationDTO registrationDTO) {
		Member member = new Member();
		
		if(registrationDTO.hasFirstName()) {
			member.setFirstName(registrationDTO.getFirstName());
		}
		
		if(registrationDTO.hasLastName()) {
			member.setLastName(registrationDTO.getLastName());
		}
		
		if(registrationDTO.hasEmailId()) {
			member.setEmailId(registrationDTO.getEmailId());
		}
		
		if(registrationDTO.hasPassword()) {
			member.setPassword(registrationDTO.getPassword());
		}
		
		if(registrationDTO.hasPhone()) {
			member.setPhone(registrationDTO.getPhone());
		}
		
		member.setStatus(MemberStatusEnum.ACTIVE.getStatus());
		
		if(registrationDTO.hasFirstName()){

			char firstletter = member.getFirstName().toUpperCase().charAt(0);
            String profilePictureUrl="https://cricketminds.s3.ap-south-1.amazonaws.com/profilepictures/"+String.valueOf(firstletter)+".png";
			member.setProfilePictureUrl(profilePictureUrl);
		}

		if(registrationDTO.hasForceRestPassword()){

			member.setForceResetPassword(registrationDTO.getForceResetPassword());
		}else{
             member.setForceResetPassword(false);
		}

		member.setCreatedDate(new Date());
		
//		MemberType memberType = new MemberType();
//		memberType.setMemberType(MemberTypeEnum.STUDENT.getType());
//		memberType.setCreatedDate(new Date());
//		memberType.setMember(member);
//		
//		member.addMemberType(memberType);
		
		return member;
	}
}
