package com.jivrom.cricketminds.domain.service;

import java.util.Date;
import java.util.List;

import com.jivrom.cricketminds.domain.dto.SessionDTO;
import com.jivrom.cricketminds.domain.dto.SessionRegisterDTO;
import com.jivrom.cricketminds.domain.dto.SessionUpdateDTO;
import com.jivrom.cricketminds.domain.dto.UserDTO;
import com.jivrom.cricketminds.domain.model.SessionEnrollment;
import com.jivrom.cricketminds.payment.dto.PaymentDTO;

public interface SessionService {

    public List<SessionDTO> getAllSessions();

    public SessionDTO createSession(SessionRegisterDTO sessionRegisterDTO);

	public List<SessionDTO> findPastSessions();

	public List<SessionDTO> findFutureSessions();

	public List<SessionDTO> findCurrentSessions();
	
    public List<SessionDTO> findSessions(Long memberId, String status);
    
	public SessionDTO getSession(Long sessionId);

	public SessionDTO getSessionBySlotId(Long slotId);
	
	public Long remainingSlots(Long sessionId);

	public SessionDTO updateSession(SessionUpdateDTO sessionUpdateDTO);

	public SessionDTO deleteSession(Long sessionId);
	
	public SessionEnrollment payAndEnrollSession(Long session, PaymentDTO paymentDTO);

	public SessionEnrollment enrollSessionBeforePay(Long sessionId, Long memberId);
	
	public SessionEnrollment getEnrollment(Long eventId, Long memberId);
	
	public List<UserDTO> enrolledMembers(Long sessionId, String role);

	public UserDTO removeMembers(Long sessionId, Long memberId);

	public List<SessionDTO> getSessionsByMonth(String monthOfYear);
	
	public List<SessionDTO> getAvailableSessionsByDate(Date sessionDate);
}
