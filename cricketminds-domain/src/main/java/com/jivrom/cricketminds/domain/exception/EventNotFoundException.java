package com.jivrom.cricketminds.domain.exception;

public class EventNotFoundException extends RuntimeException {

	public EventNotFoundException(String message) {
		super(message);
	}
	
}