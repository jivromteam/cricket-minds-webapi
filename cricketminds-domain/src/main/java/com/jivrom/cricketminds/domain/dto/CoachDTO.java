package com.jivrom.cricketminds.domain.dto;

import java.util.ArrayList;
import java.util.List;

import com.jivrom.cricketminds.domain.model.Member;
import com.jivrom.cricketminds.domain.model.MemberType;
import com.jivrom.cricketminds.domain.model.MemberTypeEnum;

public class CoachDTO {

    
    private Long memberId;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    //private String memberType;
    private List<UserType> types = new ArrayList<>();

     public CoachDTO(Member member){

        this.memberId=member.getMemberId();
        this.firstName = member.getFirstName();
        this.lastName=member.getLastName();
        this.phone=member.getPhone();
        this.email=member.getEmailId();

        if(member.hasMemberTypes()) {
			for(MemberType type : member.getMemberTypes()) {
				MemberTypeEnum typeEnum = MemberTypeEnum.fromType(type.getMemberType());
				this.types.add(new UserType(typeEnum.getType(), typeEnum.getDesc()));
			}
		}
    }

    public CoachDTO(){
    
    }
 
    /*public CoachDTO( MemberType memberType) {
        
        MemberTypeEnum typeEnum = MemberTypeEnum.fromType(type.getMemberType());
    	this.memberType = memberType.getMemberType();
    }*/
    
    public List<UserType> getTypes() {
		return types;
	}

	public void setTypes(List<UserType> types) {
		this.types = types;
	}

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }  

    public String  getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName= firstName;
    } 

    public String  getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName= lastName;
    } 

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone= phone;
    } 

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email= email;
    } 

    /*public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType= memberType;
    } */
    
    public static class UserType {
		
		private String type;
		private String desc;
		
		public UserType() {}
		
		public UserType(String type, String desc) {
			this.type = type;
			this.desc = desc;
		}
		
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getDesc() {
			return desc;
		}
		public void setDesc(String desc) {
			this.desc = desc;
		}
		
	}
}