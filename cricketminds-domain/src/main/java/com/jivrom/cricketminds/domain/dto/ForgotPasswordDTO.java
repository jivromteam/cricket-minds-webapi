package com.jivrom.cricketminds.domain.dto;

public class ForgotPasswordDTO {

  
	private String emailId;

    public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


}