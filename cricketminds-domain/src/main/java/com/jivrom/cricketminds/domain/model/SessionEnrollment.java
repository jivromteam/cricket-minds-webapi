package com.jivrom.cricketminds.domain.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="SESSION_ENROLLMENT",
uniqueConstraints = {
	}
)

public class SessionEnrollment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="SESSION_ENROLLMENT_ID",nullable = false)
	private Long sessionEnrollmentId;
	
    //@Column(name="SESSION_ID")
    //private Long sessionId;
    
    //@Column(name="MEMBER_ID")
	//private Long memberId;
	
	@ManyToOne
	@JoinColumn(name="MEMBER_ID", referencedColumnName = "MEMBER_ID", nullable = false)
	private Member member;
	
	@ManyToOne
	@JoinColumn(name="SESSION_ID", referencedColumnName = "SESSION_ID", nullable = false)
	private Session session;

	@Column(name="PAYMENT_STATUS")
	private String paymentStatus;

    @Column(name="MEMBER_PAYMENT_ID")
	private Long memberPaymentId;
	
	@Column(name="ROLE")
    private String role;
	
	@Column(name="LAST_PAYMENT_DATE")
	private Date lastPaymentDate;

    @Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	public Long getSessionEnrollmentId() {
		return sessionEnrollmentId;
	}

	public void setSessionEnrollmentId(Long sessionEnrollmentId) {
		this.sessionEnrollmentId = sessionEnrollmentId;
	}

	/*
	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	*/

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Long getMemberPaymentId() {
		return memberPaymentId;
	}

	public void setMemberPaymentId(Long memberPaymentId) {
		this.memberPaymentId = memberPaymentId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}
	
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Date getLastPaymentDate() {
		return lastPaymentDate;
	}

	public void setLastPaymentDate(Date lastPaymentDate) {
		this.lastPaymentDate = lastPaymentDate;
	}
}
