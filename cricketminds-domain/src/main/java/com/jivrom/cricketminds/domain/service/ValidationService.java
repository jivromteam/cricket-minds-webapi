package com.jivrom.cricketminds.domain.service;

import com.jivrom.cricketminds.domain.dto.ErrorDTO;
import com.jivrom.cricketminds.domain.dto.EventRegisterDTO;
import com.jivrom.cricketminds.domain.dto.LoginDTO;
import com.jivrom.cricketminds.domain.dto.RegistrationDTO;
import com.jivrom.cricketminds.domain.dto.SessionDTO;
import com.jivrom.cricketminds.domain.dto.SessionRegisterDTO;

public interface ValidationService {

	public ErrorDTO login(LoginDTO loginDTO);

	public ErrorDTO register(RegistrationDTO registrationDTO);

	public ErrorDTO eventRegister(EventRegisterDTO eventRegisterDTO);

	public ErrorDTO sessionRegister(SessionRegisterDTO sessionRegisterDTO);

}
