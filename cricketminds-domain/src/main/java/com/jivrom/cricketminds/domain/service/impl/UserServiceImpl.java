package com.jivrom.cricketminds.domain.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.jivrom.cricketminds.domain.dto.UserDTO;
import com.jivrom.cricketminds.domain.exception.UserNotFoundException;
import com.jivrom.cricketminds.domain.model.Mail;
import com.jivrom.cricketminds.domain.model.Member;
import com.jivrom.cricketminds.domain.model.MemberStatusEnum;
import com.jivrom.cricketminds.domain.model.MemberType;
import com.jivrom.cricketminds.domain.repository.MemberRepository;
import com.jivrom.cricketminds.domain.repository.MemberTypeRepository;
import com.jivrom.cricketminds.domain.service.EmailService;
import com.jivrom.cricketminds.domain.service.UserService;

@Service
public class UserServiceImpl  implements UserService {

	@Resource
	private MemberRepository memberRepository;

	@Resource
    private EmailService emailService;
	
	@Resource
	private MemberTypeRepository memberTypeRepository;
	
	@Value("${mail.from}")
	private String fromEmail;
	
	@Override
	public List<UserDTO> getAllMembers() {
		List<UserDTO> retval = new ArrayList<UserDTO>();
		
		List<Member> members = memberRepository.findAllMembers();
		
		for(Member member: members) {
			UserDTO userDto =  new UserDTO(member);
			retval.add(userDto);
		}
		return retval;
	}
	
	@Override
	public List<UserDTO> getMembers(String status) {
		List<UserDTO> retval = new ArrayList<UserDTO>();
		
		List<Member> members = memberRepository.findByStatus(status);
		
		for(Member member: members) {
			UserDTO userDto =  new UserDTO(member);
			retval.add(userDto);
		}
		return retval;
	}

	@Override
	public UserDTO getMember(Long memberId) {
		Member member = memberRepository.findOne(memberId);
		System.out.println("member : "+member);
		if(member != null) {
			UserDTO userDto = new UserDTO(member);
			return userDto;
		} else {
			throw new UserNotFoundException("No User found with memberId "+memberId);
		}
	}

	@Override
	public UserDTO getMember(String emailId) {
		Member member = memberRepository.findByEmailId(emailId);
		System.out.println("member : "+member);
		if(member != null) {

			UserDTO userDto = new UserDTO(member);
			
			return userDto;
		} else {
			throw new UserNotFoundException("No User found with emailId "+emailId);
		}
	}

	@Transactional
	@Override
	public void updateMemberType(Long memberId, String type) {
		
		List<MemberType> memberTypes = memberTypeRepository.findByMemberId(memberId);

		if(memberTypes.size()>0){
		   MemberType memberType=memberTypes.get(0);
		   memberType.setMemberType(type);
		   memberType.setUpdatedDate(new Date());
		   memberTypeRepository.save(memberType);
		}else{
		   MemberType memberType=new MemberType();
		   memberType.setMemberId(memberId);
		   memberType.setMemberType(type);
		   memberType.setCreatedDate(new Date());
		   memberTypeRepository.save(memberType);
		}

	
	}

    @Override
	public UserDTO updateProfilePictureUrl(Long memberId, String profilePictureUrl) {
		
		Member member  = memberRepository.findOne(memberId);
		member.setProfilePictureUrl(profilePictureUrl);
		memberRepository.save(member);
		UserDTO userDTO = new UserDTO(member);
		return userDTO;
	}

	@Override
	public UserDTO updateMember(Long memberId, String firstName, String lastName,String phoneNumber, Date dateOfBirth, String profilePicUrl) {

		Member member  = memberRepository.findOne(memberId);
		//System.out.println("member : "+member);

		member.setFirstName(firstName);
		member.setLastName(lastName);
		member.setPhone(phoneNumber);
		member.setDateOfBirth(dateOfBirth);
		member.setProfilePictureUrl(profilePicUrl);
		memberRepository.save(member);

		UserDTO userDTO=new UserDTO(member);
        return userDTO;
	}

	@Override
	public UserDTO resetPassword(Long memberId, String password) {

		Member member  = memberRepository.findOne(memberId);
		member.setForceResetPassword(false);
		member.setPassword(password);
		memberRepository.save(member);

		UserDTO userDTO=new UserDTO(member);
		return userDTO;
	}

	@Override
	public UserDTO forgotPassword( String emailId) {

		Member member = memberRepository.findByEmailId(emailId);
		
		
		if (member!=null){

			String newpassword=UUID.randomUUID().toString();
			member.setPassword(newpassword);
			member.setForceResetPassword(true);
			memberRepository.save(member);

			Mail mail=new Mail();

            mail.setMailFrom(fromEmail);
    
            mail.setMailTo(emailId);
			mail.setMailSubject("You have a new message from Cricket Minds regarding resetting password");
			mail.setMailContent("Your password has been reset."+"\n"+"Your temporary password is : "+newpassword+"."+"\n"+ "Login using temporary password.");

			String message = "You have a new message from "+ mail.getMailFrom()+"\n";
			message+=mail.getMailContent();
			
            mail.setMailContent(message);
			emailService.sendEmail(mail);
			UserDTO userDto = new UserDTO(member);
			
			return userDto;
	} else{
		throw new UserNotFoundException("No User found with emailId "+emailId);

	}
	
}

	@Override
	public UserDTO deleteMember(Long memberId) {

		Member member = memberRepository.findOne(memberId);

		if (member!=null){
		member.setStatus(MemberStatusEnum.INACTIVE.getStatus());
		memberRepository.save(member);

		UserDTO userDTO=new UserDTO(member);
		return userDTO;

		}else {
			throw new UserNotFoundException("No User found with memberId "+memberId);
		}
	
		
	}
}
