package com.jivrom.cricketminds.domain.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jivrom.cricketminds.domain.model.Session;
import com.jivrom.cricketminds.domain.model.Slot;

public class SlotDTO {

    private Long slotId;
    private Long sessionId;
    @JsonFormat(pattern = "MM-dd-yyyy")
    private Date sessionDate;
    @JsonFormat(pattern = "MM-dd-yyyy HH:mm")
	private Date startTime;
    @JsonFormat(pattern = "MM-dd-yyyy HH:mm")
    private Date endTime;
	private String status;
	private Long maxEnrollment;
	
	private String sessionType;
	private String sessionName;



    public SlotDTO(Slot slot) {
        this.slotId = slot.getSlotId();
        this.sessionId=slot.getSessionId();
        this.sessionDate = slot.getSessionDate();
        this.startTime=slot.getStartTime();
        this.endTime=slot.getEndTime();
        this.maxEnrollment=slot.getMaxEnrollment();
	}  
    
    public SlotDTO(Slot slot,  Session session) {
    	this(slot);
    	
    	this.sessionName = session.getSessionName();
    	this.sessionType = session.getSessionType().getType();
    }

    public SlotDTO() {
        
    }
  

	public Long getSlotId() {
        return slotId;
    }

    public void setSlotId(Long slotId) {
        this.slotId = slotId;
    }  
    
    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }  

    public Date getSessionDate() {
        return sessionDate;
    }

    public void setSessionDate(Date sessionDate) {
        this.sessionDate = sessionDate;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getMaxEnrollment() {
        return maxEnrollment;
    }

    public void setMaxEnrollment(Long maxEnrollment) {
        this.maxEnrollment = maxEnrollment;
    }

	public String getSessionType() {
		return sessionType;
	}

	public void setSessionType(String sessionType) {
		this.sessionType = sessionType;
	}

	public String getSessionName() {
		return sessionName;
	}

	public void setSessionName(String sessionName) {
		this.sessionName = sessionName;
	}

}