package com.jivrom.cricketminds.domain.service;

import java.util.Date;
import java.util.List;

import com.jivrom.cricketminds.domain.dto.UserDTO;
import com.jivrom.cricketminds.domain.model.MemberType;

public interface UserService {

	public List<UserDTO> getAllMembers();
	
	public List<UserDTO> getMembers(String status);
	
	public UserDTO getMember(Long memberId);
	
	public UserDTO getMember(String emailId);
	
	public void updateMemberType(Long memberId, String type);

	public UserDTO updateProfilePictureUrl(Long memberId, String profilePictureUrl);

	public UserDTO updateMember(Long memberId,String firstName, String lastName,String phoneNumber, Date dateOfBirth, String profilePicUrl);

	public UserDTO resetPassword(Long memberId, String password);

	public UserDTO forgotPassword(String emailId);

	public UserDTO deleteMember(Long memberId);

}
