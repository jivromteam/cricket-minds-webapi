package com.jivrom.cricketminds.domain.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="MEMBER_PAYMENT")
public class MemberPayment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="MEMBER_PAYMENT_ID",nullable = false)
	private Long memberPaymentId;
	
	@Column(name="MEMBER_ID",nullable = false)
	private Long memberId;
	
	@ManyToOne
	@JoinColumn(name="MEMBER_ID", referencedColumnName = "MEMBER_ID", nullable = false, insertable = false, updatable = false)
	private Member member;
	
	@Column(name="PYMNT_ID")
	private String paymentId;
	
	@Column(name="PYMNT_REASON")
	private String paymentReason;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="RECEIPT_NUMBER")
	private String receiptNumber;
	
	@Column(name="RECEIPT_URL")
	private String receiptUrl;
	
	@Column(name="AMOUNT")
	private String amount;
	
	@Column(name="CURRENCY")
	private String currency;
	
	@Column(name="PYMNT_STATUS")
	private String paymentStatus;
	
	@Column(name="PYMNT_RESPONSE")
	private String paymentResponse;
	
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	public Long getMemberPaymentId() {
		return memberPaymentId;
	}

	public void setMemberPaymentId(Long memberPaymentId) {
		this.memberPaymentId = memberPaymentId;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getPaymentReason() {
		return paymentReason;
	}

	public void setPaymentReason(String paymentReason) {
		this.paymentReason = paymentReason;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getReceiptUrl() {
		return receiptUrl;
	}

	public void setReceiptUrl(String receiptUrl) {
		this.receiptUrl = receiptUrl;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPaymentResponse() {
		return paymentResponse;
	}

	public void setPaymentResponse(String paymentResponse) {
		this.paymentResponse = paymentResponse;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
