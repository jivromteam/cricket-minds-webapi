package com.jivrom.cricketminds.domain.dto;

import java.util.Map;

public class ResponseDTO {

	private ErrorDTO error;
	private Map<String,Object> response;
	public ErrorDTO getError() {
		return error;
	}
	public void setError(ErrorDTO error) {
		this.error = error;
	}
	public Map<String, Object> getResponse() {
		return response;
	}
	public void setResponse(Map<String, Object> data) {
		this.response = data;
	}
	
	
	
}
