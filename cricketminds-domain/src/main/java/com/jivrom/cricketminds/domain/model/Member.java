package com.jivrom.cricketminds.domain.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="MEMBER",
uniqueConstraints = {
	}
)
public class Member {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="MEMBER_ID",nullable = false)
	private Long memberId;

	@Column(name="EMAIL")
	private String emailId;
	
	@Column(name="FIRST_NAME")
	private String firstName;
	
	@Column(name="LAST_NAME")
	private String lastName;
	
	@Column(name="PHONE_NUMBER")
	private String phone;

	// @Column(name="ALTERNATE_PHONE_NUMBER")
	// private String alternatePhone;

	@Column(name="PASSWORD")
	private String password;
	
	@Column(name="FORCE_RESET_PASSWORD",nullable = false)
	private Boolean forceResetPassword;
	
	@Column(name="STATUS")
	private String status;

	@Column(name="ISADMIN")
	private Boolean isAdmin;

	@Column(name="PROFILE_PICTURE_URL")
	private String profilePictureUrl;

	@Column(name="DATE_OF_BIRTH")
	private Date dateOfBirth;

	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	@OneToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "MEMBER_ID", nullable = false, insertable = false, updatable = false)
    @Fetch(value = FetchMode.SUBSELECT)
	private List<MemberType> memberTypes = new ArrayList<MemberType>();
	

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	// public String getAlternatePhone() {
	// 	return alternatePhone;
	// }

	// public void setAlternatePhone(String alternatePhone) {
	// 	this.alternatePhone = alternatePhone;
	// }
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getForceResetPassword() {
		return forceResetPassword;
	}

	public void setForceResetPassword(Boolean forceResetPassword) {
		this.forceResetPassword = forceResetPassword;
	}

	public String getProfilePictureUrl() {
		return profilePictureUrl;
	}

	public void setProfilePictureUrl(String profilePictureUrl) {
		this.profilePictureUrl = profilePictureUrl;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public boolean hasMemberTypes() {
		return this.memberTypes != null && !this.memberTypes.isEmpty();
	}
	public List<MemberType> getMemberTypes() {
		return memberTypes;
	}

	public void setMemberTypes(List<MemberType> memberTypes) {
		this.memberTypes = memberTypes;
	}
	
	public void addMemberType(MemberType memberType) {
		this.memberTypes.add(memberType);
	}

}
