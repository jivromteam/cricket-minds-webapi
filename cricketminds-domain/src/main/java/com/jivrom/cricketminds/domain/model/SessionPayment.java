package com.jivrom.cricketminds.domain.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="SESSION_PAYMENT",
uniqueConstraints = {
	}
)

public class SessionPayment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="SESSION_PAYMENT_ID",nullable = false)
	private Long sessionPaymentId;
	
    @Column(name="MEMBER_ID")
	private Long memberId;
    
    @Column(name="SESSION_ID")
    private Long sessionId;
    
    @ManyToOne
	@JoinColumn(name="SESSION_ID", referencedColumnName = "SESSION_ID", nullable = false, insertable = false, updatable = false)
	private Session session;
    
    @ManyToOne
	@JoinColumn(name="MEMBER_ID", referencedColumnName = "MEMBER_ID", nullable = false, insertable = false, updatable = false)
	private Member member;
    
    @Column(name="PAYMENT_DUE_DATE")
    private Date paymentDueDate;
    
    @Column(name="AMOUNT")
    private String amountDue;
    
    @Column(name="PAYMENT_MONTH")
    private String paymentMonth;
    
    @Column(name="MEMBER_PAYMENT_ID")
	private Long memberPaymentId;

	@Column(name="STATUS")
	private String status;
    
    @Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	public Long getSessionPaymentId() {
		return sessionPaymentId;
	}

	public void setSessionPaymentId(Long sessionPaymentId) {
		this.sessionPaymentId = sessionPaymentId;
	}
	
	public String getAmountDue() {
		return amountDue;
	}

	public void setAmountDue(String amountDue) {
		this.amountDue = amountDue;
	}

	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getMemberPaymentId() {
		return memberPaymentId;
	}

	public void setMemberPaymentId(Long memberPaymentId) {
		this.memberPaymentId = memberPaymentId;
	}
	
	public Date getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
    }
    
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getPaymentMonth() {
		return paymentMonth;
	}

	public void setPaymentMonth(String paymentMonth) {
		this.paymentMonth = paymentMonth;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
}
