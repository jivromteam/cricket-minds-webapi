package com.jivrom.cricketminds.domain.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class SessionRegisterDTO {

    private String cronTab;
    private String sessionName;
    private Long sessionTypeId;
    private String sessionType;
    private String sessionPrice;
	  private String slotsAvailable;
    @JsonFormat(pattern = "MM-dd-yyyy HH:mm")
    private Date startDate;
    
    @JsonFormat(pattern = "MM-dd-yyyy HH:mm")
    private Date endDate;
    
    // @JsonFormat(pattern = "MM-dd-yyyy HH:mm")

    @JsonFormat(pattern = "HH:mm")
    private Date startTime;

    @JsonFormat(pattern = "HH:mm")
    private Date endTime;

    private String[] enrolledMembers;
    private String[] assignedCoaches;
    
	  public String getSessionName() {
      return sessionName;
    }

    public void setSessionName(String sessionName) {
		  this.sessionName = sessionName;
    }

    public String getSessionPrice() {
      return sessionPrice;
    }

    public void setSessionPrice(String sessionPrice) {
		  this.sessionPrice = sessionPrice;
    }

    public Date getStartDate() {
		  return startDate;
	  }

	  public void setStartDate(Date startDate) {
		  this.startDate = startDate;
    }

    public Date getEndDate() {
		  return endDate;
	  }

	  public void setEndDate(Date endDate) {
		  this.endDate = endDate;
    }

    public String getSlotsAvailable() {
		  return slotsAvailable;
	  }

	  public void setSlotsAvailable(String slotsAvailable) {
		  this.slotsAvailable = slotsAvailable;
    }

    public String getSessionType() {
      return sessionType;
    }
  
    public void setSessionType(String sessionType) {
      this.sessionType = sessionType;
    }

    public Long getSessionTypeId() {
      return sessionTypeId;
    }

    public void setSessionTypeId(Long sessionTypeId) {
      this.sessionTypeId = sessionTypeId;
    }  

    public String getCronTab() {
      return cronTab;
    }

    public void setCronTab(String cronTab) {
		  this.cronTab = cronTab;
    }

    public Date getStartTime() {
      return startTime;
    }
  
    public void setStartTime(Date startTime) {
      this.startTime = startTime;
    }
  
    public Date getEndTime() {
      return endTime;
    }
  
    public void setEndTime(Date endTime) {
      this.endTime = endTime;
    }

    public String[] getEnrolledMembers() {
      return enrolledMembers;
    }

    public void setEnrolledMembers(String[] enrolledMembers) {
      this.enrolledMembers = enrolledMembers;
    }

    public String[] getAssignedCoaches() {
      return assignedCoaches;
    }

    public void setAssignedCoaches(String[] assignedCoaches) {
      this.assignedCoaches = assignedCoaches;
    }

    // public List<UserType> getTypes() {
    //   return types;
    // }
  
    // public void setTypes(List<UserType> types) {
    //   this.types = types;
    // }

    // public static class ListType {
		
    //   private String type;
      
    //   public UserType() {}
      
    //   public UserType(String type) {
    //     this.type = type;
    //   }
      
    //   public String getType() {
    //     return type;
    //   }
    //   public void setType(String type) {
    //     this.type = type;
    //   }
      
      
    // }
}