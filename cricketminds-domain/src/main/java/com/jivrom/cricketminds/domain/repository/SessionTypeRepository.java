package com.jivrom.cricketminds.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jivrom.cricketminds.domain.model.SessionType;

@Repository
public interface SessionTypeRepository extends JpaRepository<SessionType, Long>{

    @Query(nativeQuery = true, value = "SELECT SESSION_TYPE_ID from SESSION_TYPE  WHERE TYPE = :sessionType ")
    public Long findSessionTypeId(@Param("sessionType")String sessionType); 
    
    @Query(nativeQuery = true, value = "SELECT TYPE from SESSION_TYPE  WHERE SESSION_TYPE_ID  = :sessionTypeId ")
    public String findSessionType(@Param("sessionTypeId")Long sessionTypeId);
}
