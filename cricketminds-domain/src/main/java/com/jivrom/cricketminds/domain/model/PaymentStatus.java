package com.jivrom.cricketminds.domain.model;

public enum PaymentStatus {

	DUE("Due"),
	
	PAID("Paid"),
	
	PENDING("Pending"),
	
	PAID_IN_CASH("Paid In Cash")
	
	;
	
	private String status;
	
	PaymentStatus(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return this.status;
	}
}
