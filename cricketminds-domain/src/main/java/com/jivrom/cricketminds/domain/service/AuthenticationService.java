package com.jivrom.cricketminds.domain.service;

import com.jivrom.cricketminds.domain.dto.LoginDTO;
import com.jivrom.cricketminds.domain.dto.RegistrationDTO;
import com.jivrom.cricketminds.domain.dto.UserDTO;
import com.jivrom.cricketminds.domain.model.Member;

public interface AuthenticationService {

	public UserDTO login(LoginDTO loginDTO);
	
	public Member register(RegistrationDTO registrationDTO);
}
