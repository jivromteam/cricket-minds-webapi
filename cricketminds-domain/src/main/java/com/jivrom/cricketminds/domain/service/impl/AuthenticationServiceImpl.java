package com.jivrom.cricketminds.domain.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.jivrom.cricketminds.domain.dto.LoginDTO;
import com.jivrom.cricketminds.domain.dto.RegistrationDTO;
import com.jivrom.cricketminds.domain.dto.UserDTO;
import com.jivrom.cricketminds.domain.exception.UserExistsException;
import com.jivrom.cricketminds.domain.model.Mail;
import com.jivrom.cricketminds.domain.model.Member;
import com.jivrom.cricketminds.domain.model.MemberFactory;
import com.jivrom.cricketminds.domain.model.MemberStatusEnum;
import com.jivrom.cricketminds.domain.model.MemberType;
import com.jivrom.cricketminds.domain.model.MemberTypeEnum;
import com.jivrom.cricketminds.domain.repository.MemberRepository;
import com.jivrom.cricketminds.domain.repository.MemberTypeRepository;
import com.jivrom.cricketminds.domain.service.AuthenticationService;
import com.jivrom.cricketminds.domain.service.EmailService;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

	@Resource
	private MemberRepository memberRepository;

	@Resource
	private MemberFactory memberFactory;
	
	@Resource
	private MemberTypeRepository memberTypeRepository;

	@Resource
    private EmailService emailService;
	
	@Value("${mail.from}")
	private String fromEmail;
	
	@Value("${mail.admin}")
	private String adminEmail;
	
	
	@Override
	public UserDTO login(LoginDTO loginDTO) {
		// TODO Auto-generated method stub

		Member retval = memberRepository.findByEmailId(loginDTO.getUserName());
		if (retval != null) {

				if (retval.getStatus().equalsIgnoreCase(MemberStatusEnum.ACTIVE.getStatus())) {
					UserDTO user = new UserDTO(retval);
					return user;
				} else {
					throw new RuntimeException("User is not allowed to login");
				}


		} else {
			// TODO: throw no user found exception
			throw new RuntimeException("No User found");
		}
	}

	@Transactional
	@Override
	public Member register(RegistrationDTO registrationDTO) {
		// TODO: add validations.

	
		Member member=memberRepository.findByEmailId(registrationDTO.getEmailId());
		 if(member!=null){
          if(member.getStatus().equalsIgnoreCase(MemberStatusEnum.INACTIVE.getStatus())){
			   member.setStatus(MemberStatusEnum.ACTIVE.name());
			   member.setFirstName(registrationDTO.getFirstName());
			   member.setLastName(registrationDTO.getLastName());
			   member.setPassword(registrationDTO.getPassword());
			   member.setPhone(registrationDTO.getPhone());
			//    member.setAlternatePhone(registrationDTO.getAlternatePhone());
			   member.setForceResetPassword(false);
			   member.setUpdatedDate(new Date());
			   //memberRepository.save(member);
			   List<MemberType> memberTypes= memberTypeRepository.findByMemberId(member.getMemberId());
			   //deleting existing usertype
			   for(MemberType memberType: memberTypes) {
				memberTypeRepository.delete(memberType.getMemberTypeId());
			}
		  }else{
			throw new UserExistsException("User already exists with this emailId"+registrationDTO.getEmailId());
		  }
		 }else{
			 member=memberFactory.create(registrationDTO);
		 }

//		MemberType memberType = new MemberType();
//		memberType.setMemberType(MemberTypeEnum.STUDENT.getType());
//		memberType.setMember(member);
		

		Member retval=memberRepository.save(member);
		
        MemberType memberType = new MemberType();
		memberType.setMemberType(MemberTypeEnum.MEMBER.getType());
		memberType.setCreatedDate(new Date());
		memberType.setMemberId(retval.getMemberId());
		
		member.addMemberType(memberType);
		
		memberType = memberTypeRepository.save(memberType);
		retval.addMemberType(memberType);

		Mail mail=new Mail();

		mail.setMailFrom(fromEmail);

		mail.setMailTo(retval.getEmailId());
		mail.setMailSubject("Welcome to Cricket minds");

		String message = "Thank you for registering to Cricket Minds! We will contact you as soon as possible";
		mail.setMailContent(message);
		emailService.sendEmail(mail);
		
		mail.setMailTo(adminEmail);
		mail.setMailSubject("New user registration from Cricket Minds");

		String adminmessage = "New user has registered and waiting for your response."+"\n"+"Registration Details :"+"\n"+"First Name: "+member.getFirstName()+"\n"+"Last Name: "+member.getLastName()+"\n"+"Email: "+member.getEmailId()+"\n"+"Phone: "+member.getPhone();
		mail.setMailContent(adminmessage);
		emailService.sendEmail(mail);

		return retval;
	}

}
