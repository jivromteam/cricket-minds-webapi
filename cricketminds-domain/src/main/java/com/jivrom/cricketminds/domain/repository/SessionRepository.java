package com.jivrom.cricketminds.domain.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jivrom.cricketminds.domain.model.Session;

@Repository
public interface SessionRepository extends JpaRepository<Session, Long>{
    
    @Query(nativeQuery = true, value = "SELECT * from SESSION WHERE END_DATE< curdate()") 	
    public List<Session> findPastSessions(); 

    @Query(nativeQuery = true, value = "SELECT * from SESSION WHERE START_DATE > curdate()") 	
    public List<Session> findFutureSessions(); 	 
    	
    @Query(nativeQuery = true, value = "SELECT * from SESSION WHERE START_DATE <= curdate() AND END_DATE >curdate()") 	
    public List<Session> findCurrentSessions();
    
    @Query(nativeQuery = true, value = "SELECT NAME from SESSION WHERE SESSION_ID = :sessionId") 	
    public String findSessionName( @Param("sessionId") Long sessionId);
    
    @Query(nativeQuery = true, value = "SELECT SESSION_TYPE_ID from SESSION WHERE SESSION_ID = :sessionId") 	
    public Long findSessionTypeId( @Param("sessionId") Long sessionId);

    @Query(nativeQuery = true, value = "SELECT SLOTS_AVAILABLE from SESSION WHERE SESSION_ID = :sessionId") 	
    Long findSessionEnrollmentAvailable( @Param("sessionId") Long sessionId);
    
    @Query(nativeQuery = true, value = "SELECT s.* from SESSION s inner join SESSION_ENROLLMENT sn on s.SESSION_ID = sn.SESSION_ID WHERE sn.MEMBER_ID = :memberId and s.END_DATE< curdate()") 	
    public List<Session> findPastSessions(@Param("memberId")Long memberId); 
    
    @Query(nativeQuery = true, value = "SELECT * from SESSION s inner join SESSION_ENROLLMENT sn on s.SESSION_ID = sn.SESSION_ID WHERE sn.MEMBER_ID = :memberId AND s.START_DATE > curdate()")
    public List<Session> findFutureSessions(@Param("memberId")Long memberId); 	
    
    @Query(nativeQuery = true, value = "SELECT * from SESSION s inner join SESSION_ENROLLMENT sn on s.SESSION_ID = sn.SESSION_ID WHERE sn.MEMBER_ID = :memberId AND s.START_DATE <= curdate() AND s.END_DATE >curdate()")
    public List<Session> findCurrentSessions(@Param("memberId")Long memberId);  

    /*@Query(value = "Select * from Official o inner join OfficialAuthentication oA on o.OfficialId = oA.OfficialId and o.OfficialId= :officialId ", nativeQuery = true)
    public Official findByOfficialId(@Param("officialId") Long officialId);  */
    
    //@Query(nativeQuery = true, value = "SELECT * from SESSION WHERE :day BETWEEN DAY(START_DATE) and DAY(END_DATE) and :year BETWEEN YEAR(START_DATE) and YEAR(END_DATE) and :month BETWEEN MONTH(START_DATE) and MONTH(END_DATE)")
    //public List<Session> findSessionsByMonth(@Param("month") Integer month, @Param("day") Integer day, @Param("year") Integer year);

    @Query(nativeQuery = true, value = "SELECT * from SESSION WHERE :year BETWEEN YEAR(START_DATE) and YEAR(END_DATE) and :month BETWEEN MONTH(START_DATE) and MONTH(END_DATE)")
    public List<Session> findSessionsByMonth(@Param("month") Integer month, @Param("year") Integer year);
    
    @Query(nativeQuery = true, value = "SELECT * from SESSION WHERE START_DATE <= :sessionDate and END_DATE >= :sessionDate")
    public List<Session> findSessionsAvailableByDate(@Param("sessionDate") Date sessionDate);
}
