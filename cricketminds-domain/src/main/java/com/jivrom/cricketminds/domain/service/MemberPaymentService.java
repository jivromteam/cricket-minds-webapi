package com.jivrom.cricketminds.domain.service;

import java.util.List;

import com.jivrom.cricketminds.domain.dto.MemberPaymentDTO;

public interface MemberPaymentService {

	public List<MemberPaymentDTO> getAllPayments();
	
	public List<MemberPaymentDTO> getAllPayments(Long memberId);
}
