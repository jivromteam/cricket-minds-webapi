package com.jivrom.cricketminds.domain.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jivrom.cricketminds.domain.model.Member;
import com.jivrom.cricketminds.domain.model.MemberPayment;
public class MemberPaymentDTO {

	// member info
	private Long userId;
	private String firstName;
	private String lastName;
	private String emailId;
	
	// payment info
	private String eventName;
	//private String sessionName;
	private Long memberPaymentId;
	private String paymentId;
	private String paymentReason;
	private String receiptNumber;
	private String receiptUrl;
	private Long amount;
	private String paymentStatus;
	
	@JsonFormat(pattern = "MM-dd-yyyy HH:mm")
	private Date date;
	
	private String description;
	
	public MemberPaymentDTO(MemberPayment memberPayment) {
		this.memberPaymentId = memberPayment.getMemberPaymentId();
		this.paymentId = memberPayment.getPaymentId();
		this.paymentReason = memberPayment.getPaymentReason();
		this.receiptNumber = memberPayment.getReceiptNumber();
		this.receiptUrl = memberPayment.getReceiptUrl();
		this.amount = Long.parseLong(memberPayment.getAmount());
		this.paymentStatus = memberPayment.getPaymentStatus();
		this.date = memberPayment.getCreatedDate();
		this.description = memberPayment.getDescription();
		
		Member member = memberPayment.getMember();
		this.firstName = member.getFirstName();
		this.lastName = member.getLastName();
		this.userId  = member.getMemberId();
		
		//TODO: Event_enrollment_repo. getbymemberPaymentid -> event_enrollment_obj...event_id, Event_repo. findOne (EventId) -> event_Name, this.eventName = event.event_Name
	
	}
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getEventName(){
		return eventName;
	}
	public void setEventName(String eventName){
		this.eventName = eventName;
	}
	/*public String getSessionName(){
		return sessionName;
	}
	public void setSessionName(String sessionName){
		this.sessionName = sessionName;
	} */
	public Long getMemberPaymentId() {
		return memberPaymentId;
	}
	public void setMemberPaymentId(Long memberPaymentId) {
		this.memberPaymentId = memberPaymentId;
	}
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	public String getPaymentReason() {
		return paymentReason;
	}
	public void setPaymentReason(String paymentReason) {
		this.paymentReason = paymentReason;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getReceiptUrl() {
		return receiptUrl;
	}
	public void setReceiptUrl(String receiptUrl) {
		this.receiptUrl = receiptUrl;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
