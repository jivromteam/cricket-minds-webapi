package com.jivrom.cricketminds.domain.service;

import java.util.List;

import com.jivrom.cricketminds.domain.dto.PaymentDueDTO;
import com.jivrom.cricketminds.domain.dto.SessionPaymentDTO;
import com.jivrom.cricketminds.payment.dto.MemberSessionPaymentDTO;

public interface PaymentService {

	public PaymentDueDTO getPaymentDue(Long memberId);
	
	public List<SessionPaymentDTO> getPaymentDue();
	
	public void generatePaymentDue(Long sessionId, String month, List<Long> memberIds, String amount);
	
	public void makeSessionPayment(Long memberId, MemberSessionPaymentDTO sessionPaymentDTO);
}
