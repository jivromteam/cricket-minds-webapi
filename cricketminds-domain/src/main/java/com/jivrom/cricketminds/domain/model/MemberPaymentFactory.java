package com.jivrom.cricketminds.domain.model;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.jivrom.cricketminds.payment.dto.PaymentDTO;
import com.stripe.model.Charge;

@Component
public class MemberPaymentFactory {

	public MemberPayment create(PaymentDTO paymentDTO, Charge charge, PaymentReason paymentReason) {
		MemberPayment memberPayment = new MemberPayment();
		memberPayment.setMemberId(paymentDTO.getMemberId());
		memberPayment.setPaymentReason(paymentReason.getReason());
		
		memberPayment.setDescription(paymentDTO.getDescription());
		
		memberPayment.setPaymentId(charge.getId());
		memberPayment.setReceiptNumber(charge.getReceiptNumber());
		memberPayment.setReceiptUrl(charge.getReceiptUrl());
		memberPayment.setAmount(Long.toString(charge.getAmount() / 100)); // convert into dollor
		memberPayment.setCurrency(charge.getCurrency());
		memberPayment.setPaymentStatus(charge.getStatus());
		
		memberPayment.setPaymentResponse(charge.toJson());
		
		memberPayment.setCreatedDate(new Date());
		
		return memberPayment;
	}
}
