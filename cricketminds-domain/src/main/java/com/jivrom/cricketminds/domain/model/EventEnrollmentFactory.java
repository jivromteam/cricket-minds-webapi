package com.jivrom.cricketminds.domain.model;

import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class EventEnrollmentFactory {

	public EventEnrollment create(Event event, Member member, String paymentStatus, Long paymentId) {
		EventEnrollment eventEnrollment = new EventEnrollment();
		
//		eventEnrollment.setEventId(eventId);
//		eventEnrollment.setMemberId(memberId);
		
		eventEnrollment.setEvent(event);
		
		eventEnrollment.setMember(member);
		
		
		eventEnrollment.setPaymentStatus(paymentStatus);
		eventEnrollment.setMemberPaymentId(paymentId);
		eventEnrollment.setCreatedDate(new Date());
		
		return eventEnrollment;
	}
}
