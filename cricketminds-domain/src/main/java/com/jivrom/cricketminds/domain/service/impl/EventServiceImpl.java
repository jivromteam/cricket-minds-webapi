package com.jivrom.cricketminds.domain.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.jivrom.cricketminds.domain.dto.EventDTO;
import com.jivrom.cricketminds.domain.dto.EventRegisterDTO;
import com.jivrom.cricketminds.domain.dto.EventUpdateDTO;
import com.jivrom.cricketminds.domain.dto.ResponseDTO;
import com.jivrom.cricketminds.domain.dto.UserDTO;
import com.jivrom.cricketminds.domain.exception.EventNotFoundException;
import com.jivrom.cricketminds.domain.exception.UserNotFoundException;
import com.jivrom.cricketminds.domain.model.Event;
import com.jivrom.cricketminds.domain.model.EventEnrollment;
import com.jivrom.cricketminds.domain.model.EventEnrollmentFactory;
import com.jivrom.cricketminds.domain.model.EventStatusEnum;
import com.jivrom.cricketminds.domain.model.Mail;
import com.jivrom.cricketminds.domain.model.Member;
import com.jivrom.cricketminds.domain.model.MemberPayment;
import com.jivrom.cricketminds.domain.model.MemberPaymentFactory;
import com.jivrom.cricketminds.domain.model.PaymentReason;
import com.jivrom.cricketminds.domain.repository.EventEnrollmentRepository;
import com.jivrom.cricketminds.domain.repository.EventRepository;
import com.jivrom.cricketminds.domain.repository.MemberPaymentRepository;
import com.jivrom.cricketminds.domain.repository.MemberRepository;
import com.jivrom.cricketminds.domain.service.EmailService;
import com.jivrom.cricketminds.domain.service.EventService;
import com.jivrom.cricketminds.domain.service.UserService;
import com.jivrom.cricketminds.payment.dto.PaymentDTO;
import com.jivrom.cricketminds.payment.service.ExternalPaymentService;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;

@Service
public class EventServiceImpl implements EventService {

    @Resource
    private EventRepository eventRepository;
    
    @Resource
    private ExternalPaymentService externalPaymentService;
    
    @Resource
    private MemberPaymentFactory memberPaymentFactory;
    
    @Resource
    private MemberPaymentRepository memberPaymentRepository;
    
    @Resource
    private EventEnrollmentFactory eventEnrollmentFactory;
    
    @Resource
    private EventEnrollmentRepository eventEnrollmentRepository;
    
    @Resource
    private EmailService emailService;
    
    @Resource
    private MemberRepository memberRepository;
    
    @Value("${mail.from}")
	private String fromEmail;
	
	@Value("${mail.admin}")
	private String adminEmail;

    @Override
    public EventDTO createEvent(EventRegisterDTO eventRegisterDTO) {

        Event event = new Event();
        event.setEventName(eventRegisterDTO.getEventName());
        event.setEventDescription(eventRegisterDTO.getEventDescription());
        event.setLocation(eventRegisterDTO.getLocation());
        event.setFee(eventRegisterDTO.getFee());
        event.setStartDate(eventRegisterDTO.getStartDate());
        event.setEndDate(eventRegisterDTO.getEndDate());
        event.setEnrollmentEndDate(eventRegisterDTO.getEnrollmentEndDate());
        event.setSlotsAvailable(eventRegisterDTO.getSlotsAvailable());
        event.setEventBannerUrl(eventRegisterDTO.getEventBannerUrl());
        event.setCreatedDate(new Date());
        event.setEventStatus("Active");

        Event retVal = eventRepository.save(event);
        EventDTO eDto = new EventDTO(retVal);
        return eDto;
    }

    @Override
    public List<EventDTO> getAllEvents() {
        List<EventDTO> returnValue = new ArrayList<EventDTO>();
        List<Event> events = eventRepository.findAll();

        for(Event event: events) {
			EventDTO eventDTO =  new EventDTO(event);
			returnValue.add(eventDTO);
		}
        return returnValue;
    }

    @Override
    public EventDTO getEvent(Long eventId) {
        Event event = eventRepository.findOne(eventId);

        if(event != null) {
			EventDTO eventDTO = new EventDTO(event);
			return eventDTO;
		} else {
			throw new EventNotFoundException("No Event found with eventId "+eventId);
        }
    }


    @Override
    public List<EventDTO> findPastEvents() {
        List<EventDTO> returnValue = new ArrayList<EventDTO>();
        List<Event> events = eventRepository.findPastEvents();

        for(Event event: events) {
			EventDTO eventDTO =  new EventDTO(event);
			returnValue.add(eventDTO);
		}
        return returnValue;
    }

    @Override
    public List<EventDTO> findFutureEvents() {
        List<EventDTO> returnValue = new ArrayList<EventDTO>();
        List<Event> events = eventRepository.findFutureEvents();

        for(Event event: events) {
			EventDTO eventDTO =  new EventDTO(event);
			returnValue.add(eventDTO);
		}
        return returnValue;
    }

    @Override
    public List<EventDTO> findCurrentEvents() {
        List<EventDTO> returnValue = new ArrayList<EventDTO>();
        List<Event> events = eventRepository.findCurrentEvents();

        for(Event event: events) {
			EventDTO eventDTO =  new EventDTO(event);
			returnValue.add(eventDTO);
		}
        return returnValue;
    }
    
    @Override
    public List<EventDTO> findEvents(Long memberId, String status) {
    	List<EventDTO> returnValue = new ArrayList<EventDTO>();
    	
    	List<Event> events = new ArrayList<>();
    	
    	if(status.equals("past")){
    		events = eventRepository.findPastEvents(memberId);
        } else if(status.equals("future")){
        	events = eventRepository.findFutureEvents(memberId);
        	
        } else if(status.equals("current")){
        	events = eventRepository.findCurrentEvents(memberId);
        }
    	
        for(Event event: events) {
			EventDTO eventDTO =  new EventDTO(event);
			returnValue.add(eventDTO);
		}
        return returnValue;
    }


    @Override
    public EventDTO updateEvent(Long eventId, String eventName, String eventDescription, String location, String fee,
            String slotsAvailable, String eventBannerUrl) {
        
            Event event  = eventRepository.findOne(eventId);

            if(event!= null){
                event.setEventName(eventName);
                event.setEventDescription(eventDescription);
                event.setLocation(location);
                event.setFee(fee);
                event.setSlotsAvailable(slotsAvailable);
                event.setEventBannerUrl(eventBannerUrl);
    
                eventRepository.save(event);
                EventDTO eventDTO=new EventDTO(event);
    
                return eventDTO;
            }else{
                throw new EventNotFoundException("No Event found with eventId "+eventId);
            }

           
    }

    @Override
    public Long remainingEventSlots(Long eventId) {

            Event event  = eventRepository.findOne(eventId);

            if(event!= null){
                Long slotsUsed = eventEnrollmentRepository.findEventEnrollmentUsed(eventId);
                Long slotsAvailable = Long.parseLong(event.getSlotsAvailable()); //eventRepository.findSlotsAvailable(eventId);
                Long slotsRemain = (slotsAvailable - slotsUsed);
                
                //eventRepository.save(event);
                //EventDTO eventDTO=new EventDTO(event);
    
                return slotsRemain;
            }else{
                throw new EventNotFoundException("No Event found with eventId "+eventId);
            }

           
    }

    @Override
    public EventDTO deleteEvent(Long eventId) {

        Event event = eventRepository.findOne(eventId);

        if (event!=null){
            event.setEventStatus(EventStatusEnum.INACTIVE.getStatus());
            eventRepository.save(event);
    
            EventDTO eventDTO=new EventDTO(event);
            return eventDTO;
    
            }else {
                throw new EventNotFoundException("No Event found with eventId "+eventId);
            }
        }

	@Override
	public EventDTO updateEvent(Long eventId, EventUpdateDTO eventUpdateDTO) {
		Event event  = eventRepository.findOne(eventId);

        if(event!= null){
            event.setEventName(eventUpdateDTO.getEventName());
            event.setEventDescription(eventUpdateDTO.getEventDescription());
            event.setLocation(eventUpdateDTO.getLocation());
            event.setFee(eventUpdateDTO.getFee());
            event.setStartDate(eventUpdateDTO.getStartDate());
            event.setEndDate(eventUpdateDTO.getEndDate());
            event.setEnrollmentEndDate(eventUpdateDTO.getEnrollmentEndDate());
            event.setSlotsAvailable(eventUpdateDTO.getSlotsAvailable());
            event.setEventBannerUrl(eventUpdateDTO.getEventBannerUrl());
            event.setCreatedDate(new Date());
            event.setEventStatus("Active");

            eventRepository.save(event);
            EventDTO eventDTO=new EventDTO(event);

            return eventDTO;
        } else{
            throw new EventNotFoundException("No Event found with eventId "+eventId);
        }
	}

	@Override
	public EventEnrollment payAndEnrollMember(Long eventId, PaymentDTO paymentDTO) {
		try {
            Long memberId = paymentDTO.getMemberId();
            String description = paymentDTO.getDescription();
            String token = paymentDTO.getToken();
            Long amount = paymentDTO.getAmount();
            Member member = memberRepository.findOne(memberId); 
            String emailId = member.getEmailId();
            String memberName = member.getFirstName() +" "+member.getLastName();
			List<EventEnrollment> existingEnrollments = eventEnrollmentRepository.findByMemberAndEvent(memberId, eventId);
			if(existingEnrollments != null && existingEnrollments.size() > 0) {
				throw new RuntimeException("Event already enrolled for eventId: "+eventId+", memberId:"+memberId);
			}
			
			//TODO: validate null for event and member
			Event event = eventRepository.findOne(eventId);
			String eventName = event.getEventName();
			//Member member = memberRepository.findOne(memberId);
			
			
			
            Charge charge  = externalPaymentService.charge(paymentDTO);
            
            String receipt = charge.getReceiptUrl();
			// save the payment details
			MemberPayment memberPayment = memberPaymentFactory.create(paymentDTO, charge, PaymentReason.EVENT);
			MemberPayment updated = memberPaymentRepository.save(memberPayment);
			
			
			EventEnrollment eventEnrollment = eventEnrollmentFactory.create(event, member, updated.getPaymentStatus(), updated.getMemberPaymentId());
			EventEnrollment retval = eventEnrollmentRepository.save(eventEnrollment);
            
            // Mail to the user after successful event enrollment.
			String pricePaid = Long.toString(amount/100);
            Mail mail=new Mail();
            mail.setMailFrom(fromEmail);
    
            mail.setMailTo(emailId);
			mail.setMailSubject("You have a new message from Cricket Minds regarding payment for event.");
			mail.setMailContent("The payment has been successful for "+eventName+" by the user: "+"\n"+"\n"+"Name : "+memberName+"\n"+"Email Id : "+emailId+"\n"+"Amount : "+pricePaid+"\n"+"Payment Receipt : "+receipt+"\n"+"\n");

			emailService.sendEmail(mail);

            // Mail to the admin or headcoach after successful event enrollment.
            Mail adminMail = new Mail();
            adminMail.setMailFrom(fromEmail);
            
            adminMail.setMailTo(adminEmail);
			adminMail.setMailSubject("Message from Cricket Minds regarding payment for an event.");
			adminMail.setMailContent(" An Event enrollment has been done for the event : "+eventName+" by the user : "+"\n"+"\n"+"Name : "+memberName+"\n"+"Email Id : "+emailId+"\n"+"Amount : "+pricePaid+"\n"+"Payment Receipt : "+receipt+"\n"+"\n");
			
			emailService.sendEmail(adminMail);

            // save the enrollment
			return retval;
			
		} catch (StripeException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public EventEnrollment getEnrollment(Long eventId, Long memberId) {
		
		List<EventEnrollment> enrollments = eventEnrollmentRepository.findByMemberAndEvent(memberId, eventId);
		if(enrollments.size() > 0) {
			return enrollments.get(0);
		}
		return null;
	}
	
	@Override
	public List<UserDTO> enrolledMembers(Long eventId) {
		List<EventEnrollment> enrollments = eventEnrollmentRepository.findByEvent(eventId);
		
		List<UserDTO> retval = new ArrayList<>();
		for(EventEnrollment enrollment: enrollments) {
            //Member member = memberRepository.findOne(enrollment.getMember());
            Member member = enrollment.getMember();
			UserDTO userDro = new UserDTO(member);
			retval.add(userDro);
		}
		
		return retval;
	}
	

        /*@Override
        public List<EventDTO> getEvents(String eventStatus) {
            
            List<EventDTO> returnValue = new ArrayList<EventDTO>();
		
		List<Event> events = eventRepository.findEventByStatus(eventStatus);
		
		for(Event event: events) {
			EventDTO eventDTO =  new EventDTO(event);
			returnValue.add(eventDTO);
		}
        return returnValue;
    
     return returnValue;
}*/
}