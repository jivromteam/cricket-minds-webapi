package com.jivrom.cricketminds.domain.model;

public enum MemberStatusEnum {

	NULL("NULL"),
	
	PENDING("Pending"),
	ACTIVE("Active"),
	INACTIVE("Inactive");
	
	private String status;
	
	MemberStatusEnum(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return this.status;
	}
	
	public static MemberStatusEnum fromValue(String status) {
		for(MemberStatusEnum thisStatus: values()) {
			if(thisStatus.getStatus().equals(status)) {
				return thisStatus;
			}
		}
		
		return NULL;
	}
	
	public boolean isNull() {
		return this == NULL;
	}
}

