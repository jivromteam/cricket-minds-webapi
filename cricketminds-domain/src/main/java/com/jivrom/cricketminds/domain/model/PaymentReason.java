package com.jivrom.cricketminds.domain.model;

public enum PaymentReason {

	NULL("NULL"),
	
	EVENT("Event"),
	
	COACHINNG_SESSION("CoachingSession")
	
	;
	
	private String reason;
	
	PaymentReason(String reason) {
		this.reason = reason;
	}
	
	public String getReason() {
		return this.reason;
	}
}
