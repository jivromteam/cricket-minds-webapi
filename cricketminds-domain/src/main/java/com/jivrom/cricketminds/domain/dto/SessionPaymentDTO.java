package com.jivrom.cricketminds.domain.dto;

import com.jivrom.cricketminds.domain.model.SessionPayment;

public class SessionPaymentDTO {

	private Long sessionPaymentId;
	private Long sessionId;
	private String sessionName;
	
	private String paymentMonth;
	private String amount;
	
	private Long memberId;
	private String firstName;
	private String lastName;
	
	public SessionPaymentDTO() {}
	
	public SessionPaymentDTO(SessionPayment sessionPayment) {
		this.sessionPaymentId = sessionPayment.getSessionPaymentId();
		this.sessionId = sessionPayment.getSessionId();
		this.sessionName = sessionPayment.getSession().getSessionName();
				
		this.paymentMonth = sessionPayment.getPaymentMonth();
		this.amount  = sessionPayment.getAmountDue();
		this.memberId = sessionPayment.getMemberId();
		
		this.firstName = sessionPayment.getMember().getFirstName();
		this.lastName = sessionPayment.getMember().getLastName();
		
		
	}
	
	public Long getSessionPaymentId() {
		return sessionPaymentId;
	}
	public void setSessionPaymentId(Long sessionPaymentId) {
		this.sessionPaymentId = sessionPaymentId;
	}
	public Long getSessionId() {
		return sessionId;
	}
	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}
	public String getSessionName() {
		return sessionName;
	}
	public void setSessionName(String sessionName) {
		this.sessionName = sessionName;
	}
	public String getPaymentMonth() {
		return paymentMonth;
	}
	public void setPaymentMonth(String paymentMonth) {
		this.paymentMonth = paymentMonth;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public Long getMemberId() {
		return memberId;
	}
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
}
