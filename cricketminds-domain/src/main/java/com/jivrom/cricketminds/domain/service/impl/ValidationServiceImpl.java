package com.jivrom.cricketminds.domain.service.impl;

import org.springframework.stereotype.Service;

import com.jivrom.cricketminds.common.util.CricketMindsCommonUtil;
import com.jivrom.cricketminds.domain.dto.ErrorDTO;
import com.jivrom.cricketminds.domain.dto.EventRegisterDTO;
import com.jivrom.cricketminds.domain.dto.LoginDTO;
import com.jivrom.cricketminds.domain.dto.RegistrationDTO;
import com.jivrom.cricketminds.domain.dto.SessionDTO;
import com.jivrom.cricketminds.domain.dto.SessionRegisterDTO;
import com.jivrom.cricketminds.domain.service.ValidationService;

@Service
public class ValidationServiceImpl implements ValidationService {

	@Override
	public ErrorDTO login(LoginDTO loginDTO) {
		if (loginDTO != null) {

		} else {
			return new ErrorDTO("Login details are null", "TBD");
		}
		return null;
	}

	@Override
	public ErrorDTO register(RegistrationDTO registrationDTO) {
		if (registrationDTO != null) {
			// Validate FirstName
			if (!CricketMindsCommonUtil.validateString(registrationDTO.getFirstName())
					|| !CricketMindsCommonUtil.validateFirstName(registrationDTO.getFirstName())) {
				return new ErrorDTO("Registration detail - Invalid FirstName", "CRIC002");
			}
			if (!CricketMindsCommonUtil.validateString(registrationDTO.getLastName())
					|| !CricketMindsCommonUtil.validateLastName(registrationDTO.getLastName())) {
				return new ErrorDTO("Registration detail - Invalid LastName", "CRIC003");
			}
			if (!CricketMindsCommonUtil.validateString(registrationDTO.getEmailId())
					|| !CricketMindsCommonUtil.isEmailValid(registrationDTO.getEmailId())) {
				return new ErrorDTO("Registration detail - Email validation fails", "CRIC004");
			}
//			if (!CricketMindsCommonUtil.validateString(registrationDTO.getPhone())
//					|| !CricketMindsCommonUtil.isPhoneNumberValid(registrationDTO.getPhone())) {
//				return new ErrorDTO("Registration detail - Phone validation fails", "CRIC005");
//			}
			// if (!CricketMindsCommonUtil.validateString(registrationDTO.getPassword())
			// || !CricketMindsCommonUtil.isPasswordValid(registrationDTO.getPassword())) {
			// return new ErrorDTO("Registration detail - Password validation fails",
			// "CRIC005");
			// }
			// if
			// (!CricketMindsCommonUtil.validateString(registrationDTO.getConfirmPassword())
			// ||
			// !registrationDTO.getConfirmPassword().equals(registrationDTO.getPassword()))
			// {
			// return new ErrorDTO("Registration detail - Password validation fails",
			// "CRIC005");
			// }

			if (!registrationDTO.getConfirmPassword().equals(registrationDTO.getPassword())) {
				return new ErrorDTO("Registration detail - Password and Confirm Password not matched", "CRIC006");
			}

		} else {
			return new ErrorDTO("Registration details are null", "CRIC001");
		}
		return null;
	}

	@Override
	public ErrorDTO eventRegister(EventRegisterDTO eventRegisterDTO) {

		if(eventRegisterDTO.getEventName().equals("")){
			return new ErrorDTO("Eventname cannot be empty","ERROR01");
		}else if(eventRegisterDTO.getEventDescription().equals("")){
            return new ErrorDTO("Eventdescription cannot be empty","ERROR02");
		}else if(eventRegisterDTO.getLocation().equals("")){
			return new ErrorDTO("Location cannot be empty","ERROR03");
		}else if(eventRegisterDTO.getFee().equals("")){
			return new ErrorDTO("Fee cannot be empty","ERROR04");
		}else if(eventRegisterDTO.getSlotsAvailable().equals("")){
			return new ErrorDTO("Slotsavailable cannot be empty","ERROR05");
		} 
//		else if(eventRegisterDTO.getEventBannerUrl().equals("")){
//			return new ErrorDTO("EventBannerUrl cannot be empty","ERROR06");
//		}
		
		return null;
	}

	@Override
	public ErrorDTO sessionRegister(SessionRegisterDTO sessionRegisterDTO) {

		if(sessionRegisterDTO.getSessionName().equals("")){
			return new ErrorDTO("Session Name cannot be empty","ERROR01");
		} else if(sessionRegisterDTO.getSessionPrice().equals("")){
			return new ErrorDTO("Session Fee cannot be empty","ERROR02");
		} //else if(sessionRegisterDTO.getSlotsAvailable().equals("")){
		//	return new ErrorDTO("Session slots cannot be empty","ERROR03");
		//}
		
		return null;
	}
}
