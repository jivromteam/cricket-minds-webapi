package com.jivrom.cricketminds.domain.dto;

public class RegistrationDTO {
	
	public RegistrationDTO() {}

	private String firstName;
	private String lastName;
	private String emailId;
	private String password;
	private String confirmPassword;
	private Boolean forceResetPassword;
	private String phone;
	// private String alternatePhone;
	
//	private List<String> types;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public boolean hasFirstName() {
		return firstName != null && !firstName.isEmpty();
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public boolean hasLastName() {
		return lastName != null && !lastName.isEmpty();
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public boolean hasEmailId() {
		return emailId != null && !emailId.isEmpty();
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean hasPassword() {
		return password != null && !password.isEmpty();
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	

	public Boolean getForceResetPassword() {
		return forceResetPassword;
	}

	public void setForceResetPassword(Boolean forceResetPassword) {
		this.forceResetPassword = forceResetPassword;
	}

	public boolean hasForceRestPassword() {
		return forceResetPassword != null;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public boolean hasPhone() {
		return phone != null && !phone.isEmpty();
	}

	// public String getAlternatePhone() {
	// 	return alternatePhone;
	// }

	// public void setAlternatePhone(String alternatePhone) {
	// 	this.alternatePhone = alternatePhone;
	// }
	
	// public boolean hasAlternatePhone() {
	// 	return alternatePhone != null && !alternatePhone.isEmpty();
	// }
	
	/*
	 * public List<String> getTypes() { return types; } public void
	 * setTypes(List<String> types) { this.types = types; }
	 * 
	 * public boolean hasType() { return !this.types.isEmpty(); }
	 */	

}
