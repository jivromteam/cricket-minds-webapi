package com.jivrom.cricketminds.domain.dto;

public class CronDTO {
    
    private String seconds;
    private String minutes;
    private String hours;
    private String day_of_month;
    private String month;
    private String day_of_week;
    private String year;

    public CronDTO() {}

    public String getSeconds() {
        return seconds;
    }

    public void setSeconds(String seconds) {
        this.seconds = seconds;
    }

    public String getMinutes(){
        return minutes;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }

    public String getHours(){
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getDayOfMonth(){
        return day_of_month;
    }

    public void setDayOfMonth(String day_of_month) {
        this.day_of_month = day_of_month;
    }

    public String getMonths() {
        return month;
    }

    public void setMonths(String month) {
        this.month = month;
    }

    public String getDayOfWeek() {
        return day_of_week;
    }

    public void setDayOfWeek(String day_of_week) {
        this.day_of_week = day_of_week;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
    
}