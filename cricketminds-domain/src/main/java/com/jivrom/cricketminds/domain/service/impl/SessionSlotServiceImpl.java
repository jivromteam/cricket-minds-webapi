package com.jivrom.cricketminds.domain.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.jivrom.cricketminds.common.util.DateUtil;
import com.jivrom.cricketminds.domain.dto.AssignCoachDTO;
import com.jivrom.cricketminds.domain.dto.CoachDTO;
import com.jivrom.cricketminds.domain.dto.CreateSlotDTO;
import com.jivrom.cricketminds.domain.dto.SlotDTO;
import com.jivrom.cricketminds.domain.dto.SlotDetailDTO;
import com.jivrom.cricketminds.domain.dto.UserDTO;
import com.jivrom.cricketminds.domain.exception.EventNotFoundException;
import com.jivrom.cricketminds.domain.model.DaysOfWeek;
import com.jivrom.cricketminds.domain.model.Member;
import com.jivrom.cricketminds.domain.model.MemberSlot;
import com.jivrom.cricketminds.domain.model.MemberTypeEnum;
import com.jivrom.cricketminds.domain.model.Session;
import com.jivrom.cricketminds.domain.model.SessionEnrollment;
import com.jivrom.cricketminds.domain.model.SessionEnrollmentFactory;
import com.jivrom.cricketminds.domain.model.Slot;
import com.jivrom.cricketminds.domain.repository.MemberRepository;
import com.jivrom.cricketminds.domain.repository.MemberSlotRepository;
import com.jivrom.cricketminds.domain.repository.MemberTypeRepository;
import com.jivrom.cricketminds.domain.repository.SessionEnrollmentRepository;
import com.jivrom.cricketminds.domain.repository.SessionRepository;
import com.jivrom.cricketminds.domain.repository.SessionTypeRepository;
import com.jivrom.cricketminds.domain.repository.SlotRepository;
import com.jivrom.cricketminds.domain.service.PaymentService;
import com.jivrom.cricketminds.domain.service.SessionSlotService;

@Service
public class SessionSlotServiceImpl implements SessionSlotService {

	public static final Logger logger = LoggerFactory.getLogger(SessionSlotServiceImpl.class);
	
	
	@Resource
	private SlotRepository slotRepository;

	@Resource
	private SessionRepository sessionRepository;

	@Resource
	private SessionTypeRepository sessionTypeRepository;

	@Resource
	private SessionEnrollmentRepository sessionEnrollmentRepository;

	@Resource
	private MemberRepository memberRepository;

	@Resource
	private MemberTypeRepository memberTypeRepository;

	@Resource
	private MemberSlotRepository memberSlotRepository;

	@Resource
	private SessionEnrollmentFactory sessionEnrollmentFactory;
	
	@Resource
	private PaymentService paymentService;
	
	
	// create a slot

	@Deprecated
	@Override
	public SlotDTO createSlot(CreateSlotDTO createSlotDTO) {

		Slot slot = new Slot();
		slot.setSessionId(slotRepository.findSessionId(createSlotDTO.getSessionId()));

		slot.setSessionDate(createSlotDTO.getSessionDate());
		slot.setStartTime(createSlotDTO.getStartTime());
		slot.setEndTime(createSlotDTO.getEndTime());
		slot.setMaxEnrollment(createSlotDTO.getMaxEnrollment());

		slot.setCreatedDate(new Date());
		slot.setUpdatedDate(new Date());

		Slot retVal = slotRepository.save(slot);
		SlotDTO retValue = new SlotDTO(retVal);

		List<SessionEnrollment> Members = sessionEnrollmentRepository.findBySessionId(slot.getSessionId());
		for (SessionEnrollment member : Members) {
			MemberSlot userAdd = new MemberSlot();
			userAdd.setSlotId(retValue.getSlotId());
			Member memb = member.getMember();
			userAdd.setMemberId(memb.getMemberId());
			userAdd.setMemberType(member.getRole());
			userAdd.setCreatedDate(slot.getCreatedDate());
			userAdd.setUpdatedDate(slot.getUpdatedDate());

			MemberSlot retval = memberSlotRepository.save(userAdd);
		}
		return retValue;
		// return null;
	}

	// get all sessionslots

	@Override
	public List<SlotDTO> getAllSessionSlots() {

		List<SlotDTO> returnValue = new ArrayList<SlotDTO>();
		List<Slot> slots = slotRepository.findAll();

		for (Slot slot : slots) {
			SlotDTO slotDTO = new SlotDTO(slot);
			returnValue.add(slotDTO);
		}
		return returnValue;
	}

	// getslot by sessionId

	@Override
	public List<SlotDTO> getSlots(Long sessionId) {

		List<SlotDTO> retval = new ArrayList<>();

		Session session = sessionRepository.findOne(sessionId);

		List<Slot> slots = slotRepository.findBySessionId(sessionId);

		if (slots != null) {
			for (Slot slot : slots) {
				SlotDTO slotDTO = new SlotDTO(slot, session);
				retval.add(slotDTO);

			}
		}

		return retval;
	}

	// get slot by slotId

	@Override
	public SlotDTO getSlotById(Long slotId) {

		Slot slot = slotRepository.findOne(slotId);

		if (slot != null) {

			SlotDTO slotDTO = new SlotDTO(slot);
			slotDTO.setSessionName(sessionRepository.findSessionName(slot.getSessionId()));
			slotDTO.setSessionType(
					sessionTypeRepository.findSessionType(sessionRepository.findSessionTypeId(slot.getSessionId())));
			return slotDTO;

		} else {
			throw new EventNotFoundException("No slot found with slotId " + slotId);
		}
	}

	// get slots by month
	public List<SlotDTO> getSlotsByMonth(String monthOfYear) {
		List<SlotDTO> retval = new ArrayList<>();

		Integer month = Integer.parseInt(monthOfYear.substring(0, 2));
		Integer year = Integer.parseInt(monthOfYear.substring(3, 7));
		System.out.println("The month is: " + month + " and the year is: " + year);
		List<Slot> slots = slotRepository.findSlotsByMonth(month, year);
		if (slots != null) {
			for (Slot slot : slots) {
				SlotDTO slotDTO = new SlotDTO(slot);
				slotDTO.setSessionName(sessionRepository.findSessionName(slot.getSessionId()));
				slotDTO.setSessionType(sessionTypeRepository
						.findSessionType(sessionRepository.findSessionTypeId(slot.getSessionId())));
				System.out.println(
						"Session Name: " + slotDTO.getSessionName() + " and Session type: " + slotDTO.getSessionType());

				retval.add(slotDTO);

			}
		}
		return retval;
	}

	// update a slot

	@Override
	public SlotDTO updateSlots(SlotDTO SlotDTO) {
		System.out.println("!!!!!!!!!!!!!!!!!Testing for SLot update!!!!!!!!!!!!!!!!");
		Slot slot = slotRepository.findOne(SlotDTO.getSlotId());
		if (slot != null) {
			// slot.setSessionId(createSlotDTO.getSessionId());
			// slot.setSessionId(sessionRepository.findOne(createSlotDTO.getSessionId()));
			// slot.setSessionId(slotRepository.findSessionId(SlotDTO.getSessionId()));

			// slot.setSessionDate(createSlotDTO.getSessionDate());
			slot.setStartTime(SlotDTO.getStartTime());
			slot.setEndTime(SlotDTO.getEndTime());
			// slot.setStatus(sessionSlotDTO.getStatus());
			slot.setMaxEnrollment(SlotDTO.getMaxEnrollment());

			slot.setCreatedDate(new Date());
			slot.setUpdatedDate(new Date());
			System.out.println("!!!!!!!!!!!!!!!!!Testing for Slot save @ Repository!!!!!!!!!!!!!!!!");
			Slot retVal = slotRepository.save(slot);
			System.out.println("!!!!!!!!!!!!!!!!!Slot Updated!!!!!!!!!!!!!!!!");
			SlotDTO retValue = new SlotDTO(retVal);
			return retValue;
		} else {
			throw new EventNotFoundException("No slot found with slotId " + SlotDTO.getSlotId());
		}
	}

	// delete a slot

	@Override
	public SlotDTO deleteSlot(Long slotId) {

		Slot slot = slotRepository.findOne(slotId);

		if (slot != null) {

			List<MemberSlot> Member = memberSlotRepository.findBySlotId(slotId);
			for (MemberSlot member : Member) {
				memberSlotRepository.delete(member);
			}

			slotRepository.delete(slot);

			SlotDTO slotDTO = new SlotDTO(slot);
			return slotDTO;

		} else {
			throw new EventNotFoundException("No slot found with slotId " + slotId);
		}
	}

	// assign coach for slots

	@Override
	public CoachDTO assignCoach(AssignCoachDTO assignCoachDTO) {

		// List<MemberType> coaches =
		// memberTypeRepository.findByMemberId(assignCoachDTO.getMemberId());
		try {
			System.out.println(" Slot Id : " + assignCoachDTO.getSlotId());
			Slot slot = slotRepository.findOne(assignCoachDTO.getSlotId());
			Session session = sessionRepository.findOne(slot.getSessionId());
			Long sessionId = session.getSessionId();

			// MemberSlot coachExist = new MemberSlot();
			// Member Member = memberRepository.findOne(assignCoachDTO.getMemberId());
			// String role =
			// memberTypeRepository.getMemberType(assignCoachDTO.getMemberId());
			// String paymentStatus = "NULL";
			// Long memberPaymentId = null;
			MemberSlot coachExist = new MemberSlot();
			coachExist = memberSlotRepository.findByMemberId(assignCoachDTO.getMemberId(), assignCoachDTO.getSlotId());

			// List<SlotDTO> slots = this.getSlots(assignCoachDTO.getSessionId());
			// for( Slot slot: slots) {
			// SlotDTO slotDTO = new SlotDTO(slot);
			// Long slotId = slotDTO.getSlotId();
			// MemberSlot memSlot = new MemberSlot();
			// memSlot = memberSlotRepository.findByMemberId(assignCoachDTO.getMemberId(),
			// slotId);
			// coachExist.add(memSlot);
			// }

			// if(coachExist == null && coachExist.size() == 0){
			if (coachExist == null) {
				System.out.println("entering slotid execution...........");
				// List<CoachDTO> returnValue = new ArrayList<>();

				// for( Slot slot: slots) {
				// SlotDTO slotDTO = new SlotDTO(slot);
				// Long slotId = slotDTO.getSlotId();
				// MemberSlot memberslot = new MemberSlot();
				// memberslot.setSlotId(slotRepository.findBySlotId(slotId));
				// memberslot.setMemberId(memberTypeRepository.findMemberId(assignCoachDTO.getMemberId()));

				// memberslot.setMemberType(assignCoachDTO.getMemberType());

				// memberslot.setCreatedDate(new Date());
				// memberslot.setUpdatedDate(new Date());

				// MemberSlot retVal = memberSlotRepository.save(memberslot);
				// Member member = memberRepository.findOne(retVal.getMemberId());
				// CoachDTO retValue = new CoachDTO(member);
				// returnValue.add(retValue);
				// }

				// List<SessionEnrollment> existingEnrollments =
				// sessionEnrollmentRepository.findByMemberAndSession(assignCoachDTO.getMemberId(),
				// sessionId);
				// if (existingEnrollments != null && existingEnrollments.size() > 0) {
				// throw new RuntimeException(
				// "Session already enrolled for sessionId: " + sessionId + ", memberId:" +
				// assignCoachDTO.getMemberId());
				// } else {
				// SessionEnrollment sessionEnrollment =
				// sessionEnrollmentFactory.create(session, Member, role, paymentStatus,
				// memberPaymentId);
				// SessionEnrollment RetVal =
				// sessionEnrollmentRepository.save(sessionEnrollment);
				// }
				MemberSlot memberslot = new MemberSlot();
				memberslot.setSlotId(slotRepository.findBySlotId(assignCoachDTO.getSlotId()));
				memberslot.setMemberId(memberTypeRepository.findMemberId(assignCoachDTO.getMemberId()));
				// memberslot.setMemberId(memberRepository.findByMemberId(assignCoachDTO.getMemberId()));
				memberslot.setMemberType(assignCoachDTO.getMemberType());

				memberslot.setCreatedDate(new Date());
				memberslot.setUpdatedDate(new Date());

				MemberSlot retVal = memberSlotRepository.save(memberslot);
				Member member = memberRepository.findOne(retVal.getMemberId());
				CoachDTO retValue = new CoachDTO(member);

				return retValue;

			} else {
				return null;
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	// delete coach for slots

	@Override
	public CoachDTO deleteCoach(AssignCoachDTO assignCoachDTO) {

		try {
			System.out.println(" Slot Id : " + assignCoachDTO.getSlotId());
			Slot slot = slotRepository.findOne(assignCoachDTO.getSlotId());

			// if(slot==null) {
			// throw new RuntimeException(
			// "Slot does not exist.");
			// }

			MemberSlot coachExist = new MemberSlot();
			coachExist = memberSlotRepository.findByMemberId(assignCoachDTO.getMemberId(), assignCoachDTO.getSlotId());

			// if(coachExist == null) {
			// throw new RuntimeException(
			// "Slot does not exist.");
			// }
			System.out.println("entering slotid deletion...........");

			Member member = coachExist.getMember();
			memberSlotRepository.delete(coachExist);

			CoachDTO retValue = new CoachDTO(member);

			return retValue;

		} catch (RuntimeException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	// get all coaches

	@Override
	public List<CoachDTO> getAllCoaches() {

		List<Member> members = memberRepository.findAllCoaches();

		List<CoachDTO> coachDTOs = new ArrayList<CoachDTO>();

		for (Member member : members) {
			CoachDTO coachDTO = new CoachDTO(member);
			coachDTOs.add(coachDTO);
		}

		return coachDTOs;
	}

	@Override
	public SlotDetailDTO getSlotDetail(Long slotId) {
		SlotDTO slotDTO = this.getSlotById(slotId);

		List<MemberSlot> memberSlots = memberSlotRepository.findBySlotId(slotId);
		List<UserDTO> users = new ArrayList<>();
		List<UserDTO> coaches = new ArrayList<>();
		for (MemberSlot memberSlot : memberSlots) {
			String memberType = memberSlot.getMemberType();
			UserDTO user = new UserDTO(memberSlot.getMember());

			switch (MemberTypeEnum.fromType(memberType)) {
			case HEAD_COACH:
			case COACH:
				coaches.add(user);
				break;
			case MEMBER:
				users.add(user);
				break;
			default:
				// do nothing
			}

		}

		SlotDetailDTO slotDetailDTO = new SlotDetailDTO(slotDTO, users, coaches);
		return slotDetailDTO;
	}

	// get remaining bookings for slots

	@Override
	public Long remainingSlotBookings(Long slotId) {
		Slot slot = slotRepository.findOne(slotId);
		String memberType = "MEMBER";
		if (slot != null) {
			Long bookingsUsed = memberSlotRepository.findSlotBookingsUsed(slotId, memberType);
			Long bookingsAvailable = slot.getMaxEnrollment(); // eventRepository.findSlotsAvailable(eventId);
			Long bookingsRemain = (bookingsAvailable - bookingsUsed);

			// eventRepository.save(event);
			// EventDTO eventDTO=new EventDTO(event);

			return bookingsRemain;
		} else {
			throw new EventNotFoundException("No Slot found with slotId " + slotId);
		}
	}

	@Override
	public void generateSlots(Long sessionId) {

		Session session = sessionRepository.findOne(sessionId);

		Date slotStartDate;
		if (session.getLastSlotGenerationDate() == null) {
			slotStartDate = session.getStartDate();
		} else {
			slotStartDate = DateUtil.nextDay(session.getLastSlotGenerationDate());
		}
        
		Date monthEnd = DateUtil.monthEnd(slotStartDate);

		Date slotEndDate;
		if (session.getEndDate() != null) {
			slotEndDate = DateUtil.earliest(monthEnd, session.getEndDate());
		} else {
			slotEndDate = monthEnd;
        }
        
		if (slotEndDate.after(slotStartDate)) {
            this.generateSlots(session, slotStartDate, slotEndDate);
            
		} else {
			logger.info("No slots need to be generated for sessionid:" +sessionId);
		}
        
	}

	private void generateSlots(Session session, Date slotStartDate, Date slotEndDate) {
        
        int startHour = DateUtil.getHour(session.getStartTime());
        
		int startMin = DateUtil.getMin(session.getStartTime());
        
        int endHour = DateUtil.getHour(session.getEndTime());
        
		int endMin = DateUtil.getMin(session.getEndTime());
        
		List<Long> members = new ArrayList<>();
		List<Long> coaches = new ArrayList<>();
        
        String cron = session.getCronTab();
        
		// filter only the day_of_week
		String dayOfWeek = cron.split(" ")[5];
        
		List<Date> slotDates = this.getQualifiedDates(dayOfWeek, slotStartDate, slotEndDate);
        
        List<SessionEnrollment> enrollments = sessionEnrollmentRepository.findBySessionId(session.getSessionId());
        
		for (SessionEnrollment enrollment : enrollments) {
			MemberTypeEnum role = MemberTypeEnum.fromType(enrollment.getRole());
			if (role == MemberTypeEnum.COACH) {
				coaches.add(enrollment.getMember().getMemberId());
			} else if (role == MemberTypeEnum.MEMBER) {
				members.add(enrollment.getMember().getMemberId());
			}
		}
		
		if(!slotDates.isEmpty()) {
			for (Date slotDate : slotDates) {
				// create slot
				Calendar startTime = Calendar.getInstance();
				startTime.setTime(slotDate);
				startTime.set(Calendar.HOUR_OF_DAY, startHour);
				startTime.set(Calendar.MINUTE, startMin);

				Calendar endTime = Calendar.getInstance();
				endTime.setTime(slotDate);
				endTime.set(Calendar.HOUR_OF_DAY, endHour);
				endTime.set(Calendar.MINUTE, endMin);

				this.createSlot(session.getSessionId(), slotDate, startTime.getTime(), endTime.getTime(), members, coaches);

			}
			
			// generate payments
			String paymentMonth = DateUtil.formatDate(slotStartDate, DateUtil.MONTH_FORMAT);
			paymentService.generatePaymentDue(session.getSessionId(), paymentMonth, members, session.getSessionPrice());
			
		} else {
            
			//no slots generated
		}
		
		// on successfull completion, update generate slots
		session.setLastSlotGenerationDate(slotEndDate);
		sessionRepository.save(session);
        
	}

	private void createSlot(Long sessionId, Date slotDate, Date startTime, Date endTime, List<Long> members,
			List<Long> coaches) {
		
		Slot slot = new Slot();
		
		slot.setSessionId(sessionId);

		slot.setSessionDate(slotDate);
		slot.setStartTime(startTime);
		slot.setEndTime(endTime);
		slot.setMaxEnrollment(null);

		slot.setCreatedDate(new Date());
		slot.setUpdatedDate(new Date());

		Slot retVal = slotRepository.save(slot);
		
		for(Long member: members) {
			MemberSlot memberSlot = new MemberSlot();
			memberSlot.setSlotId(retVal.getSlotId());
			memberSlot.setMemberId(member);
			memberSlot.setMemberType(MemberTypeEnum.MEMBER.getType());
			memberSlot.setCreatedDate(new Date());
			memberSlot.setUpdatedDate(new Date());
			memberSlotRepository.save(memberSlot);
		}
		for(Long coach: coaches) {
			MemberSlot memberSlot = new MemberSlot();
			memberSlot.setSlotId(retVal.getSlotId());
			memberSlot.setMemberId(coach);
			memberSlot.setMemberType(MemberTypeEnum.COACH.getType());
			memberSlot.setCreatedDate(new Date());
			memberSlot.setUpdatedDate(new Date());
			memberSlotRepository.save(memberSlot);
		}
		
	}

	private List<Date> getQualifiedDates(String dayOfWeek, Date slotStartDate, Date slotEndDate) {
		List<Date> retval = new ArrayList<>();
		String[] daysOfWeek = dayOfWeek.split(",");
		List<Integer> days = new ArrayList<>();

		for (String day : daysOfWeek) {
			days.add(DaysOfWeek.fromDayString(day).getCalendarDay());
		}

		List<Calendar> datesInRange = DateUtil.datesBetween(slotStartDate, slotEndDate);

		for (Calendar slotDate : datesInRange) {
			if (days.contains(slotDate.get(Calendar.DAY_OF_WEEK))) {
//				createslot
				retval.add(DateUtils.truncate(slotDate, java.util.Calendar.DAY_OF_MONTH).getTime());
			}
		}
		return retval;
	}

}