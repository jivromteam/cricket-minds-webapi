package com.jivrom.cricketminds.domain.service;

import java.util.List;

import com.jivrom.cricketminds.domain.dto.EventDTO;
import com.jivrom.cricketminds.domain.dto.EventRegisterDTO;
import com.jivrom.cricketminds.domain.dto.EventUpdateDTO;
import com.jivrom.cricketminds.domain.dto.UserDTO;
import com.jivrom.cricketminds.domain.model.EventEnrollment;
import com.jivrom.cricketminds.payment.dto.PaymentDTO;

public interface EventService {

	public List<EventDTO> getAllEvents();

	public List<EventDTO> findPastEvents();

	public List<EventDTO> findFutureEvents();

	public List<EventDTO> findCurrentEvents();
	
	public List<EventDTO> findEvents(Long memberId, String status);

//public List<EventDTO> getEvents(String eventStatus);

	public EventDTO createEvent(EventRegisterDTO eventRegisterDTO);

	public EventDTO getEvent(Long eventId);

	public EventDTO updateEvent(Long eventId, String eventName, String eventDescription, String location, String fee,
			String slotsAvailable, String eventBannerUrl);

	public Long remainingEventSlots(Long eventId);

	public EventDTO updateEvent(Long eventId, EventUpdateDTO eventUpdateDTO);

	public EventDTO deleteEvent(Long eventId);
	
	public EventEnrollment payAndEnrollMember(Long eventId, PaymentDTO paymentDTO);
	
	public EventEnrollment getEnrollment(Long eventId, Long memberId);
	
	public List<UserDTO> enrolledMembers(Long eventId);

}