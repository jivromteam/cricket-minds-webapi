package com.jivrom.cricketminds.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jivrom.cricketminds.domain.model.MemberType;

@Repository
public interface MemberTypeRepository extends JpaRepository<MemberType, Long> {

	public List<MemberType> findByMemberId(Long memberId);


	@Query(nativeQuery = true, value = "SELECT MEMBER_TYPE from MEMBER_TYPE  WHERE MEMBER_TYPE = :memberType") 
	public String findMemberType( @Param("memberType") String memberType);

	@Query(nativeQuery = true, value = "SELECT MEMBER_ID from MEMBER_TYPE  WHERE MEMBER_ID = :memberId") 
	public Long findMemberId( @Param("memberId") Long memberId);

	@Query(nativeQuery = true, value = "SELECT MEMBER_TYPE from MEMBER_TYPE  WHERE MEMBER_ID = :memberId") 
	public String getMemberType( @Param("memberId") Long memberId);
}