package com.jivrom.cricketminds.domain.dto;

public class AssignCoachDTO {

    private Long slotId;
    private Long memberId;
    private String memberType;
    
	
    
    public AssignCoachDTO(){}
	

    public Long getSlotId() {
        return slotId;
    }

    public void setSlotId(Long slotId) {
        this.slotId = slotId;
    }  
    
    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }  

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }  

    
    
    
}