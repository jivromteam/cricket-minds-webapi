package com.jivrom.cricketminds.domain.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PaymentDueDTO {

	private Long memberId;
	private String totalAmountDue;
	
	private List<PaymentDueHistory> dues = new ArrayList<>();
	
	
	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}


	public String getTotalAmountDue() {
		return totalAmountDue;
	}


	public void setTotalAmountDue(String totalAmountDue) {
		this.totalAmountDue = totalAmountDue;
	}


	public List<PaymentDueHistory> getDues() {
		return dues;
	}

	public void setDues(List<PaymentDueHistory> dues) {
		this.dues = dues;
	}
	
	public void addDue(PaymentDueHistory due) {
		this.dues.add(due);
	}


	public static class PaymentDueHistory {
		
		private Long sesssionPaymentId;
		private String sessionName;
		private String amountDue;
		private Date dueDate;
		private String paymentMonth;
		
		public PaymentDueHistory() {}
		
		public PaymentDueHistory(Long sesssionPaymentId, String amountDue, Date dueDate, String sessionName, String paymentMonth) {
			this.sesssionPaymentId = sesssionPaymentId;
			this.amountDue = amountDue;
			this.dueDate = dueDate;
			this.sessionName = sessionName;
			this.paymentMonth = paymentMonth;
		}
		
		public Long getSesssionPaymentId() {
			return sesssionPaymentId;
		}
		public void setSesssionPaymentId(Long sesssionPaymentHistoryId) {
			this.sesssionPaymentId = sesssionPaymentHistoryId;
		}
		public String getAmountDue() {
			return amountDue;
		}
		public void setAmountDue(String amountDue) {
			this.amountDue = amountDue;
		}
		public Date getDueDate() {
			return dueDate;
		}
		public void setDueDate(Date dueDate) {
			this.dueDate = dueDate;
		}

		public String getSessionName() {
			return sessionName;
		}

		public void setSessionName(String sessionName) {
			this.sessionName = sessionName;
		}

		public String getPaymentMonth() {
			return paymentMonth;
		}

		public void setPaymentMonth(String paymentMonth) {
			this.paymentMonth = paymentMonth;
		}
		
	}
}
