package com.jivrom.cricketminds.domain.exception;

public class SessionNotFoundException extends RuntimeException{
    
    public SessionNotFoundException(String message) {
		super(message);
	}
}