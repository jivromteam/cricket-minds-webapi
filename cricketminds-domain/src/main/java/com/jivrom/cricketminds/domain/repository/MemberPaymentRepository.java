package com.jivrom.cricketminds.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jivrom.cricketminds.domain.model.MemberPayment;

@Repository
public interface MemberPaymentRepository extends JpaRepository<MemberPayment, Long> {

	@Query(nativeQuery = true, value = "SELECT * from MEMBER_PAYMENT mp, MEMBER m WHERE m.MEMBER_ID = mp.MEMBER_ID and mp.MEMBER_ID = :memberId order by mp.CREATED_DATE DESC") 
	public List<MemberPayment> findByMemberId(@Param("memberId") Long memberId);
	
	@Query(nativeQuery = true, value = "SELECT * from MEMBER_PAYMENT mp, MEMBER m WHERE m.MEMBER_ID = mp.MEMBER_ID order by mp.CREATED_DATE DESC") 
	public List<MemberPayment> findAllMember();
}
