package com.jivrom.cricketminds.domain.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="SESSION",
uniqueConstraints = {
	}
)

public class Session {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="SESSION_ID",nullable = false)
	private Long sessionId;

	@Column(name="SESSION_TYPE_ID")
	private Long sessionTypeId;
	
	@ManyToOne
	@JoinColumn(name="SESSION_TYPE_ID", referencedColumnName = "SESSION_TYPE_ID", nullable = false, insertable = false, updatable = false)
	private SessionType sessionType;
	
	@Column(name="NAME")
	private String sessionName;

	@Column(name="SESSION_PRICE")
	private String sessionPrice;

	@Column(name="START_DATE")
	private Date startDate;
	
	@Column(name="END_DATE")
	private Date endDate;

    @Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="UPDATE_DATE")
	private Date updatedDate;

	@Column(name="CRON_TAB")
	private String cronTab;

	@Column(name="START_TIME")
	private Date startTime;

	@Column(name="END_TIME")
	private Date endTime;
	
	@Column(name="LAST_SLOT_GENERATION_DATE")
	private Date lastSlotGenerationDate;
	
	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	public Long getSessionTypeId() {
		return sessionTypeId;
	}

	public void setSessionTypeId(Long sessionTypeId) {
		this.sessionTypeId = sessionTypeId;
	}

	public String getSessionName() {
		return sessionName;
	}

	public void setSessionName(String sessionName) {
		this.sessionName = sessionName;
	}

    public String getSessionPrice() {
		return sessionPrice;
	}

	public void setSessionPrice(String sessionPrice) {
		this.sessionPrice = sessionPrice;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public SessionType getSessionType() {
		return sessionType;
	}

	public void setSessionType(SessionType sessionType) {
		this.sessionType = sessionType;
	}  

	public String getCronTab() {
        return cronTab;
    }

    public void setCronTab(String cronTab) {
        this.cronTab = cronTab;
    }

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getLastSlotGenerationDate() {
		return lastSlotGenerationDate;
	}

	public void setLastSlotGenerationDate(Date lastSlotGenerationDate) {
		this.lastSlotGenerationDate = lastSlotGenerationDate;
	}
	
}
