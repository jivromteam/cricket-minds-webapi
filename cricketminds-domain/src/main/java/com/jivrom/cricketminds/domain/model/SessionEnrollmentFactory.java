package com.jivrom.cricketminds.domain.model;

import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class SessionEnrollmentFactory {

    public SessionEnrollment create(Session session, Member member, String role, String paymentStatus, Long paymentId) {
		SessionEnrollment sessionEnrollment = new SessionEnrollment();
		
//		eventEnrollment.setEventId(eventId);
//		eventEnrollment.setMemberId(memberId);
		
		sessionEnrollment.setSession(session);
		
		sessionEnrollment.setMember(member);
		sessionEnrollment.setRole(role);
		
		sessionEnrollment.setPaymentStatus(paymentStatus);
		sessionEnrollment.setMemberPaymentId(paymentId);
		sessionEnrollment.setCreatedDate(new Date());
		if(sessionEnrollment.getPaymentStatus() != "NULL" || sessionEnrollment.getRole() != "HDCOACH" || sessionEnrollment.getRole() != "COACH") {
			sessionEnrollment.setLastPaymentDate(new Date());
		}
		
		return sessionEnrollment;
	}


    
}