package com.jivrom.cricketminds.common.util;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CricketMindsCommonUtil {
	private static Logger logger = LoggerFactory.getLogger(CricketMindsCommonUtil.class);

	public static Date parseStrToDate(String date, String format) {
		Date returnDate = null;
		if (validateString(date) && validateString(format)) {
			SimpleDateFormat formatter = new SimpleDateFormat(format);

			try {
				returnDate = formatter.parse(date);
			} catch (ParseException e) {
				return null;
			}
		}
		return returnDate;
	}

	public static String formatDateToStr(Date date, String format) {

		String returnDate = null;
		if (date != null && format != null && !format.isEmpty()) {
			SimpleDateFormat formatter = new SimpleDateFormat(format);
			returnDate = formatter.format(date);
		}

		return returnDate;
	}

	public static int parseStrToInteger(String strValue) {
		int intValue = 0;
		try {
			intValue = Integer.parseInt(strValue);
		} catch (Exception e) {
			intValue = 0;
		}
		return intValue;
	}

	public static double parseStrToDouble(String strValue) {
		double doubleValue = 0.0F;
		try {
			doubleValue = Double.parseDouble(strValue);
		} catch (Exception e) {
			doubleValue = 0.0F;
		}

		return doubleValue;
	}

	public static Date dateAddSubtract(Date date, int day, int week, int month, int year) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		if (day != 0) {
			calendar.add(Calendar.DATE, day);
			return calendar.getTime();
		} else if (week != 0) {
			calendar.add(Calendar.WEEK_OF_MONTH, week);
			return calendar.getTime();
		} else if (month != 0) {
			calendar.add(Calendar.MONTH, month);
			return calendar.getTime();
		} else if (year != 0) {
			calendar.add(Calendar.YEAR, year);
			return calendar.getTime();
		}
		return null;

	}

	public static boolean validateString(String value) {
		if (value != null && value != " " && !value.trim().isEmpty())
			return true;
		return false;
	}

	public static String formatPhoneNumber(String phoneNum) {

		MessageFormat format = new MessageFormat("+1({0})-{1}-{2}");
		if (validateString(phoneNum)) {
			String returnphoneNum = phoneNum.replace("-", "");
			if ((returnphoneNum.length() > 10) && (isPhoneNumberValid(returnphoneNum))) {
				String[] phoneNumArr = { returnphoneNum.substring(0, 3), returnphoneNum.substring(3, 6),
						returnphoneNum.substring(6) };
				return format.format(phoneNumArr);
			}
		}
		return null;
	}

	public static String removeSpaces(String str) {
		return str.replaceAll("\\s", "");
	}

	// validate first name
	public static boolean validateFirstName(String firstName) {
		if (firstName != null) {
			return firstName.matches("[A-Za-z]*");
		}
		return false;
	}

	// validate last name
	public static boolean validateLastName(String lastName) {
		if (lastName != null) {
			return lastName.matches("([a-zA-Z '-])*");
		}
		return false;
	}

	public static boolean isEmailValid(String email) {
		if (email != null) {
			return email.matches("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$");
		}
		return false;
	}

	public static boolean isPasswordValid(String password) {
		if (password != null) {
			return password.matches("((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{8,40})");
		}
		return false;
	}

	public static boolean isPhoneNumberValid(String phone){
		if ((phone != null) && ((phone.length()>=10) && (phone.length()<=14))){
			return phone.matches("(\\d{2})(\\d{3})([-]?)(\\d{3})([-]?)(\\d{4}))");
		}
		return false;
	}
}
