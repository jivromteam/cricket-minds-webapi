package com.jivrom.cricketminds.common.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateUtil {
	
	private static String DATE_FORMAT = "MM-dd-yyyy";
	
	public static String MONTH_FORMAT = "MMM-yyyy";

	public static Date parseDate(String date) {
		DateFormat df = new SimpleDateFormat(DATE_FORMAT);
		
		try {
			return df.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RuntimeException("Invalid date format provided, date :  "+date);
		}
	}
	
	public static String formatDate(Date date) {
		DateFormat df = new SimpleDateFormat(DATE_FORMAT);
		return df.format(date);
	}
	
	public static String formatDate(Date date, String format) {
		DateFormat df = new SimpleDateFormat(format);
		return df.format(date);
	}
	
	public static Date nextDay(Date date) {
		Calendar calender = Calendar.getInstance();
		calender.setTime(date);
		
		calender.add(Calendar.DATE, 1);
		return calender.getTime();
	}
	
	public static Date monthEnd(Date date) {
		Calendar calender = Calendar.getInstance();
		calender.setTime(date);
		
		calender.set(Calendar.DAY_OF_MONTH, calender.getActualMaximum(Calendar.DAY_OF_MONTH));
		
		return calender.getTime();
	}
	
	public static Date earliest(Date date1, Date date2) {
		if(date1.after(date2)) {
			return date2;
		} else {
			return date1;
		}
	}
	
	public static int getHour(Date date) {
		Calendar calender = Calendar.getInstance();
		calender.setTime(date);
		
		return calender.get(Calendar.HOUR_OF_DAY);
	}
	
	public static int getMin(Date date) {
		Calendar calender = Calendar.getInstance();
		calender.setTime(date);
		
		return calender.get(Calendar.MINUTE);
	}
	
	public static List<Calendar> datesBetween(Date startDate, Date endDate) {
		
		List<Calendar> datesInRange = new ArrayList<>();
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(endDate);
		
		while (calendar.before(endCalendar)) {
	        Calendar dateCalendar = Calendar.getInstance();
	        dateCalendar.setTime(calendar.getTime());
	        datesInRange.add(dateCalendar);
	        calendar.add(Calendar.DATE, 1);
	    }
		
		return datesInRange;
	}
	
	
	
	public static void main(String[] args) {
		Date date = DateUtil.parseDate("12-21-2016");
		System.out.println(date);
		
		String str  = DateUtil.formatDate(new Date());
		System.out.println(str);
		
		Calendar todayCalendar = Calendar.getInstance();
		todayCalendar.add(Calendar.DAY_OF_WEEK, 1);
		int val = todayCalendar.get(Calendar.DAY_OF_WEEK);
		System.out.println("dy of week valuke  :"+val+", date :"+todayCalendar.getTime());
		
		String cron = "0 0 20-20 ? * SUN,TUE,THU,SAT *";
		System.out.println("day : "+cron.split(" ")[5]);
		
		String month = DateUtil.formatDate(new Date(), MONTH_FORMAT);
		System.out.println("month : "+month);
	}
}
