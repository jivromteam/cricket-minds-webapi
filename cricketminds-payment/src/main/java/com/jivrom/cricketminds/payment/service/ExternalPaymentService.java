package com.jivrom.cricketminds.payment.service;

import com.jivrom.cricketminds.payment.dto.PaymentDTO;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;

public interface ExternalPaymentService {

	public Charge charge(PaymentDTO paymentDTO) throws StripeException;
}
