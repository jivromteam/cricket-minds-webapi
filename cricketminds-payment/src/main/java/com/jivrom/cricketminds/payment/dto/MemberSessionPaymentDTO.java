package com.jivrom.cricketminds.payment.dto;

import java.util.List;

public class MemberSessionPaymentDTO extends PaymentDTO {

	private List<Long> sessionPaymentIds;
	
	public MemberSessionPaymentDTO() {
		super();
	}

	public List<Long> getSessionPaymentIds() {
		return sessionPaymentIds;
	}

	public void setSessionPaymentIds(List<Long> sessionPaymentIds) {
		this.sessionPaymentIds = sessionPaymentIds;
	}
	
	
}
