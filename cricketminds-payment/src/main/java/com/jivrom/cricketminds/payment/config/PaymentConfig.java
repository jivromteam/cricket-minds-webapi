package com.jivrom.cricketminds.payment.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = "com.jivrom.cricketminds.payment")
@Configuration
public class PaymentConfig {

}
