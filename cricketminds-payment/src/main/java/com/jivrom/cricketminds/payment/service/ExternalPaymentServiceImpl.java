package com.jivrom.cricketminds.payment.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.jivrom.cricketminds.payment.dto.PaymentDTO;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;

@Service
public class ExternalPaymentServiceImpl implements ExternalPaymentService {

	@Value("${stripe.api.key}")
	private String apiKey;
	
	private static final String CURRENCY = "usd";
	
	@Override
	public Charge charge(PaymentDTO paymentDTO) throws StripeException {
		Stripe.apiKey = apiKey;
		
		Map<String, Object> chargeParams = new HashMap<String, Object>();
		chargeParams.put("amount", paymentDTO.getAmount()); // amount in cents, again
		chargeParams.put("currency", CURRENCY);
		chargeParams.put("source", paymentDTO.getToken());
		chargeParams.put("description", paymentDTO.getDescription());
		
		try {
			Charge retval = Charge.create(chargeParams);
			System.out.println("retval : "+retval);
			return retval;
		} catch (StripeException e) {
			e.printStackTrace();
			throw e;
		}
		
	}

}
