var cricketmindsApp = angular.module('cricketmindsApp', ['ui.router']);


cricketmindsApp.config(function($stateProvider, $urlRouterProvider) {
	
	$urlRouterProvider.otherwise('/login');
	
	$stateProvider.state('login', {
		url: '/login',
		//controller:'loginController',
        templateUrl: 'partials/login.html'
//		views: {
//			'': {
//                controller:'loginController',
//                templateUrl: 'partials/login.html'
//            }
//		}
	})
	
	.state('register', {
		url: '/register',
		//controller:'loginController',
        templateUrl: 'partials/register.html'
	})
	
	
	.state('home', {
        url: '/home',
        templateUrl: 'partials/partial-home.html'
    })
    
     // nested list with custom controller
    .state('home.list', {
        url: '/list',
        templateUrl: 'partials/partial-home-list.html',
        controller: function($scope) {
            $scope.dogs = ['Bernese', 'Husky', 'Goldendoodle'];
        }
    })
    
    // nested list with just some random string data
    .state('home.paragraph', {
        url: '/paragraph',
        template: 'I could sure use a drink right now.'
    })
    
    .state('about', {
        url: '/about',
        views: {

            // the main template will be placed here (relatively named)
            '': { templateUrl: 'partials/partial-about.html' },

            // the child views will be defined here (absolutely named)
            'columnOne@about': { template: 'Look I am a column!' },

            // for column two, we'll define a separate controller 
            'columnTwo@about': { 
                templateUrl: 'partials/table-data.html',
                controller: 'scotchController'
            }
        }

    });
});



// Controllers
cricketmindsApp.controller('homeController', [ '$scope', '$state',
	function($scope, $state) {
		$scope.user = 'dev';

		$scope.username = 'World';

		$scope.sayHello = function() {
			$scope.greeting = 'Hello  1 ' + $scope.username + '!';
		};
		
		$scope.goLogin = function() {
			alert("go login");
			$state.go('login');
		};
	} ]);


cricketMindsApp.controller('loginController', [ '$scope',
		function($scope) {
			$scope.user = 'dev';

			$scope.username = 'World';

			
		} ]);


cricketmindsApp.controller('scotchController', function($scope) {

    $scope.message = 'test';

    $scope.scotches = [
        {
            name: 'Macallan 12',
            price: 50
        },
        {
            name: 'Chivas Regal Royal Salute',
            price: 10000
        },
        {
            name: 'Glenfiddich 1937',
            price: 20000
        }
    ];

});