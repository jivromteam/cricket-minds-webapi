

app.service('treatmentErrorData', ['$http', function ($http) {
  var service = {};
  service.getTreatmentErrorData = function (data, success, error) {
    $http.get(CONSTANTS.DEV_URL + "dcloader/treatmentLoaderErrors/shiftNumber/" + data.shiftId + "/shiftDate/" + data.shiftDate + "/status/" + data.status).success(function (items) {
      success(items);
    }).error(function (data, status) {
      error('Repos error', status);
    })
  }

  service.getShifts = function(){
    return [{ "shiftId": 1 }, { "shiftId": 1 }, { "shiftId": 3 }, { "shiftId": 4 }, { "shiftId": 5 }, { "shiftId": 6 }, { "shiftId": 7 }];
  }

  service.getStatus = function(){
    return [{ "status": 'FAILED' }, { "status": 'ERROR' }];
  }

  return service;
}]);
