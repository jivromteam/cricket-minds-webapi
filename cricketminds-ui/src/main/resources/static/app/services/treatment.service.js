

app.service('treatmentData', ['$http', function ($http) {
  var service = {};
  service.getTreatmentData = function (data, success, error) {
    $http.get(CONSTANTS.DEV_URL + "dcloader/treatmentLoaderRequests/clinicId/" + data.clinicId + "/shiftDate/" + data.shiftDate + "").success(function (items) {
      success(items);
    }).error(function (data, status) {
      error('Repos error', status);
    })
  }

  service.getClinics = function (success, error) {
      console.log(success, error)
    $http.get("http://hal-sboot-q01:7778/dama/physician/TNurse/clinics").success(function (items) {
       success(items);
    }).error(function (data, status) {
      error('Repos error', status);
    })
}


  // service.getClinics = function () {
  //   return [{ "clinicId": 2019, "name": "Nanticoke" },
  //   { "clinicId": 1044, "name": "Carbon County" },
  //   { "clinicId": 1120, "name": "Philadelphia" },
  //   { "clinicId": 1123, "name": "Pottsville" },
  //   { "clinicId": 1271, "name": "Wilkes Barre" }];
  // }

  return service;
}]);
