

app.service('gridData', ['$http', function ($http) {
  var service = {};
  service.getGridData = function (data,success, error) {
    $http.get(CONSTANTS.DEV_URL + "dcloader/dcLoaderFailures/fromDate/" + data.startDate + "/toDate/" + data.endDate + "/status/ERROR").success(function (items) {
      success(items);
    }).error(function(data, status) {
      error('Repos error', status);
    })
  }
  return service;
}]);
