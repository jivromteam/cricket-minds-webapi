app.service('loadingQueueService', [function () {
    var srv = {};
    srv.loadingQueue = [];
    srv.timer = null;

    srv.add = function (task) {
        srv.loadingQueue.push(task);
        var spinner = document.getElementById('spinner');
        spinner.style.display = 'block';
    };

    srv.remove = function (task) { document.getElementById('spinner').style.display = 'none'; };

    srv.isEmpty = function () {
        if (srv.loadingQueue.length === 0) {
            return true;
        } else {
            return false;
        }
    };

    srv.clear = function () {
        srv.loadingQueue.length = 0;
        document.getElementById('spinner').style.display = 'none';
    };

    srv.getQueue = function () { return srv.loadingQueue; };

    return srv;

}]);
