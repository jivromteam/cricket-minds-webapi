function GRID_DATA_COLUMN() {
    return {
        columnDefs: [

            {
                field: 'resourceId',
                displayName: 'Resource Id',
                filter: {
                    placeholder: 'Search Resource Id',
                },
                enableSorting: true,
                width: '*',
            }, {
                field: 'resourceType',
                displayName: 'Resource Type',
                filter: {
                    placeholder: 'resourceType',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'resourceVersion',
                displayName: 'Resource Version',
                filter: {
                    placeholder: 'Resource Version',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'referencedId',
                displayName: 'Referenced Id',
                filter: {
                    placeholder: 'Referenced Id',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'referencedType',
                displayName: 'Referenced Type',
                filter: {
                    placeholder: 'Referenced Type',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'status',
                displayName: 'Status',
                filter: {
                    placeholder: 'Status',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'receivedDate',
                displayName: 'Received Date',
                filter: {
                    placeholder: 'Received Date',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'updatedDate',
                displayName: 'Updated Date',
                filter: {
                    placeholder: 'Updated Date',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'exception',
                displayName: 'Exception',
                filter: {
                    placeholder: 'Exception',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'errorMessage',
                displayName: 'Error Message',
                filter: {
                    placeholder: 'Error Message',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'source',
                displayName: 'Source',
                filter: {
                    placeholder: 'Source',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'reprocessCount',
                displayName: 'Reprocess Count',
                filter: {
                    placeholder: 'Reprocess Count',
                },
                enableSorting: true,
                width: '*',
            }
        ],
        enableColumnResizing: true,
        enableColumnMenus: false,
        enableFiltering: true,
        paginationPageSizes: [10, 25, 50, 75],
        paginationPageSize: 10,
        rowHeight: 30,
    };
}

function TREATMENT_COLUMN() {
    return {
        columnDefs: [

            {
                field: 'shiftId',
                displayName: 'Shift Id',
                filter: {
                    placeholder: 'Shift Id',
                },
                enableSorting: true,
                width: '*',
            }, {
                field: 'clinicId',
                displayName: 'Clinic Id',
                filter: {
                    placeholder: 'Clinic Id',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'shiftNumber',
                displayName: 'Shift Number',
                filter: {
                    placeholder: 'Shift Number',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'shiftDate',
                displayName: 'Shift Date',
                filter: {
                    placeholder: 'Shift Date',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'shiftStatus',
                displayName: 'Shift Status',
                filter: {
                    placeholder: 'Shift Status',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'responseStatus',
                displayName: 'Response Status',
                filter: {
                    placeholder: 'Response Status',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'errorReason',
                displayName: 'Error Reason',
                filter: {
                    placeholder: 'Error Reason',
                },
                enableSorting: true,
                width: '*',
            }
        ],
        enableColumnResizing: true,
        enableColumnMenus: false,
        enableFiltering: true,
        paginationPageSizes: [10, 25, 50, 75],
        paginationPageSize: 10,
        rowHeight: 30,
    };
}

function TREATMENT_ERROR_COLUMN() {
    return {
        columnDefs: [

            {
                field: 'resourceId',
                displayName: 'Resource Id',
                filter: {
                    placeholder: 'Search Resource Id',
                },
                enableSorting: true,
                width: '*',
            }, {
                field: 'resourceType',
                displayName: 'Resource Type',
                filter: {
                    placeholder: 'resourceType',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'resourceVersion',
                displayName: 'Resource Version',
                filter: {
                    placeholder: 'Resource Version',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'referencedType',
                displayName: 'Referenced Type',
                filter: {
                    placeholder: 'Referenced Type',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'status',
                displayName: 'Status',
                filter: {
                    placeholder: 'Status',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'receivedDate',
                displayName: 'Received Date',
                filter: {
                    placeholder: 'Received Date',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'updatedDate',
                displayName: 'Updated Date',
                filter: {
                    placeholder: 'Updated Date',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'exception',
                displayName: 'Exception',
                filter: {
                    placeholder: 'Exception',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'errorMessage',
                displayName: 'Error Message',
                filter: {
                    placeholder: 'Error Message',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'source',
                displayName: 'Source',
                filter: {
                    placeholder: 'Source',
                },
                enableSorting: true,
                width: '*',
            },
            {
                field: 'reprocessCount',
                displayName: 'Reprocess Count',
                filter: {
                    placeholder: 'Reprocess Count',
                },
                enableSorting: true,
                width: '*',
            }
        ],
        enableColumnResizing: true,
        enableColumnMenus: false,
        enableFiltering: true,
        paginationPageSizes: [10, 25, 50, 75],
        paginationPageSize: 10,
        rowHeight: 30,
    };
}

function DATE_OPTIONS() {
    return {
        formatYear: 'yy',
        startingDay: 1
    }
}