
function pad(val) {
    var valString = val + "";
    if (valString.length < 2) {
        return "0" + valString;
    }
    else {
        return valString;
    }
}

function dateFormat(date) {
    let newDate = new Date(date);
    let year = newDate.getFullYear();
    let month = newDate.getMonth() + 1;
    let day = date.getDate();
    let stringDate = pad(month) + pad(day) + year;
    return stringDate;
}

function dateDisplayFormat() {
    return 'MM-dd-yyyy';
}

function openDropDown(e) {
    var menu = e.target.parentNode.parentNode.children[0];
    setTimeout(function () {
        angular.element(menu).triggerHandler('click');
    }, 14);
}