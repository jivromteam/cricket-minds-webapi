
var app = angular.module('tacWorkflowWebApp', ['ui.router', 'ngResource', 'ui.bootstrap', 'ui.grid', 'ui.grid.pagination'])
app.config(function ($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('grid-data');

	$stateProvider
		.state("grid-data", {
			url: '/grid-data',
			templateUrl: "./app/view/grid-data.html",
			controller: 'GridDataController'

		})
		.state('treatment', {
			url: '/treatment',
			templateUrl: './app/view/treatment.html',
			controller: 'TreatmentController'
		})

		.state('treatment-error', {
			url: '/treatment-error',
			templateUrl: './app/view/treatment-error.html',
			controller: 'TreatmentErrorController'
		})


}).value('app.config', {
	basePath: '/tac-workflow'
});


app.filter('filterDropdown', function () {

	return function (input, search) {

		search = !search ? "" : search.toString();
		if (search === undefined || search.trim() === '') return input;

		search = search.toLowerCase();

		var output = [];

		angular.forEach(input, function (row) {
			if (row.clinicId.toString().indexOf(search) === 0 || row.name.toLowerCase().indexOf(search) !== -1) {
				output.push(row);
			}
		});

		if (output.length === 0) return input;

		return output;
	};
});
