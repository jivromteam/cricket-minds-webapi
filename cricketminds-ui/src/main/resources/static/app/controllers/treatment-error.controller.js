
app.controller('TreatmentErrorController', ['$scope', '$filter', '$window', 'treatmentErrorData', '$timeout', 'loadingQueueService', function ($scope, filter, window, treatmentErrorData, $timeout, loadingQueueService) {

	loadingQueueService.remove('treatment-error');
	$scope.gridOptions = TREATMENT_ERROR_COLUMN();
	$scope.shifts = treatmentErrorData.getShifts();
	$scope.status = treatmentErrorData.getStatus();
	$scope.format = 'MM-dd-yyyy';

	$scope.treatmentErrorData = [];
	$scope.shiftDate = new Date();
	$scope.shiftId = $scope.shifts ? $scope.shifts[0].shiftId : "";
	$scope.statusId = $scope.status ? $scope.status[0].status : "";

	$scope.searchShiftVal = $scope.shiftId;
	$scope.searchStatusVal = $scope.statusId;

	$scope.init = function (data) {
		loadingQueueService.add('treatment-error');
		$scope.gridOptions.data = [];
		treatmentErrorData.getTreatmentErrorData(data, function (response) {
			$scope.gridOptions.data = response.data["DC loader errors"];
			loadingQueueService.remove('treatment-error');
		}, function (error) {
			loadingQueueService.remove('treatment-error');
		});
	}

	// $scope.init({ "shiftId": $scope.shiftId, "status": $scope.statusId, "shiftDate": dateFormat($scope.shiftDate) });

	$scope.searchRecord = function () {
		if ($scope.shiftDate) {
			$scope.init({ "shiftId": $scope.shiftId, "status": $scope.statusId, "shiftDate": dateFormat($scope.shiftDate) });
		}
	}

	$scope.selectShift = function (item) {
		$scope.searchShiftVal = item.shiftId;
		$scope.shiftId = item.shiftId;

	}

	$scope.selectStatus = function (item) {
		$scope.searchStatusVal = item.status;
		$scope.statusId = item.status;
	}

	$scope.showMenu = function (e) {
		openDropDown(e);
	};

	$scope.showStatus = function (e) {
		openDropDown(e);
	};

}]);
