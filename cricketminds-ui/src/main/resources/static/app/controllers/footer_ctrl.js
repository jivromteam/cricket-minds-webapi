
app.controller('footerController', ['$scope', function ($scope) {


    //footer date
    $scope.getYear = function() {
        var date = new Date();
        var day = date.getDate();
        var month = date.getMonth() + 1;
        day = (day < 10) ? '0' + day : day;
        month = (month < 10) ? '0' + month : month;
        var year = date.getFullYear();
        return month + '/' + day + '/' + year;
    };

    //copyright year
    $scope.getCopyRightYear = function() {
        var date = new Date();
        var year = date.getFullYear();
        return year;
    };

    //get current time for footer
    $scope.getTime = function() {
        var date = new Date();
        var dateStr = 'AM';
        var hour = date.getHours();

        if (hour > 12) {
            hour = hour -12;
            dateStr = 'PM';
        }

        var min  = date.getMinutes();
        min = (min < 10 ? "0" : "") + min;

        return hour + ":" + min + ' ' + dateStr;
    };



}]);
