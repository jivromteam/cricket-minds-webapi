
app.controller('TreatmentController', ['$scope', '$filter', '$window', 'treatmentData', '$timeout', 'loadingQueueService', function ($scope, filter, window, treatmentData, $timeout, loadingQueueService) {

	$scope.gridOptions = TREATMENT_COLUMN();
	$scope.dateOptions = DATE_OPTIONS();
	$scope.format = dateDisplayFormat();
	// $scope.clinics = treatmentData.getClinics();
	$scope.clinics = [];
	// $scope.searchVal =$scope.clinics[0].clinicId+ ' - ' +  $scope.clinics[0].name ;
    // $scope.clinicId = $scope.clinics ? $scope.clinics[0].clinicId : 0;
	loadingQueueService.remove('treatment-data');

	$scope.treatmentData = [];
	$scope.clinicId = '';
	$scope.shiftDate = new Date();


	$scope.init = function (data) {
		loadingQueueService.add('treatment-data');
		$scope.gridOptions.data = [];
		treatmentData.getTreatmentData(data, function (response) {
			$scope.gridOptions.data = response.data["DC loader errors"];
			loadingQueueService.remove('treatment-data');
		}, function (error) {
			loadingQueueService.remove('treatment-data');
		});
	}

	$scope.getClinic = function(){

		treatmentData.getClinics(function (response) {
			$scope.clinics  = response.data.physician.clinics;
			// $scope.searchVal =$scope.clinics[0].clinicId+ ' - ' +  $scope.clinics[0].name ;
			$scope.clinicId = $scope.clinics ? $scope.clinics[0].clinicId : 0;
			loadingQueueService.remove('treatment-data');
		}, function (error) {

			loadingQueueService.remove('treatment-data');
		});
	}
	$scope.getClinic();

	// $scope.init({ "clinicId": $scope.clinicId, "shiftDate": dateFormat($scope.shiftDate) });

	$scope.searchRecord = function () {
		if ($scope.shiftDate) {
			$scope.init({ "clinicId": $scope.clinicId, "shiftDate": dateFormat($scope.shiftDate) });
		}
	}

	$scope.selectClinic = function (item) {
		$scope.searchVal = item.clinicId + ' - ' + item.name;
		$scope.clinicId = item.clinicId;
	}

	$scope.showMenu = function (e) {
		openDropDown(e);
	};

	$scope.clearMenu = function () {
		$scope.searchVal = "";
	}


}]);
