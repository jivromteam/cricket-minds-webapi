# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###



### How do I get set up? ###

**Pre-requisite for the set up:**
	_Install Java8_
	_Install Apache Maven_

**Build steps,**

run below command from the root folder (current location)
`mvn clean install`

once the build is successful, execute below command to start the server

`java -jar cricketminds-app/target/cricketminds.jar`

look for below statement to make sure application is started
`com.jivrom.cricketmindds.app.Application  : Started Application`

`http://localhost:2005/cricketminds/`

### Contribution guidelines ###



### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact