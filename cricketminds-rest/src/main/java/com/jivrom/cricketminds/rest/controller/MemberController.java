package com.jivrom.cricketminds.rest.controller;

import java.io.Console;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.validation.constraints.Null;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import groovy.transform.stc.PickAnyArgumentHint;

import com.jivrom.cricketminds.domain.dto.ErrorDTO;
import com.jivrom.cricketminds.domain.dto.ForgotPasswordDTO;
import com.jivrom.cricketminds.domain.dto.PasswordDTO;
import com.jivrom.cricketminds.domain.dto.ProfileDTO;
import com.jivrom.cricketminds.domain.dto.ProfilePictureDTO;
import com.jivrom.cricketminds.domain.dto.RegistrationDTO;
import com.jivrom.cricketminds.domain.dto.ResponseDTO;
import com.jivrom.cricketminds.domain.dto.UserDTO;
import com.jivrom.cricketminds.domain.exception.UserExistsException;
import com.jivrom.cricketminds.domain.exception.UserNotFoundException;
import com.jivrom.cricketminds.domain.model.Member;
import com.jivrom.cricketminds.domain.model.MemberStatusEnum;
import com.jivrom.cricketminds.domain.repository.MemberRepository;
import com.jivrom.cricketminds.domain.service.AuthenticationService;
import com.jivrom.cricketminds.domain.service.UserService;
import com.jivrom.cricketminds.domain.service.ValidationService;


@RestController 
public class MemberController {

	@Resource
	private MemberRepository memberRepository;
	
	@Resource
	private ValidationService validationService;

	@Resource
	private AuthenticationService authenticationService;

	@Resource
	private UserService userService;
	public static Logger logger=Logger.getLogger("global");


    @RequestMapping(value = "register", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO> register(@RequestBody RegistrationDTO registrationDTO) {
		ResponseDTO response = new ResponseDTO();

		ErrorDTO error = validationService.register(registrationDTO);
		if (error != null) {

			response.setError(error);
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);
		}

		// BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		// registrationDTO.setPassword(passwordEncoder.encode(registrationDTO.getPassword()));
		
		try {
			Member member = authenticationService.register(registrationDTO);
			if (member != null) {
				Map<String, Object> data = new HashMap<>();
				data.put("data", "success");
				response.setResponse(data);
				return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
			}
		} catch(UserExistsException unfe) {
			ErrorDTO errorDTO = new ErrorDTO("User exists with this emailId, please login using: " + registrationDTO.getEmailId(), "CM102");
			 response.setError(errorDTO);
			 return new ResponseEntity<ResponseDTO>(response, HttpStatus.FORBIDDEN);
		}catch(Exception e) {
			e.printStackTrace();
			
			ErrorDTO err = new ErrorDTO(e.getMessage(), e.getMessage());
			response.setError(err);
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return null;
	}


	@RequestMapping(value = "members", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getMembers() {

		ResponseDTO response = new ResponseDTO();

		List<UserDTO> userResponse = userService.getAllMembers();

		Map<String, Object> responseMap = new HashMap<>();
		responseMap.put("data", userResponse);

		response.setResponse(responseMap);

		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "members/{status}", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getMembersByStatus(@PathVariable String status) {

		ResponseDTO response = new ResponseDTO();

		List<UserDTO> userResponse = userService.getMembers(status);

		Map<String, Object> responseMap = new HashMap<>();
		responseMap.put("data", userResponse);

		response.setResponse(responseMap);

		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "member/{memberId}", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getMemberId(@PathVariable Long memberId) {
		System.out.println("given memberId : " + memberId);
		ResponseDTO response = new ResponseDTO();
		UserDTO memberDto = userService.getMember(memberId);

		Map<String, Object> responseMap = new HashMap<>();
		responseMap.put("data", memberDto);

		response.setResponse(responseMap);

		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "member", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getMemberByEmailId(
			@RequestParam(required = true, name = "emailId") String emailId) {
		System.out.println("given emailId : " + emailId);
		ResponseDTO response = new ResponseDTO();
		UserDTO memberDto = userService.getMember(emailId);

		Map<String, Object> responseMap = new HashMap<>();
		responseMap.put("data", memberDto);

		response.setResponse(responseMap);

		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/member/{memberId}/updatestatus/{updateStatus}", method = RequestMethod.PUT)
	public ResponseEntity<ResponseDTO> updateSatus(@PathVariable Long memberId, @PathVariable String updateStatus) {

		System.out.println("given data memberId:" + memberId + ", updateStatus: " + updateStatus);

		ResponseDTO response = new ResponseDTO();

		Member member = memberRepository.findOne(memberId);
		MemberStatusEnum approvalStatus = MemberStatusEnum.fromValue(updateStatus);

		System.out.println("approvalStatus : " + approvalStatus);
		if (!approvalStatus.isNull()) {
			member.setStatus(approvalStatus.getStatus());
			memberRepository.save(member);
		} else {

			ErrorDTO errorDTO = new ErrorDTO("Provided status is invalid, status:" + updateStatus, "CM11");
			response.setError(errorDTO);

			return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);
		}

		Map<String, Object> responseMap = new HashMap<>();
		responseMap.put("memberId", memberId);
		responseMap.put("approvalStatus", approvalStatus.getStatus());

		responseMap.put("status", "success");

		Map<String, Object> data = new HashMap<>();
		data.put("data", responseMap);
		response.setResponse(data);
		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/member/{memberId}/type/{type}", method = RequestMethod.PUT)
	public ResponseEntity<ResponseDTO>updateMemberType(@PathVariable Long memberId,@PathVariable String type) {

		System.out.println("given data memberId:" + memberId +  ", type: " + type);

		ResponseDTO response = new ResponseDTO();

		userService.updateMemberType(memberId, type);

		UserDTO userDTO = userService.getMember(memberId);

		/*
		 * ErrorDTO errorDTO = new
		 * ErrorDTO("Provided status is invalid, status:"+updateType, "CM11");
		 * response.setError(errorDTO);
		 * 
		 * return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);
		 */

		Map<String, Object> responseMap = new HashMap<>();
		responseMap.put("data", userDTO);

		responseMap.put("status", "success");

		response.setResponse(responseMap);
		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
	}

	// TODO: use different dto for RequestBody
	@RequestMapping(value = "profile", method = RequestMethod.PUT)
	public Object updateProfile(@RequestBody Member member) {
		System.out.println("member : " + member);

		System.out.println("off date :L " + member.getCreatedDate());

		return memberRepository.save(member);
	}

	// profile picture

	@RequestMapping(value = "profilePicture", method = RequestMethod.PUT)
	public ResponseEntity<ResponseDTO> updateProfilePicture(
		@RequestBody ProfilePictureDTO profilepicturedto) {

			ResponseDTO response = new ResponseDTO();
			Map<String, Object> responseMap = new HashMap<>();

		try {
		 Long memberId=profilepicturedto.getMemberId();
		 String profilePictureUrl=profilepicturedto.getProfilePictureUrl();
		 UserDTO userDTO=userService.getMember(memberId);

			userDTO.setProfilePictureUrl(profilePictureUrl);
			userService.updateProfilePictureUrl(memberId, profilePictureUrl);
			responseMap.put("data", userDTO);
			responseMap.put("status", "success");
	
			response.setResponse(responseMap);
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);

			
	   } catch(UserNotFoundException unfe) {
		   ErrorDTO errorDTO = new ErrorDTO("Provided MemberId is invalid, MemberId:" + profilepicturedto.getMemberId(), "CM11");
			response.setError(errorDTO);
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);
	   } catch (Exception e) {
		   ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
        
		   response.setError(errorDTO);
		   responseMap.put("status", "Internal Sever Error");
		response.setResponse(responseMap);
		
		return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}

	   //profileupdate

	@RequestMapping(value = "updateProfile/{memberId}", method = RequestMethod.PUT)
	public ResponseEntity<ResponseDTO> updateProfile(@PathVariable Long memberId,
		@RequestBody ProfileDTO profiledto) {
			ResponseDTO response = new ResponseDTO();
			Map<String, Object> responseMap = new HashMap<>();

			try {
				UserDTO userDto=userService.updateMember(memberId,profiledto.getFirstName(),profiledto.getLastName(),profiledto.getPhone(),profiledto.getDateOfBirth(), profiledto.getProfilePicUrl());

				responseMap.put("data", userDto);
				responseMap.put("status", "success");
				
				response.setResponse(responseMap);
				return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
				
			} catch (Exception e) {
				responseMap.put("data", null);
		        responseMap.put("status", "Internal Sever Error");
		        response.setResponse(responseMap);
		        return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		//password reset
		
		@RequestMapping(value="resetPassword", method=RequestMethod.PUT)
		public ResponseEntity<ResponseDTO> passwordReset(@RequestBody PasswordDTO passwordDTO) {

			ResponseDTO response = new ResponseDTO();
			Map<String, Object> responseMap = new HashMap<>();

			try {

				UserDTO userDto=userService.resetPassword(passwordDTO.getMemberId(),passwordDTO.getPassword());

				responseMap.put("data", userDto);
				responseMap.put("status", "success");

				response.setResponse(responseMap);
				return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
				
			} catch (Exception e) {
				responseMap.put("data", null);
		        responseMap.put("status", "Internal Sever Error");
		        response.setResponse(responseMap);
		        return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		//forgot password

		@RequestMapping(value="forgotPassword", method=RequestMethod.POST)
		public ResponseEntity<ResponseDTO> forgotPassword(@RequestBody ForgotPasswordDTO forgotPasswordDTO) {


			ResponseDTO response = new ResponseDTO();
			Map<String, Object> responseMap = new HashMap<>();

			try {
				
				UserDTO userDto=userService.forgotPassword(forgotPasswordDTO.getEmailId());

				responseMap.put("data", userDto);
                responseMap.put("status", "success");
				response.setResponse(responseMap);
				return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);

			} catch(UserNotFoundException unfe) {
				ErrorDTO errorDTO = new ErrorDTO("Provided emailId is invalid, EmailId:" + forgotPasswordDTO.getEmailId(), "CM11");
				 response.setError(errorDTO);
				 return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);

			} catch (Exception e) {
				ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
			 
				response.setError(errorDTO);
				responseMap.put("status", "Internal Sever Error");
			    response.setResponse(responseMap);
			 
			 return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		}

		//delete a member

		@RequestMapping(value="deleteMember/{memberId}", method=RequestMethod.DELETE)
		public ResponseEntity<ResponseDTO> deleteMemberById(@PathVariable Long memberId){

			ResponseDTO response = new ResponseDTO();
			Map<String, Object> responseMap = new HashMap<>();

			try {
				
				UserDTO memberDto = userService.deleteMember(memberId);
				responseMap.put("data", memberDto);
                responseMap.put("status", "success");
				response.setResponse(responseMap);
				return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);



			} catch(UserNotFoundException unfe) {
				ErrorDTO errorDTO = new ErrorDTO("Provided memberId is invalid", null);
				 response.setError(errorDTO);
				 return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);

			} catch (Exception e) {
				ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
			 
				response.setError(errorDTO);
				responseMap.put("status", "Internal Sever Error");
			    response.setResponse(responseMap);
			 
			 return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}

				
			}
			
		}
		
		
		



		



	
	




