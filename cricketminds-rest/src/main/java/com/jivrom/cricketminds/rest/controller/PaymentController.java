package com.jivrom.cricketminds.rest.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jivrom.cricketminds.domain.dto.ErrorDTO;
import com.jivrom.cricketminds.domain.dto.MemberPaymentDTO;
import com.jivrom.cricketminds.domain.dto.PaymentDueDTO;
import com.jivrom.cricketminds.domain.dto.ResponseDTO;
import com.jivrom.cricketminds.domain.dto.SessionPaymentDTO;
import com.jivrom.cricketminds.domain.service.MemberPaymentService;
import com.jivrom.cricketminds.domain.service.PaymentService;
import com.jivrom.cricketminds.payment.dto.MemberSessionPaymentDTO;

@RestController
public class PaymentController {
	
	@Resource
	private MemberPaymentService memberPaymentService;
	
	@Resource
	private PaymentService paymentService;

	@RequestMapping(value = "payments", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getAllPayments() {

		ResponseDTO response = new ResponseDTO();
		
		List<MemberPaymentDTO> dto = memberPaymentService.getAllPayments();

		Map<String, Object> responseMap = new HashMap<>();
		responseMap.put("data", dto);

		response.setResponse(responseMap);

		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "payments/member/{memberId}", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getAllPaymentsByMember(@PathVariable Long memberId) {

		ResponseDTO response = new ResponseDTO();
		
		List<MemberPaymentDTO> dto = memberPaymentService.getAllPayments(memberId);

		Map<String, Object> responseMap = new HashMap<>();
		responseMap.put("data", dto);
		

		response.setResponse(responseMap);

		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "payments/due", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getAllPaymentsDue() {

		ResponseDTO response = new ResponseDTO();
		
		List<SessionPaymentDTO> paymentDues = paymentService.getPaymentDue();
		

		Map<String, Object> responseMap = new HashMap<>();
		responseMap.put("data", paymentDues);
		

		response.setResponse(responseMap);

		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "payments/member/{memberId}/due", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getAllPaymentsDueByMember(@PathVariable Long memberId) {

		ResponseDTO response = new ResponseDTO();
		
		PaymentDueDTO paymentDue = paymentService.getPaymentDue(memberId);
		

		Map<String, Object> responseMap = new HashMap<>();
		responseMap.put("data", paymentDue);
		

		response.setResponse(responseMap);

		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value="member/{memberId}/sessionpayment", method=RequestMethod.PUT)
    public ResponseEntity<ResponseDTO> makePayment(@PathVariable Long memberId, @RequestBody MemberSessionPaymentDTO sessionPaymentDTO) {
    	
    	ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();
        
    	try {
    		
    		paymentService.makeSessionPayment(memberId, sessionPaymentDTO);
    		    		
            responseMap.put("status", "success");
			response.setResponse(responseMap);
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
			
    	} catch(Exception e) {
    		e.printStackTrace();
    		
    		ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
            
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
            
    		return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
    }
}
