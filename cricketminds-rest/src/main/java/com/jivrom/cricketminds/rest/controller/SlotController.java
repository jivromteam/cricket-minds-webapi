package com.jivrom.cricketminds.rest.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jivrom.cricketminds.domain.dto.AssignCoachDTO;
import com.jivrom.cricketminds.domain.dto.CoachDTO;
import com.jivrom.cricketminds.domain.dto.CreateSlotDTO;
import com.jivrom.cricketminds.domain.dto.ErrorDTO;
import com.jivrom.cricketminds.domain.dto.ResponseDTO;
import com.jivrom.cricketminds.domain.dto.SlotDTO;
import com.jivrom.cricketminds.domain.dto.SlotDetailDTO;
import com.jivrom.cricketminds.domain.exception.UserNotFoundException;
import com.jivrom.cricketminds.domain.service.SessionSlotService;

@RestController
public class SlotController {

    @Resource
    private SessionSlotService sessionSlotService;

    //create a slot

    /**
     * creation of slot is initiated from generateSlot method.s
     * @param createSlotDTO
     * @return
     */
    @Deprecated
    @RequestMapping(value = "createSessionSlot", method = RequestMethod.POST)
    public ResponseEntity<ResponseDTO> createSessionSlot(@RequestBody CreateSlotDTO createSlotDTO) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {

            SlotDTO returnDTO = sessionSlotService.createSlot(createSlotDTO);
            responseMap.put("data", returnDTO);
            responseMap.put("status", "success");
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
            
        } catch (UserNotFoundException unfe) {
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);
    } catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
         response.setResponse(responseMap);
         
        return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
}

       @RequestMapping(value="sessionslots", method=RequestMethod.GET)
       public ResponseEntity<ResponseDTO> getAllSessionSlots() {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {
            List<SlotDTO> sessionResponse = sessionSlotService.getAllSessionSlots();
            responseMap.put("data", sessionResponse);
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        } catch (UserNotFoundException unfe) {
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
     
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
        return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
       }
       
       //get slots by month
       @RequestMapping(value="sessionslots/{monthOfYear}", method=RequestMethod.GET)
       public ResponseEntity<ResponseDTO> getAllSessionSlots(@PathVariable String monthOfYear) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {
        	// ref: select * from SLOT WHERE YEAR(SESSION_DATE) = 2019 AND MONTH(SESSION_DATE) = 6
            System.out.println("The monthOfYear variable value is: "+monthOfYear);
            List<SlotDTO> sessionResponse = sessionSlotService.getSlotsByMonth(monthOfYear); //FIXME: implement
            responseMap.put("data", sessionResponse);
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        } catch (UserNotFoundException unfe) {
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
     
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
        return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
       }


       //getslot by sessionId

    @RequestMapping(value="session/{sessionId}/slots", method=RequestMethod.GET)
    public ResponseEntity<ResponseDTO> getSlotBySessionId(@PathVariable Long sessionId) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {

        	List<SlotDTO> retval = sessionSlotService.getSlots(sessionId);
            responseMap.put("data", retval);
            response.setResponse(responseMap);

            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        }catch (UserNotFoundException unfe) {
            ErrorDTO errorDTO = new ErrorDTO(unfe.getMessage(), "CM400");
            response.setError(errorDTO);
            responseMap.put("status", "Invalid Request Error");
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);

        }catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        
    }

    //get slot by slotId

    @RequestMapping(value="slot/{slotId}", method=RequestMethod.GET)
    public ResponseEntity<ResponseDTO> getSlotBySlotId(@PathVariable Long slotId) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {

        	SlotDTO retval = sessionSlotService.getSlotById(slotId);
            responseMap.put("data", retval);
            response.setResponse(responseMap);

            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        }catch (UserNotFoundException unfe) {
            ErrorDTO errorDTO = new ErrorDTO(unfe.getMessage(), "CM400");
            response.setError(errorDTO);
            responseMap.put("status", "Invalid Request Error");
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);

        }catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        
    }
    
    
    // get slot details
    
    @RequestMapping(value="slotDetails/{slotId}", method=RequestMethod.GET)
    public ResponseEntity<ResponseDTO> getSlotDetailsBySlotId(@PathVariable Long slotId) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {

        	SlotDetailDTO retval = sessionSlotService.getSlotDetail(slotId);
            responseMap.put("data", retval);
            response.setResponse(responseMap);

            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        }catch (UserNotFoundException unfe) {
            ErrorDTO errorDTO = new ErrorDTO(unfe.getMessage(), "CM400");
            response.setError(errorDTO);
            responseMap.put("status", "Invalid Request Error");
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);

        }catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        
    }

        //delete a slot

       @RequestMapping(value="deleteSlot/{slotId}", method=RequestMethod.DELETE)
       public ResponseEntity<ResponseDTO> deleteSlotBySessionId(@PathVariable Long slotId) {

      
        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {
            SlotDTO returnDTO = sessionSlotService.deleteSlot(slotId);
            responseMap.put("data", returnDTO);
            responseMap.put("status", "success");
			response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        } catch (UserNotFoundException unfe) {

            ErrorDTO errorDTO = new ErrorDTO(unfe.getMessage(), "CM400");
            response.setError(errorDTO);
            responseMap.put("status", "Invalid Request Error");
            response.setResponse(responseMap);

            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
         response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
       }
       
       // assigning coach

       @RequestMapping(value="assignCoach", method=RequestMethod.POST)
       public ResponseEntity<ResponseDTO> assignCoachForSlot(@RequestBody AssignCoachDTO assignCoachDTO) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {
            System.out.println("Entering coach assinging end point0000..........");
            CoachDTO returnDTO = sessionSlotService.assignCoach(assignCoachDTO);
            System.out.println("Entering coach assinging ending..........");
            responseMap.put("data", returnDTO);
            responseMap.put("status", "success");
			response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        }  catch (UserNotFoundException unfe) {

            ErrorDTO errorDTO = new ErrorDTO(unfe.getMessage(), "CM400");
            response.setError(errorDTO);
            responseMap.put("status", "Invalid Request Error");
            response.setResponse(responseMap);

            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
         response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // delete coach

    @RequestMapping(value="deleteCoach", method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO> deleteCoachForSlot(@RequestBody AssignCoachDTO assignCoachDTO) {

     ResponseDTO response = new ResponseDTO();
     Map<String, Object> responseMap = new HashMap<>();

     try {
         System.out.println("Entering coach deletion end point0000..........");
         CoachDTO returnDTO = sessionSlotService.deleteCoach(assignCoachDTO);
         System.out.println("Entering coach deletion ending..........");
         responseMap.put("data", returnDTO);
         responseMap.put("status", "success");
         response.setResponse(responseMap);
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
     }  catch (UserNotFoundException unfe) {

         ErrorDTO errorDTO = new ErrorDTO(unfe.getMessage(), "CM400");
         response.setError(errorDTO);
         responseMap.put("status", "Invalid Request Error");
         response.setResponse(responseMap);

         return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);
     } catch (Exception e) {
         ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
      
         response.setError(errorDTO);
         responseMap.put("status", "Internal Sever Error");
      response.setResponse(responseMap);
      
      return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
     }
 }

    //get all coaches 

    @RequestMapping(value="coaches", method=RequestMethod.GET)
    public ResponseEntity<ResponseDTO> getAllCoaches() {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();
        try {
            List<CoachDTO> retval = sessionSlotService.getAllCoaches();
            responseMap.put("data", retval);
            responseMap.put("status", "success");
			response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        } catch (UserNotFoundException unfe) {

            ErrorDTO errorDTO = new ErrorDTO(unfe.getMessage(), "CM400");
            response.setError(errorDTO);
            responseMap.put("status", "Invalid Request Error");
            response.setResponse(responseMap);

            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
         response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }
    
       //update slot
        @RequestMapping(value = "updateSlot", method = RequestMethod.POST)
        public ResponseEntity<ResponseDTO> updateSlot(@RequestBody SlotDTO SlotDTO) {

            ResponseDTO response = new ResponseDTO();
            Map<String, Object> responseMap = new HashMap<>();

            try {

                SlotDTO returnDTO = sessionSlotService.updateSlots(SlotDTO);
                responseMap.put("data", returnDTO);
                responseMap.put("status", "success");
                response.setResponse(responseMap);
                return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
            
            } catch (UserNotFoundException unfe) {
                return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);
            } catch (Exception e) {
                ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
                response.setError(errorDTO);
                responseMap.put("status", "Internal Sever Error");
                response.setResponse(responseMap);
         
                return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }
    
        }

    // @RequestMapping(value = "slotsEnrollment/{slotId}", method = RequestMethod.GET)
	// public ResponseEntity<ResponseDTO> getRemainingSlotBookings(@PathVariable Long slotId) {
    //     ResponseDTO response = new ResponseDTO();
    //     Map<String, Object> responseMap = new HashMap<>();
        
    //     try{
    //         Long bookingleft = sessionSlotService.remainingSlotBookings(slotId);
           
    //         if(sessionSlotService.getSlotById(slotId) != null){
    //             responseMap.put("data", bookingleft);
    //         } else{
    //             responseMap.put("data", null);
    //         }
    //         response.setResponse(responseMap);

    //         return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
    //     }catch (Exception e) {
	// 		e.printStackTrace();
	// 		ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!" + e.getMessage(), "CM11");

	// 		response.setError(errorDTO);
	// 		responseMap.put("status", "Internal Sever Error");
	// 		response.setResponse(responseMap);

	// 		return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	// 	}
    // }

    @RequestMapping(value="generateSlots/{sessionId}", method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO> generateSlotsBySessionId(@PathVariable Long sessionId) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {
            sessionSlotService.generateSlots(sessionId);
            responseMap.put("data", "success");
            response.setResponse(responseMap);

            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        }catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        
    }
}