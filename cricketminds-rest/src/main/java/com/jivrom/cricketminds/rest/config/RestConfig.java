package com.jivrom.cricketminds.rest.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = "com.jivrom.cricketminds.rest")
@Configuration
public class RestConfig {

}
