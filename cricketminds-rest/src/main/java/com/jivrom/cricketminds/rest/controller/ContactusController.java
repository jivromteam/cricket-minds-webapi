package com.jivrom.cricketminds.rest.controller;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jivrom.cricketminds.domain.dto.ContactDTO;
import com.jivrom.cricketminds.domain.model.Mail;
import com.jivrom.cricketminds.domain.service.EmailService;

@RestController

public class ContactusController {
    
    @Resource
    private EmailService emailService;
    
    @Value("${mail.from}")
	private String fromEmail;
	
	@Value("${mail.admin}")
	private String adminEmail;

    @RequestMapping(value = "contactUS", method = RequestMethod.POST)
    public HttpStatus contactmail(@RequestBody ContactDTO contactdto)
    {

        try {

            Mail mail=new Mail();
            mail.setMailFrom(fromEmail);
    
            //TODO: externalize mail to properties
            mail.setMailTo(adminEmail);
            mail.setMailSubject("You have a new message from Cricket Minds Contact Us Page");

            String message = "You have a new message from "+ contactdto.getName()+"\n";
            message += "Phone number : "+ contactdto.getPhone()+"\n";
            message += "Email  : "+ contactdto.getEmail()+"\n";
            message += contactdto.getMessage()+"\n";

            mail.setMailContent(message);
            emailService.sendEmail(mail);

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
                
        return HttpStatus.OK;

    }

}


