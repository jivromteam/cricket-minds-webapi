package com.jivrom.cricketminds.rest.controller;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jivrom.cricketminds.domain.model.Mail;
import com.jivrom.cricketminds.domain.service.EmailService;

@RestController
public class EmailController {

	@Resource
	private EmailService emailService;

	@RequestMapping(value = "testEmail", method = RequestMethod.POST)
	public HttpStatus sendTestEmail(@RequestBody Mail mail) {

		try {

			System.out.println("Testing email for smtp");

			emailService.sendEmail(mail);

		} catch (Exception e) {
			System.out.println(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return HttpStatus.OK;

	}

}