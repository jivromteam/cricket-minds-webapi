package com.jivrom.cricketminds.rest.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
// @EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		super.addResourceHandlers(registry);

		if (!registry.hasMappingForPattern("/webjars/**")) {
			registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
		}

		registry.addResourceHandler("/js/**").addResourceLocations("classpath:/ui/js/");
		registry.addResourceHandler("/css/**").addResourceLocations("classpath:/ui/css/");
		registry.addResourceHandler("/images/**").addResourceLocations("classpath:/ui/images/");
		registry.addResourceHandler("/views/**").addResourceLocations("classpath:/ui/views/");
		registry.addResourceHandler("/fonts/**").addResourceLocations("classpath:/ui/css/fonts/");
		
		
		
		registry.addResourceHandler("/partials/**").addResourceLocations("classpath:/ui/partials/");
		registry.addResourceHandler("/bower_components/**").addResourceLocations("classpath:/ui/bower_components/");

		registry.addResourceHandler("/**.txt").addResourceLocations("classpath:/");
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**").allowedOrigins("*").allowedMethods("PUT", "DELETE", "POST", "GET", "PATCH");
	}
}
