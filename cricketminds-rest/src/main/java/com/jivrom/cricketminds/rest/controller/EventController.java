package com.jivrom.cricketminds.rest.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jivrom.cricketminds.domain.dto.ErrorDTO;
import com.jivrom.cricketminds.domain.dto.EventDTO;
import com.jivrom.cricketminds.domain.dto.EventRegisterDTO;
import com.jivrom.cricketminds.domain.dto.EventUpdateDTO;
import com.jivrom.cricketminds.domain.dto.ResponseDTO;
import com.jivrom.cricketminds.domain.dto.UserDTO;
import com.jivrom.cricketminds.domain.exception.UserNotFoundException;
import com.jivrom.cricketminds.domain.model.EventEnrollment;
import com.jivrom.cricketminds.domain.service.EventService;
import com.jivrom.cricketminds.domain.service.ValidationService;
import com.jivrom.cricketminds.payment.dto.PaymentDTO;

@RestController
public class EventController {

    @Resource
    private EventService eventService;
    
    @Resource
	private ValidationService validationService;

    //creating an event
    
    @RequestMapping(value="createEvent", method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO> createEvent(@RequestBody EventRegisterDTO eventRegisterDTO) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        ErrorDTO error = validationService.eventRegister(eventRegisterDTO);
		if (error != null) {

			response.setError(error);
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);
		}

        try {
                EventDTO returnDTO=eventService.createEvent(eventRegisterDTO);
                responseMap.put("data", returnDTO);
                responseMap.put("status", "success");
                response.setResponse(responseMap);
                return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
            
        } catch (UserNotFoundException unfe) {
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
         response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }


    // getting all events

    @RequestMapping(value="events", method=RequestMethod.GET)
    public ResponseEntity<ResponseDTO> getAllEvents() {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {
            List<EventDTO> eventResponse = eventService.getAllEvents();
            responseMap.put("data", eventResponse);
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);

        } catch (UserNotFoundException unfe) {
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }


    //getevent by eventId

    @RequestMapping(value="event/{eventId}", method=RequestMethod.GET)
    public ResponseEntity<ResponseDTO> getEventByEventId(@PathVariable Long eventId) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {

            EventDTO returnDTO = eventService.getEvent(eventId);
            responseMap.put("data", returnDTO);
            response.setResponse(responseMap);

            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        }catch (UserNotFoundException unfe) {
            ErrorDTO errorDTO = new ErrorDTO(unfe.getMessage(), "CM400");
            response.setError(errorDTO);
            responseMap.put("status", "Invalid Request Error");
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);

        }catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        
    }


    //getevent by status


    @RequestMapping(value="events/{eventStatus}", method=RequestMethod.GET)
    public ResponseEntity<ResponseDTO> getEvents(@PathVariable String eventStatus) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {
            if(eventStatus.equals("past")){
                List<EventDTO> eventResponse = eventService.findPastEvents();
                responseMap.put("data", eventResponse);
                response.setResponse(responseMap);
                return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
            }else if(eventStatus.equals("future")){
                List<EventDTO> eventResponse = eventService.findFutureEvents();
                responseMap.put("data", eventResponse);
                response.setResponse(responseMap);
                return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
            }else if(eventStatus.equals("current")){
                List<EventDTO> eventResponse = eventService.findCurrentEvents();
                responseMap.put("data", eventResponse);
                response.setResponse(responseMap);
                return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
            }
            
            ErrorDTO errorDTO = new ErrorDTO("Invalid status!!Accepted values are past,future and current", "CM12");
            response.setError(errorDTO);
            responseMap.put("status", "BadRequest");
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    
    @RequestMapping(value="events/{eventStatus}/member/{memberId}", method=RequestMethod.GET)
    public ResponseEntity<ResponseDTO> getEventsByMemberAndStatus(@PathVariable String eventStatus, @PathVariable Long memberId) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {
        	
        	List<EventDTO> eventResponse = eventService.findEvents(memberId, eventStatus);
        	responseMap.put("data", eventResponse);
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
            
        } catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    

    //update an event

    @RequestMapping(value="updateEvent/{eventId}", method=RequestMethod.PUT)
    public ResponseEntity<ResponseDTO> updateEvent(@PathVariable Long eventId,
    @RequestBody EventUpdateDTO eventUpdateDTO) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {
            
        //          EventDTO eventDTO=eventService.updateEvent(eventId,       eventUpdateDTO.getEventName(),eventUpdateDTO.getEventDescription(), eventUpdateDTO.getLocation(),eventUpdateDTO.getFee(),eventUpdateDTO.getSlotsAvailable(),eventUpdateDTO.getEventBannerUrl());
        	
        	EventDTO eventDTO = eventService.updateEvent(eventId, eventUpdateDTO);
            responseMap.put("data", eventDTO);
            response.setResponse(responseMap);

            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);

        } catch (UserNotFoundException unfe) {
            ErrorDTO errorDTO = new ErrorDTO(unfe.getMessage(), "CM400");
            response.setError(errorDTO);
            responseMap.put("status", "Invalid Request Error");
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
       
    }

    //delete an event

    @RequestMapping(value="deleteEvent/{eventId}", method=RequestMethod.DELETE)
    public ResponseEntity<ResponseDTO> deleteEventById(@PathVariable Long eventId) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {
            
            EventDTO eventDTO = eventService.deleteEvent(eventId);
            responseMap.put("data", eventDTO);
            responseMap.put("status", "success");
			response.setResponse(responseMap);

            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        } catch (UserNotFoundException unfe) {

            ErrorDTO errorDTO = new ErrorDTO(unfe.getMessage(), "CM400");
            response.setError(errorDTO);
            responseMap.put("status", "Invalid Request Error");
            response.setResponse(responseMap);

            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
         response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
       }

       //getEvents by status

       /*@RequestMapping(value="events/{status}", method=RequestMethod.GET)
       public ResponseEntity<ResponseDTO> getEventsByStatus(@PathVariable String eventStatus) {

           ResponseDTO response = new ResponseDTO();
           Map<String, Object> responseMap = new HashMap<>();

           try {

            //List<EventDTO> userResponse = eventService.getEvents(eventStatus);
            //responseMap.put("data", userResponse);
            responseMap.put("status", "success");
			response.setResponse(responseMap);

            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);

        } catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
         response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
          
        
       }

       */

    
    @RequestMapping(value="event/{eventId}/payandenroll", method=RequestMethod.PUT)
    public ResponseEntity<ResponseDTO> enrollEvent(@PathVariable Long eventId, @RequestBody PaymentDTO paymentDTO) {
    	
    	ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();
        
    	try {
    		
    		EventEnrollment enrollment = eventService.payAndEnrollMember(eventId, paymentDTO);
    		
    		responseMap.put("enrollmentId", enrollment.getEnrollmentId());
    		responseMap.put("paymentStatus", enrollment.getPaymentStatus());
            responseMap.put("status", "success");
			response.setResponse(responseMap);
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
			
    	} catch(Exception e) {
    		e.printStackTrace();
    		
    		ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
            
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
            
    		return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
    }
    
	@RequestMapping(value = "event/{eventId}/member/{memberId}/enrollstatus", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getEnrollmentStatusByMemberId(@PathVariable Long eventId, @PathVariable Long memberId) {

		ResponseDTO response = new ResponseDTO();
		Map<String, Object> responseMap = new HashMap<>();

		try {

			EventEnrollment returnDTO = eventService.getEnrollment(eventId, memberId);
			if(returnDTO != null) {
				responseMap.put("data", returnDTO);
				responseMap.put("status", "enrolled");
			} else {
				responseMap.put("data", null);
				responseMap.put("status", "notnrolled");
			}
			response.setResponse(responseMap);

			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!" + e.getMessage(), "CM11");

			response.setError(errorDTO);
			responseMap.put("status", "Internal Sever Error");
			response.setResponse(responseMap);

			return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

    @RequestMapping(value = "slotsLeft/{eventId}", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getEventSlotsRemaining(@PathVariable Long eventId) {
        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();
        
        try{
            Long slotsleft = eventService.remainingEventSlots(eventId);
           
            if(eventService.getEvent(eventId) != null){
                responseMap.put("data", slotsleft);
            } else{
                responseMap.put("data", null);
            }
            response.setResponse(responseMap);

            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        }catch (Exception e) {
			e.printStackTrace();
			ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!" + e.getMessage(), "CM11");

			response.setError(errorDTO);
			responseMap.put("status", "Internal Sever Error");
			response.setResponse(responseMap);

			return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
    
    @RequestMapping(value = "event/{eventId}/members", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getMembersByErollment(@PathVariable Long eventId) {

		ResponseDTO response = new ResponseDTO();
		
		List<UserDTO> userResponse = eventService.enrolledMembers(eventId);

		Map<String, Object> responseMap = new HashMap<>();
		responseMap.put("data", userResponse);

		response.setResponse(responseMap);

		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
	}
       
}