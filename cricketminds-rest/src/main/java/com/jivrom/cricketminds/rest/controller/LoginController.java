package com.jivrom.cricketminds.rest.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jivrom.cricketminds.domain.dto.ErrorDTO;
import com.jivrom.cricketminds.domain.dto.LoginDTO;
import com.jivrom.cricketminds.domain.dto.RegistrationDTO;
import com.jivrom.cricketminds.domain.dto.ResponseDTO;
import com.jivrom.cricketminds.domain.dto.UserDTO;
import com.jivrom.cricketminds.domain.model.Member;
import com.jivrom.cricketminds.domain.service.AuthenticationService;
import com.jivrom.cricketminds.domain.service.ValidationService;

@RestController
public class LoginController {

	@Resource
	private AuthenticationService authenticationService;
	
	
	
	
	@RequestMapping(value = "login", method = RequestMethod.PUT)
	public ResponseEntity<ResponseDTO> login(@RequestBody LoginDTO loginDTO) {
		ResponseDTO response = new ResponseDTO();
		
		Map<String, Object> data = new HashMap<>();
		try {
			UserDTO user  = authenticationService.login(loginDTO);
			
			data.put("data", user);
			response.setResponse(data);
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} catch(Exception e) {
			e.printStackTrace();
			ErrorDTO errorDTO = new ErrorDTO(e.getMessage(), "CM101"); //TODO: impl exception handling and error codes
			response.setError(errorDTO);
			
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
