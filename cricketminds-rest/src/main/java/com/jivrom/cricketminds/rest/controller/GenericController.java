package com.jivrom.cricketminds.rest.controller;

import java.net.CookieHandler;
import java.net.CookieManager;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GenericController {

	@RequestMapping("/version")
    public String version() {
        return "version : 1.0.1 02/08";
    }

    @RequestMapping("/")
    public String landing() {
        return "This is the langing view.";
    }
    
    @RequestMapping("/testEmailAuth")
    public String emailAuth() {
    	

    	try {
    		
	    	String url = "https://accounts.google.com/ServiceLoginAuth";
	    	String gmail = "https://mail.google.com/mail/";
	
	    	HttpUrlConnectionExample http = new HttpUrlConnectionExample();
	
	    	// make sure cookies is turn on
	    	CookieHandler.setDefault(new CookieManager());
	
	    	// 1. Send a "GET" request, so that you can extract the form's data.
	    	String page = http.GetPageContent(url);
	    	String postParams = http.getFormParams(page, "jivrompvt@gmail.com", "Jivrom@2323");
	
	    	// 2. Construct above post's content and then send a POST request for
	    	// authentication
	    	http.sendPost(url, postParams);
	
	    	// 3. success then go to gmail.
	    	String result = http.GetPageContent(gmail);
	    	System.out.println(result);
      
    	} catch(Exception e) {
    		e.printStackTrace();
    		return "error";
    	}
        return "Success";
    }
    
}
