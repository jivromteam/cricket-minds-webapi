package com.jivrom.cricketminds.rest.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jivrom.cricketminds.common.util.DateUtil;
import com.jivrom.cricketminds.domain.dto.ErrorDTO;
import com.jivrom.cricketminds.domain.dto.ResponseDTO;
import com.jivrom.cricketminds.domain.dto.SessionDTO;
import com.jivrom.cricketminds.domain.dto.SessionRegisterDTO;
import com.jivrom.cricketminds.domain.dto.SessionUpdateDTO;
import com.jivrom.cricketminds.domain.dto.UserDTO;
import com.jivrom.cricketminds.domain.exception.UserNotFoundException;
import com.jivrom.cricketminds.domain.model.SessionEnrollment;
import com.jivrom.cricketminds.domain.service.SessionService;
import com.jivrom.cricketminds.domain.service.ValidationService;
import com.jivrom.cricketminds.payment.dto.PaymentDTO;


@RestController
public class SessionController {

    @Resource
    private SessionService sessionService;

    @Resource
    private ValidationService validationService;
    
    //creating a session

    @RequestMapping(value="createCoachingSession", method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO> createCoachSession(@RequestBody SessionRegisterDTO sessionRegisterDTO) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        ErrorDTO error = validationService.sessionRegister(sessionRegisterDTO);
		if (error != null) {

			response.setError(error);
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);
		}

        try {
            SessionDTO returnDTO=sessionService.createSession(sessionRegisterDTO);
            responseMap.put("data", returnDTO);
            responseMap.put("status", "success");
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        } catch (UserNotFoundException unfe) {
                return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
                ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
             
                response.setError(errorDTO);
                responseMap.put("status", "Internal Sever Error");
             response.setResponse(responseMap);
             
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
   //getting all sessions

   @RequestMapping(value="coachingsessions", method=RequestMethod.GET)
   public ResponseEntity<ResponseDTO> getAllSessions() {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {
            List<SessionDTO> sessionResponse = sessionService.getAllSessions();
            responseMap.put("data", sessionResponse);
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);

        } catch (UserNotFoundException unfe) {
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
     
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
        return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //getsession by sessionId

    @RequestMapping(value="session/{sessionId}", method=RequestMethod.GET)
    public ResponseEntity<ResponseDTO> getSessionBySessionId(@PathVariable Long sessionId) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {
            System.out.println("testing session by id 1111111........");
            SessionDTO returnDTO = sessionService.getSession(sessionId);
            System.out.println("testing session by id 4444444........");
            responseMap.put("data", returnDTO);
            response.setResponse(responseMap);

            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        }catch (UserNotFoundException unfe) {
            ErrorDTO errorDTO = new ErrorDTO(unfe.getMessage(), "CM400");
            response.setError(errorDTO);
            responseMap.put("status", "Invalid Request Error");
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);

        }catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        
    }

    // get session by slot id

    @RequestMapping(value="sessionBySlotId/{slotId}", method=RequestMethod.GET)
    public ResponseEntity<ResponseDTO> getSessionBySlotId(@PathVariable Long slotId) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();
        //TODO: Slots must have session name and type.
        try {
            System.out.println("testing session by id 1111111........");
            SessionDTO returnDTO = sessionService.getSessionBySlotId(slotId);
            System.out.println("testing session by id 4444444........");
            responseMap.put("data", returnDTO);
            response.setResponse(responseMap);

            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        }catch (UserNotFoundException unfe) {
            ErrorDTO errorDTO = new ErrorDTO(unfe.getMessage(), "CM400");
            response.setError(errorDTO);
            responseMap.put("status", "Invalid Request Error");
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);

        }catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        
    }

    //getsessions by status

    @RequestMapping(value="sessions/{sessionStatus}", method=RequestMethod.GET)
    public ResponseEntity<ResponseDTO> getSessionsByStatus(@PathVariable String sessionStatus ) {
        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

     try {
        System.out.println("session status11111111111.................");
        if(sessionStatus.equals("past")){
            List<SessionDTO> sessionResponse = sessionService.findPastSessions();
            responseMap.put("data", sessionResponse);
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        }else if(sessionStatus.equals("future")){
            List<SessionDTO> sessionResponse = sessionService.findFutureSessions();
            responseMap.put("data", sessionResponse);
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        }else if(sessionStatus.equals("current")){
            List<SessionDTO> sessionResponse = sessionService.findCurrentSessions();
            responseMap.put("data", sessionResponse);
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        }
        
        ErrorDTO errorDTO = new ErrorDTO("Invalid status!!Accepted values are past,future and current", "CM12");
        response.setError(errorDTO);
        responseMap.put("status", "BadRequest");
        response.setResponse(responseMap);
        return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);
    
      } catch (Exception e) {
        ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
        response.setError(errorDTO);
        responseMap.put("status", "Internal Sever Error");
        response.setResponse(responseMap);
     
     return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
   }
        
    }
    
    //get sessions by status and id
    
    @RequestMapping(value="sessions/{sessionStatus}/member/{memberId}", method=RequestMethod.GET)
    public ResponseEntity<ResponseDTO> getSessionsByMemberAndStatus(@PathVariable String sessionStatus, @PathVariable Long memberId) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {
        	
        	List<SessionDTO> sessionResponse = sessionService.findSessions(memberId, sessionStatus);
        	responseMap.put("data", sessionResponse);
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
            
        } catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }   
    
    //get sessions by month

    @RequestMapping(value="sessionsByMonth/{monthOfYear}", method=RequestMethod.GET)
    public ResponseEntity<ResponseDTO> getAllSessionByMonth(@PathVariable String monthOfYear) {

     ResponseDTO response = new ResponseDTO();
     Map<String, Object> responseMap = new HashMap<>();

     try {
        System.out.println("The monthOfYear variable value is: "+monthOfYear);
         List<SessionDTO> sessionResponse = sessionService.getSessionsByMonth(monthOfYear); 
         responseMap.put("data", sessionResponse);
         response.setResponse(responseMap);
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
     } catch (UserNotFoundException unfe) {
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);

     } catch (Exception e) {
         ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
  
         response.setError(errorDTO);
         responseMap.put("status", "Internal Sever Error");
         response.setResponse(responseMap);
        return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
     }
     
    }
    
    
    //get available sessions for the given date
    @RequestMapping(value="sessionsByDate/{givenDate}", method=RequestMethod.GET)
    public ResponseEntity<ResponseDTO> getAvailableSessionByDate(@PathVariable String givenDate) {

     ResponseDTO response = new ResponseDTO();
     Map<String, Object> responseMap = new HashMap<>();

     try {
    	 System.out.println("The given date variable value is: "+givenDate);
    	 Date sessionDate = DateUtil.parseDate(givenDate);
    	 
         List<SessionDTO> sessionResponse = sessionService.getAvailableSessionsByDate(sessionDate); 
         responseMap.put("data", sessionResponse);
         response.setResponse(responseMap);
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
     } catch (UserNotFoundException unfe) {
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);

     } catch (Exception e) {
         ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
  
         response.setError(errorDTO);
         responseMap.put("status", "Internal Sever Error");
         response.setResponse(responseMap);
        return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
     }
     
    }
    //update an event

    @RequestMapping(value="updateSession/{sessionId}", method=RequestMethod.PUT)
    public ResponseEntity<ResponseDTO> updateSession(@PathVariable Long sessionId,
    @RequestBody SessionUpdateDTO sessionUpdateDTO) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {
        	SessionDTO sessionDTO = sessionService.updateSession(sessionUpdateDTO);
            responseMap.put("data", sessionDTO);
            response.setResponse(responseMap);

            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);

        } catch (UserNotFoundException unfe) {
            ErrorDTO errorDTO = new ErrorDTO(unfe.getMessage(), "CM400");
            response.setError(errorDTO);
            responseMap.put("status", "Invalid Request Error");
            response.setResponse(responseMap);
            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
       
    }

    //delete an event

    @RequestMapping(value="deleteSession/{sessionId}", method=RequestMethod.DELETE)
    public ResponseEntity<ResponseDTO> deleteSessionById(@PathVariable Long sessionId) {

        ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();

        try {
            
            SessionDTO sessionDTO = sessionService.deleteSession(sessionId);
            responseMap.put("data", sessionDTO);
            responseMap.put("status", "success");
			response.setResponse(responseMap);

            return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
        } catch (UserNotFoundException unfe) {

            ErrorDTO errorDTO = new ErrorDTO(unfe.getMessage(), "CM400");
            response.setError(errorDTO);
            responseMap.put("status", "Invalid Request Error");
            response.setResponse(responseMap);

            return new ResponseEntity<ResponseDTO>(response, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
         
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
         response.setResponse(responseMap);
         
         return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // pay and enroll for session
    
    @RequestMapping(value="session/{sessionId}/payandenroll", method=RequestMethod.PUT)
    public ResponseEntity<ResponseDTO> enrollSession(@PathVariable Long sessionId, @RequestBody PaymentDTO paymentDTO) {
    	
    	ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();
        
    	try {
    		
    		SessionEnrollment enrollment = sessionService.payAndEnrollSession(sessionId, paymentDTO);
    		
    		responseMap.put("enrollmentId", enrollment.getSessionEnrollmentId());
    		responseMap.put("paymentStatus", enrollment.getPaymentStatus());
            responseMap.put("status", "success");
			response.setResponse(responseMap);
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
			
    	} catch(Exception e) {
    		e.printStackTrace();
    		
    		ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
            
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
            
    		return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
    }
    
    // enroll member to session before pay

    @RequestMapping(value="session/{sessionId}/member/{memberId}", method=RequestMethod.PUT)
    public ResponseEntity<ResponseDTO> enrollSessionBeforePay(@PathVariable Long sessionId, @PathVariable Long memberId) {
    	
    	ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();
        
    	try {
    		
    		SessionEnrollment enrollment = sessionService.enrollSessionBeforePay(sessionId, memberId);
    		
    		responseMap.put("enrollmentId", enrollment.getSessionEnrollmentId());
    		responseMap.put("paymentStatus", enrollment.getPaymentStatus());
            responseMap.put("status", "success");
			response.setResponse(responseMap);
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
			
    	} catch(Exception e) {
    		e.printStackTrace();
    		
    		ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
            
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
            
    		return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
    }

    // enroll coach to session 

    @RequestMapping(value="session/{sessionId}/coach/{memberId}", method=RequestMethod.PUT)
    public ResponseEntity<ResponseDTO> enrollCoach(@PathVariable Long sessionId, @PathVariable Long memberId) {
    	
    	ResponseDTO response = new ResponseDTO();
        Map<String, Object> responseMap = new HashMap<>();
        
    	try {
    		
    		SessionEnrollment enrollment = sessionService.enrollSessionBeforePay(sessionId, memberId);
    		
    		responseMap.put("enrollmentId", enrollment.getSessionEnrollmentId());
    		responseMap.put("paymentStatus", enrollment.getPaymentStatus());
            responseMap.put("status", "success");
			response.setResponse(responseMap);
			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
			
    	} catch(Exception e) {
    		e.printStackTrace();
    		
    		ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!"+e.getMessage(), "CM11");
            
            response.setError(errorDTO);
            responseMap.put("status", "Internal Sever Error");
            response.setResponse(responseMap);
            
    		return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
    }

    // get enrollment status by member id

	@RequestMapping(value = "session/{sessionId}/member/{memberId}/enrollstatus", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getEnrollmentStatusByMemberId(@PathVariable Long sessionId, @PathVariable Long memberId) {

		ResponseDTO response = new ResponseDTO();
		Map<String, Object> responseMap = new HashMap<>();

		try {

			SessionEnrollment returnDTO = sessionService.getEnrollment(sessionId, memberId);
			if(returnDTO != null) {
				responseMap.put("data", returnDTO);
				responseMap.put("status", "enrolled");
			} else {
				responseMap.put("data", null);
				responseMap.put("status", "notnrolled");
			}
			response.setResponse(responseMap);

			return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!" + e.getMessage(), "CM11");

			response.setError(errorDTO);
			responseMap.put("status", "Internal Sever Error");
			response.setResponse(responseMap);

			return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
    
    // @RequestMapping(value = "sessionslotsLeft/{sessionId}", method = RequestMethod.GET)
	// public ResponseEntity<ResponseDTO> getSessionSlotsRemaining(@PathVariable Long sessionId) {
    //     ResponseDTO response = new ResponseDTO();
    //     Map<String, Object> responseMap = new HashMap<>();
        
    //     try{
    //         Long slotsleft = sessionService.remainingSlots(sessionId);

    //         if(sessionService.getSession(sessionId) != null){
    //             responseMap.put("data", slotsleft);
    //         } else{
    //             responseMap.put("data", null);
    //         }
    //         response.setResponse(responseMap);

    //         return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
    //     }catch (Exception e) {
	// 		e.printStackTrace();
	// 		ErrorDTO errorDTO = new ErrorDTO("Something went wrong!!" + e.getMessage(), "CM11");

	// 		response.setError(errorDTO);
	// 		responseMap.put("status", "Internal Sever Error");
	// 		response.setResponse(responseMap);

	// 		return new ResponseEntity<ResponseDTO>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	// 	}
    // }   
    
    //get enrolled members for session

    @RequestMapping(value = "session/{sessionId}/members", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getMembersByErollment(@PathVariable Long sessionId) {

		ResponseDTO response = new ResponseDTO();
		String role = "MEMBER";
		List<UserDTO> userResponse = sessionService.enrolledMembers(sessionId, role);

		Map<String, Object> responseMap = new HashMap<>();
		responseMap.put("data", userResponse);

		response.setResponse(responseMap);

		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
    }  

    //get enrolled coaches for session

    @RequestMapping(value = "session/{sessionId}/coaches", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> getCoachesByErollment(@PathVariable Long sessionId) {

		ResponseDTO response = new ResponseDTO();
		String role = "COACH";
		List<UserDTO> userResponse = sessionService.enrolledMembers(sessionId, role);

		Map<String, Object> responseMap = new HashMap<>();
		responseMap.put("data", userResponse);

		response.setResponse(responseMap);

		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
    }  
    
     //remove enrolled member for session

    @RequestMapping(value = "sessionRemove/{sessionId}/member/{memberId}", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO> removeMember(@PathVariable Long sessionId, @PathVariable Long memberId) {

		ResponseDTO response = new ResponseDTO();
		
		UserDTO userResponse = sessionService.removeMembers(sessionId,memberId);

		Map<String, Object> responseMap = new HashMap<>();
		responseMap.put("data", userResponse);

		response.setResponse(responseMap);

		return new ResponseEntity<ResponseDTO>(response, HttpStatus.OK);
    }  



   
    
}
